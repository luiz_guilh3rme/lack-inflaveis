	<?php 

		//SEO

		$title = 'Infláveis e Balões Promocionais | Lack Infláveis ';

		$description = 'A Lack Infláveis é Especializada em Infláveis. Aproveite e acesse agora e solicite já o seu orçamento online do seu Inflável!';

		$canonicalTag = '<link rel="canonical” href="http://www.lackinflaveis.com.br/"/>';

		$bg = "	<div id=\"bg-1\"></div>

		<div id=\"bg-2\" class=\"hidden-xs\"></div>

		<div id=\"bg-3\" class=\"hidden-xs\"></div>";

	?>

	<?php require_once './class/Posts.php'; ?>

	<?php require_once './includes/header.php'; ?>

		

		<!-- carousel slide/clientes -->

		<section class="rows">

			<div id="slide" class="carousel slide carousel-fade hidden-xs" data-ride="carousel" data-pause="hover">

				<ul class="carousel-indicators hide">

					<li data-target="#slide" data-slide-to="0" class="active"></li>

					<li data-target="#slide" data-slide-to="1"></li>



				</ul>

				<div class="carousel-inner">

					<div class="active item">

						<ul id="parallax">							

							<li class="layer expand-width" id="parallax-balao-1" data-depth="0.20"><img src="imagens/slide/balao-1.png" alt="Balão 1" class="img-responsive" /></li>

							<li class="layer expand-width" id="parallax-balao-2" data-depth="0.20"><img src="imagens/slide/balao-2.png" alt="Balão 2" class="img-responsive" /></li>							

							<li class="layer expand-width" id="parallax-fixed" data-depth="0.00"><img src="imagens/slide/banner-1.png" alt="Aqui suas ideias se concretizam" class="img-responsive" /></li>

						</ul>						

					</div>

					<div class="item"><img src="imagens/slide/banner-2.png" alt="A divulgação perfeita para seu evento" class="img-responsive" /></div>

				</div>

				<a class="carousel-control left"  href="#slide" data-slide="prev" role="button" title="Voltar"><span class="hide">Voltar</span></a>

				<a class="carousel-control right" href="#slide" data-slide="next" role="button" title="Avançar"><span class="hide">Avançar</span></a>

			</div>			

		</section>



		<section class="rows">

			<h2 class="title"><span id="line-2" class="hidden-xs hidden-sm"></span>Nossos <span>clientes</span></h2>

			<p class="text-indent">

				<strong>A Lack Infláveis tem como maior preocupação a satisfação completa de cada cliente </strong> que traz seu projeto à nossa loja, independente do tamanho e da natureza do mesmo. Por isso, fazemos questão de cativá-los através da entrega da melhor solução para suas demandas. 

			</p>

		</section>



		<section class="rows hidden-xs">

			<div id="cliente" class="carousel slide" data-ride="carousel">

				<ul class="carousel-indicators hide">

					<li data-target="#cliente" data-slide-to="0" class="active"></li>

					<li data-target="#cliente" data-slide-to="1"></li>

				</ul>				

				<div class="carousel-inner col-lg-8 col-md-8 col-sm-8">

					<div class="active item">

						<div class="rows">

							<ul class="list-cliente" id="list-cliente-1">

								<li class="col-lg-3 col-md-3 col-sm-3"><img src="imagens/clientes/abril.jpg" alt="Abril" class="img-responsive"></li>

								<li class="col-lg-3 col-md-3 col-sm-3"><img src="imagens/clientes/brahma.jpg" alt="Brahma" class="img-responsive"></li>

								<li class="col-lg-3 col-md-3 col-sm-3"><img src="imagens/clientes/castrol.jpg" alt="Castrol" class="img-responsive"></li>

								<li class="col-lg-3 col-md-3 col-sm-3"><img src="imagens/clientes/cielo.jpg" alt="Cielo" class="img-responsive"></li>

							</ul>

						</div>

					</div>

					<div class="item">

						<div class="rows">

							<ul class="list-cliente" id="list-cliente-1">

								<li class="col-lg-3 col-md-3 col-sm-3"><img src="imagens/clientes/devassa.jpg" alt="Devassa" class="img-responsive"></li>

								<li class="col-lg-3 col-md-3 col-sm-3"><img src="imagens/clientes/ducati.jpg" alt="Ducate" class="img-responsive"></li>

								<li class="col-lg-3 col-md-3 col-sm-3"><img src="imagens/clientes/editora-saraiva.jpg" alt="Editora Saraiva" class="img-responsive"></li>

								<li class="col-lg-3 col-md-3 col-sm-3"><img src="imagens/clientes/fiat.jpg" alt="Fiat" class="img-responsive"></li>

							</ul>

						</div>

					</div>

					<div class="item">

						<div class="rows">

							<ul class="list-cliente" id="list-cliente-1">

								<li class="col-lg-3 col-md-3 col-sm-3"><img src="imagens/clientes/itau.jpg" alt="Itau" class="img-responsive"></li>

								<li class="col-lg-3 col-md-3 col-sm-3"><img src="imagens/clientes/monster-energy.jpg" alt="Monster energy" class="img-responsive"></li>

								<li class="col-lg-3 col-md-3 col-sm-3"><img src="imagens/clientes/nova-schin.jpg" alt="Nova schin" class="img-responsive"></li>

								<li class="col-lg-3 col-md-3 col-sm-3"><img src="imagens/clientes/pdg.jpg" alt="Pdg" class="img-responsive"></li>

							</ul>

						</div>

					</div>

					<div class="item">

						<div class="rows">

							<ul class="list-cliente" id="list-cliente-1">

								<li class="col-lg-3 col-md-3 col-sm-3"><img src="imagens/clientes/sabesp.jpg" alt="Sabesp" class="img-responsive"></li>

								<li class="col-lg-3 col-md-3 col-sm-3"><img src="imagens/clientes/samsung.jpg" alt="Samsung" class="img-responsive"></li>

								<li class="col-lg-3 col-md-3 col-sm-3"><img src="imagens/clientes/santander.jpg" alt="Santander" class="img-responsive"></li>

								<li class="col-lg-3 col-md-3 col-sm-3"><img src="imagens/clientes/uniao-quimica.jpg" alt="União Quimica" class="img-responsive"></li>

							</ul>

						</div>

					</div>

					<div class="item">

						<div class="rows">

							<ul class="list-cliente" id="list-cliente-1">

								<li class="col-lg-3 col-md-3 col-sm-3"><img src="imagens/clientes/volkswagen.jpg" alt="Volkswagen" class="img-responsive"></li>

								<li class="col-lg-3 col-md-3 col-sm-3"><img src="imagens/clientes/bmw.jpg" alt="Bmw" class="img-responsive"></li>

								<li class="col-lg-3 col-md-3 col-sm-3"><img src="imagens/clientes/ems.jpg" alt="Ems" class="img-responsive"></li>

								<li class="col-lg-3 col-md-3 col-sm-3"><img src="imagens/clientes/ford.jpg" alt="Ford" class="img-responsive"></li>

							</ul>

						</div>

					</div>

				</div>			

				<a class="carousel-control left" href="#cliente" data-slide="prev" role="button" title="Voltar"><span class="hide">Voltar</span></a>

				<a class="carousel-control right" href="#cliente" data-slide="next" role="button" title="Avançar"><span class="hide">Avanaçar</span></a>

			</div>			

		</section>

		<!-- end carousel slide/clientes -->



		<!-- produtos -->

		<section class="rows clearfix">

			<h1 class="title-2"><span id="line-3" class="hidden-xs hidden-sm"></span>Nossos <span>infláveis promocionais</span></h1>

			<p class="text-indent-2 pull-right">

				<strong>Para garantir que sua empresa marque presença em qualquer evento com personalidade e confiabilidade,</strong> além de um excelente visual, todos os nossos produtos são confeccionados com material de altíssima qualidade e por uma equipe experiente e atualizada. 

			</p>

		</section>

		<section class="rows">

			<ul class="lista-produto">				

				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">

					<a href="inflaveis/roof-tops.php" title="Roof Tops">

						<img src="imagens/produtos/roof-tops.png" alt="Roof Tops" class="img-responsive">

						<span class="grama"></span>						

						<h2>Roof Tops</h2>

					</a>

				</li>		

				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">

					<a href="inflaveis/tendas.php" title="Tendas">

						<img src="imagens/produtos/tendas.png" alt="Tendas" class="img-responsive">

						<span class="asfalto"></span>						

						<h2>Tendas</h2>

					</a>

				</li>		

				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">

					<a href="inflaveis/tuneis.php" title="Túneis">

						<img src="imagens/produtos/tuneis.png" alt="Túneis" class="img-responsive">

						<span class="grama"></span>						

						<h2>Túneis</h2>

					</a>

				</li>		

				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">

					<a href="inflaveis/replicas.php" title="Réplicas">

						<img src="imagens/produtos/replicas.png" alt="Réplicas" class="img-responsive">

						<span class="asfalto"></span>						

						<h2>Réplicas</h2>

					</a>

				</li>		

				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">

					<a href="inflaveis/blimp.php" title="Blimp">

						<img src="imagens/produtos/blimp.png" alt="Blimp" class="img-responsive">

						<span class="grama"></span>						

						<h2>Blimp</h2>

					</a>

				</li>

				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">

					<a href="inflaveis/portais.php" title="Portais">

						<img src="imagens/produtos/portal.png" alt="Portais" class="img-responsive">

						<span class="asfalto"></span>						

						<h2>Portais</h2>

					</a>

				</li>	

				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">

					<a href="inflaveis/mascotes.php" title="Mascotes">

						<img src="imagens/produtos/mascote.png" alt="Mascotes" class="img-responsive">

						<span class="grama"></span>						

						<h2>Mascotes</h2>

					</a>

				</li>	

				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">

					<a href="inflaveis/totem.php" title="Totens">

						<img src="imagens/produtos/totems.png" alt="Totens" class="img-responsive">

						<span class="asfalto"></span>						

						<h2>Totens</h2>

					</a>

				</li>

				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">

					<a href="inflaveis/bola.php" title="">

						<img src="imagens/produtos/bolas.png" alt="Bolas" class="img-responsive">

						<span class="grama"></span>						

						<h2>Bola</h2>

					</a>

				</li>	

				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">

					<a href="inflaveis/tela-de-projecao.php" title="Telas de Projeção">

						<img src="imagens/produtos/telas-projecaos.png" alt="Telas de Projeção" class="img-responsive">

						<span class="asfalto"></span>						

						<h2>Telas de Projeção</h2>

					</a>

				</li>

				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">

					<a href="inflaveis/stands.php" title="Stands">

						<img src="imagens/produtos/stand.png" alt="Stands" class="img-responsive">

						<span class="grama"></span>						

						<h2>Stands</h2>

					</a>

				</li>

				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">

					<a href="inflaveis/logotipo.php" title="Logotipos">

						<img src="imagens/produtos/logotipos.png" alt="Logotipos" class="img-responsive">

						<span class="asfalto"></span>						

						<h2>Logotipos</h2>

					</a>

				</li>										

				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">

					<a href="inflaveis/fantasia.php" title="Fantasias">

						<img src="imagens/produtos/fantasias.png" alt="Fantasias" class="img-responsive">

						<span class="grama"></span>						

						<h2>Fantasias</h2>

					</a>

				</li>				

				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">

					<a href="inflaveis/painel.php" title="Painéis">

						<img src="imagens/produtos/paineis.png" alt="Painéis" class="img-responsive">

						<span class="grama"></span>						

						<h2>Painéis</h2>

					</a>

				</li>

			</ul>

		</section>

	</div>

	<!-- end produtos -->



	<div class="container-fluid dep-blog-co">
		<div id="bg-4"></div>
		<!-- depoimento/blog/contato -->
		<div class="container" style="z-index: 1;">
			<div class="rows">

				<section class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="dep">

					<h3>Depoimentos</h3>

					<ul>

						<li>

							<img src="imagens/depoimentos/depoimento-brasil-kirin.png" alt="Lider Farma">

							<p><span>Patrícia – Lider Farma - Recebemos nosso inflável e ficou tudo perfeito. Parabéns pela organização e agilidade!</span></p>

						</li>

						<li>

							<img src="imagens/depoimentos/depoimento-lider-farma.png" alt="Italo Pasqualoton">

							<p><span>Italo Pasqualoto - Brasil Kirin - Parabéns pela competência. Produtos de ótima qualidade e espero podermos fazer outros negócios com vocês.</span></p>

						</li>

						<li>

							<img src="imagens/depoimentos/depoimento-iper.png" alt="Iper Imóveis">

							<p><span>Paula Rangel Sterzi – Iper Imóveis - Os infláveis ficaram lindos! Estão sendo super elogiados. Chegou tudo direitinho, muito obrigada por seu atendimento ágil e de qualidade.</span></p>

						</li>							

					</ul>

				</section>

				<section class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="cat">

					<h3 class="title-3">Nossos <span>catálogos</span></h3>

					<p>Apresentamos a você nosso catálogo virtual, para que conheça todas as opções que colocamos à sua disposição</p>

					<form name="form-catalogo" id="form-catalogo" action="" method="post" role="form" class="clearfix">

						<input type="hidden" name="url" value="<?= "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>">

						<input type="text" name="nome" placeholder="Nome" class="form-control">

						<input type="email" name="email" placeholder="E-mail" class="form-control">

						<button type="submit" class="btn btn-success" onClick="javascript:_gaq.push(['_trackEvent','Catalogo','Clique']);">Baixar o catálogo</button>

					</form>

					<h3 class="title-4">Dúvidas<span>frequentes</span></h3>

					<p class="duvida"><a href="duvidas-frequentes.php" title="Dúvidas frequentes"><span>Qual a diferença de um inflável de ar para o de gás hélio?</span><br /> 

						Infláveis de ar são inflados por um motoventilador e são fixados por cordas no solo ou em outros pontos fixos...</a></p>

					<p class="duvida"><a href="duvidas-frequentes.php" title="Dúvidas frequentes"><span>Quais infláveis utilizam o gás hélio?</span><br /> 

						Somente os infláveis que são produzidos no material em PVC, são eles: Blimp e zeppelin....</a></p>

				</section>
				<?php require_once './includes/form-contato.php'; ?>
			</div>

		</div>

		<!-- end depoimento/blog/contato -->




	</div>

		

		<!--maps -->



		<!-- end maps -->


	<?php require_once './includes/footer.php'; ?>

	