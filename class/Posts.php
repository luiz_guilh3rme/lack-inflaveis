<?php

require_once 'Conexao.php';

class Posts extends Conexao {

    public function listarPosts() {
        $pdo = parent::getDB();
        $consulta = $pdo->prepare("SELECT * FROM `wp_posts` WHERE `post_status` = 'publish' ORDER BY post_date DESC LIMIT 2");

        $consulta->execute();
        
        if($consulta->rowCount()) {
            return $consulta->fetchAll();
        }
        
        return FALSE;
    }
    
    public function capturarImagemThumbPost($idPost) {
        $pdo = parent::getDB();
        $consulta = $pdo->prepare("SELECT guid FROM `wp_posts` WHERE `post_parent` = ? AND `post_type` = 'attachment'");
        $consulta->bindValue(1, $idPost);
        $consulta->execute();
        
        if($consulta->rowCount()) {
            return $consulta->fetch(PDO::FETCH_COLUMN);
        }
        
        return FALSE;
    }

}

?>