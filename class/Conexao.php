<?php

abstract class Conexao {
    /* =-=-=-=-= SERVIDOR REMOTO =-=-=-=-= */
    //const HOST = '54.94.155.233';
    const HOST = 'localhost';
    const DBNAME = 'lackinfl_wp';
    const USER = 'lackinfl_admin';
    const PSWD = 'lack3033!!';
    /* =-=-=-=-= SERVIDOR REMOTO =-=-=-=-= */
    
    /* =-=-=-=-= SERVIDOR LOCAL =-=-=-=-= */
//    const HOST = 'localhost';
//    const DBNAME = 'metropolitansaopaulo';
//    const USER = 'root';
//    const PSWD = '';
    /* =-=-=-=-= SERVIDOR LOCAL =-=-=-=-= */
    
    private static $conn = NULL;

    public function __construct() {}

        private static function getConexao() {
        try {
            if (self::$conn == NULL) :
                $dsn = 'mysql:host=' . self::HOST . ';dbname=' . self::DBNAME;
                self::$conn = new PDO($dsn, self::USER, self::PSWD);
                self::$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                self::$conn->exec("SET CHARACTER SET UTF8");
            endif;
        } catch (Exception $erro) {
            exit('Erro: ' . $erro->getMessage());
        }

        return self::$conn;
    }
    
    protected static function getDB() {
        return self::getConexao();
    }

}

?>
