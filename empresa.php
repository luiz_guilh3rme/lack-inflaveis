	<?php 
		//SEO
		$title = 'Empresa de Infláveis | Lack Infláveis Peça Já Seu Orçamento ';
		$description = ' Lack Infláveis empresa especializada em Infláveis. Aproveite acesse e agora e solicite já o seu orçamento online do seu Inflável!';
		$canonicalTag = '<link rel="canonical” href="http://www.lackinflaveis.com.br/empresa.php"/>';
		$bg = "<div id=\"bg-interna-1\"></div>
			   <div id=\"bg-interna-2\" class=\"hidden-xs\"></div>";
	?>
	<?php require_once './includes/header.php'; ?>
		
		<section class="rows clearfix">			
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">				
				<div class="rows" id="info-produto">					
					<h1 class="text-center">Empresa</h1>
					<ol class="breadcrumb">
						<li><a href="/">Home</a></li>					
						<li class="active"><strong>Empresa</strong></li>
					</ol>
					<p>
						<h2>Quem Somos</h2>
						A Lack Infláveis é uma empresa especializada em infláveis promocionais e comunicação visual, apresentando uma alternativa criativa e diferenciada de divulgar sua empresa, produto ou serviço. Utilizamos o que há de melhor em matéria prima no mercado para a confecção de nossos trabalhos, na busca de alcançar a excelência de qualidade em todos os nossos produtos. <strong>Projetamos e confeccionamos infláveis gigantes promocionais motoventilados a partir de 2 metros de altura e diversos produtos de mini-infláveis, blimp e bola</strong>. Nossa produção se aplica a variedade de roof top, mascote, réplica, túnel, blimp, zépellim, portal, logotipo, fantasia, tenda, <strong>bateco</strong> ou qualquer outro projeto conforme a sua necessidade, e para isso temos uma grande estrutura física com uma equipe de profissionais que estão a mais de 20 anos no mercado de balões infláveis promocionais, preparados para oferecer todo suporte necessário para os projetos.<br /><br /><br /><br /><br /><br /><br /><br />

						Temos como princípio, cumprir com os desafios e surpreender com às expectativas criadas em torno de cada projeto apresentado. Esperamos conquistar a confiança, para assim fazer da Lack Infláveis uma empresa reconhecida na sua qualidade e no atendimento personalizado, trabalhando sempre com materiais de 1ª qualidade e serviço diferenciado.<br />

						Lack Infláveis, onde as ideias se concretizam.<br />

						<h2>Impressão Digital</h2>
						Possuímos impressão digital própria e utilizamos tintas a base de solventes ecológicos.<br /><br />

						A impressão é feita diretamente em tecidos específicos, PVC, lonas e adesivos.<br /><br />

						Dados do equipamento:<br />

						Roland, modelo RE-640;<br />
						Impressora solvente de 160 cm;<br />
						Alta velocidade de impressão até 23,1m²;<br />
						Alta resolução de até 1440 dpi;<br />
						Somente Tinta ORIGINAIS ECO-SOL MAX.<br /><br />

						<h2>Missão</h2>
						Fazer com que a ideia do nosso cliente ganhe vida e ele alcance os objetivos desejados.<br /><br />

						<h2>Visão</h2>
						Consolidar a liderança no mercado nacional por trabalhos realizados com alta qualidade, além de se tornar referência nos serviços de comunicação pelos seus cases de sucesso.<br /><br />

						<h2>Valores</h2>
						Qualidade;<br />
						Compromisso com o cliente;<br />
						Ética;<br />
						Integridade;<br />
						Respeito.<br />
					</p>										
				</div>
			</div>		
		</section>

		<!-- produtos -->
		<section class="rows clearfix">
			<h2 class="title-interna"><span id="line-3" class="hidden-xs hidden-sm"></span>Nossos <span>produtos</span></h2>
		</section>
		<section class="rows">
			<ul class="lista-produto">				
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="inflaveis/roof-tops.php" title="Roof Tops">
						<img src="imagens/produtos/roof-tops.png" alt="Roof Tops" class="img-responsive">
						<span class="grama"></span>						
						<h2>Roof Tops</h2>
					</a>
				</li>		
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="inflaveis/tendas.php" title="Tendas">
						<img src="imagens/produtos/tendas.png" alt="Tendas" class="img-responsive">
						<span class="asfalto"></span>						
						<h2>Tendas</h2>
					</a>
				</li>		
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="inflaveis/tuneis.php" title="Túneis">
						<img src="imagens/produtos/tuneis.png" alt="Túneis" class="img-responsive">
						<span class="grama"></span>						
						<h2>Túneis</h2>
					</a>
				</li>		
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="inflaveis/replicas.php" title="Réplicas">
						<img src="imagens/produtos/replicas.png" alt="Réplicas" class="img-responsive">
						<span class="asfalto"></span>						
						<h2>Réplicas</h2>
					</a>
				</li>		
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="inflaveis/blimp.php" title="Blimp">
						<img src="imagens/produtos/blimp.png" alt="Blimp" class="img-responsive">
						<span class="grama"></span>						
						<h2>Blimp</h2>
					</a>
				</li>
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="inflaveis/portais.php" title="Portais">
						<img src="imagens/produtos/portal.png" alt="Portais" class="img-responsive">
						<span class="asfalto"></span>						
						<h2>Portais</h2>
					</a>
				</li>	
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="inflaveis/mascotes.php" title="Mascotes">
						<img src="imagens/produtos/mascote.png" alt="Mascotes" class="img-responsive">
						<span class="grama"></span>						
						<h2>Mascotes</h2>
					</a>
				</li>	
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="inflaveis/totem.php" title="Totens">
						<img src="imagens/produtos/totems.png" alt="Totens" class="img-responsive">
						<span class="asfalto"></span>						
						<h2>Totens</h2>
					</a>
				</li>
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="inflaveis/bola.php" title="">
						<img src="imagens/produtos/bolas.png" alt="Bolas" class="img-responsive">
						<span class="grama"></span>						
						<h2>Bola</h2>
					</a>
				</li>	
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="inflaveis/tela-de-projecao.php" title="Telas de Projeção">
						<img src="imagens/produtos/telas-projecaos.png" alt="Telas de Projeção" class="img-responsive">
						<span class="asfalto"></span>						
						<h2>Telas de Projeção</h2>
					</a>
				</li>
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="inflaveis/stands.php" title="Stands">
						<img src="imagens/produtos/stand.png" alt="Stands" class="img-responsive">
						<span class="grama"></span>						
						<h2>Stands</h2>
					</a>
				</li>
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="inflaveis/logotipo.php" title="Logotipos">
						<img src="imagens/produtos/logotipos.png" alt="Logotipos" class="img-responsive">
						<span class="asfalto"></span>						
						<h2>Logotipos</h2>
					</a>
				</li>										
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="inflaveis/fantasia.php" title="Fantasias">
						<img src="imagens/produtos/fantasias.png" alt="Fantasias" class="img-responsive">
						<span class="grama"></span>						
						<h2>Fantasias</h2>
					</a>
				</li>				
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="inflaveis/painel.php" title="Painéis">
						<img src="imagens/produtos/paineis.png" alt="Painéis" class="img-responsive">
						<span class="grama"></span>						
						<h2>Painéis</h2>
					</a>
				</li>
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="inflaveis/mini.php" title="Mini-infláveis">
						<img src="imagens/produtos/mini.png" alt="Mini-infláveis" class="img-responsive">
						<span class="asfalto"></span>						
						<h2>Mini-infláveis</h2>
					</a>
				</li>
			</ul>
		</section>
		<!-- end produtos -->	
	
	<?php require_once './includes/duvidas-frequentes.php'; ?>
	</div>
	
	<div class="container-fluid" id="mapa-interna">
		<div id="bg-interna" class="hidden-xs hidden-sm"></div>
		<div id="map_canvas"></div>
		<div class="container z-index">
			<?php require_once './includes/form-contato.php'; ?>
		</div>
	</div>

	<?php require_once './includes/footer.php'; ?>
	