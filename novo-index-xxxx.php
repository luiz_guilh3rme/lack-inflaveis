<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link type="text/css" rel="stylesheet" href="css/CSS-home2/main.css" />
        <link href='https://fonts.googleapis.com/css?family=Lobster|Antic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="">
    </head>
    <body id="home2">

        <div id="topo">
            <div class="container">
                <div   itemscope itemtype="http://schema.org/Organization">


                    <header class="row">
                        <div class="col-lg-3" id="logo" data-type="logo" itemprop="logo">
                            colocar aqui o logotipo
                        </div>
                        <div class="col-lg-6" id="menu">
                            <ul>
                                <li>home</li>
                                <li>Lack Infláveis</li>
                                <li>produtos</li>
                                <li>contato</li>
                            </ul>
                        </div>
                        <div class=" col-lg-3 redesSociais">
                            <i class="fa fa-facebook-official" aria-hidden="true"></i>
                            <i class="fa fa-twitter" aria-hidden="true"></i>
                            <i class="fa fa-google-plus" aria-hidden="true"></i>
                            <i class="fa fa-flickr" aria-hidden="true"></i>
                        </div>
                    </header>

                    <main class="row">
                        <div id="slide" class=" col-12" itemprop="description">
                            A<strong itemprop="legalName">Lack Infláveis</strong> tem como maior preocupação a satisfação completa de cada cliente que traz seu projeto à 
                            nossa loja, independente do tamanho e da natureza do mesmo. Por isso, fazemos questão de cativá-los 
                            através da entrega da melhor solução para suas demandas.
                            <div class="btn">baixar o manual</div>
                            <div class="btn">baixar o catálogo</div>
                        </div>
                        <!--<div class="col-6"> e slide</div>-->
                    </main>
                </div>
            </div>
        </div>




        <div class="centro">
            <div class="container">
                <div id="orcamento">
                    <form>
                        <h2>Solicite orçamento</h2>
                        <b>Entraremos em contato com você o mais breve possível</b>
                        <label><input type="text" name="nome" required="required" placeholder="nome"></label>
                        <label><input type="text" name="tel" required="required" placeholder="tel"></label>
                        <label><input type="text" name="cel" required="required" placeholder="cel"></label>
                        <label><input type="text" name="e-mail" required="required" placeholder="e-mail"></label>
                        <label>
                            <select>
                                <option>42243</option>
                                <option>42243</option>
                                <option>42243</option>
                                <option>42243</option>
                                <option>42243</option>
                            </select>
                        </label>
                        <label><input type="text" name="tel" required="required" placeholder="nome"></label>
                        <label><input type="text" name="tel" required="required" placeholder="nome"></label>
                        <label><input type="text" name="tel" required="required" placeholder="nome"></label>
                        <label><input type="text" name="tel" required="required" placeholder="nome"></label>
                    </form>
                </div>
                <div class="row" id="produtos" itemscope itemtype="http://schema.org/Product">
                    <h2>outros produtos</h2>
                    <!--<ul>
                        <li>
                            <img itemprop="image" src="imagem" alt="" title="">
                            <h3 itemprop="name">nome do produto</h3>
                        </li>
                    </ul>-->
                </div>
                <div id="faq" class="row">
                    <!--<ul>
                        <li>
                            <div class="pergunta"></div>
                            <div class="resposta"></div>
                        </li>
                    </ul>-->
                </div>
                <div class="row" id="mapa"></div>
                <div class="row" id="rodape"></div>
            </div>
        </div>
    </body>
</html>
