	<?php 
		//SEO
		$title = '';
		$description = '';
		$bg = "<div id=\"bg-interna-1\" class=\"hidden-xs hidden-sm\"></div>
			   <div id=\"bg-interna-2\" class=\"hidden-xs hidden-sm\"></div>";
	?>
	<?php require_once './includes/header.php'; ?>
		
		<section class="rows clearfix">			
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">				
				<div class="rows" id="info-produto">					
					<h1 class="text-center">Sua mensagem foi <br /> enviada com sucesso !!!</h1>					
					<p>
						<ol class="breadcrumb">
							<li><a href="index.php">Home</a></li>					
							<li class="active">Mensagem</li>
						</ol>
					</p>										
				</div>
			</div>		
		</section>

		<!-- produtos -->
		<section class="rows clearfix"><br /><br /><br /><br />
			<h2 class="title-interna"><span id="line-3" class="hidden-xs hidden-sm"></span>Nossos <span>produtos</span></h2>
		</section>
		<section class="rows">
			<ul class="lista-produto">				
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="roof-tops.php" title="Roof Tops">
						<img src="../imagens/produtos/roof-tops.png" alt="Roof Tops" class="img-responsive">
						<span class="grama"></span>						
						<h2>Roof Tops</h2>
					</a>
				</li>		
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="tendas.php" title="Tendas">
						<img src="../imagens/produtos/tendas.png" alt="Tendas" class="img-responsive">
						<span class="asfalto"></span>						
						<h2>Tendas</h2>
					</a>
				</li>		
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="tuneis.php" title="Túneis">
						<img src="../imagens/produtos/tuneis.png" alt="Túneis" class="img-responsive">
						<span class="grama"></span>						
						<h2>Túneis</h2>
					</a>
				</li>		
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="replicas.php" title="Réplicas">
						<img src="../imagens/produtos/replicas.png" alt="Réplicas" class="img-responsive">
						<span class="asfalto"></span>						
						<h2>Réplicas</h2>
					</a>
				</li>		
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="blimp.php" title="Blimp">
						<img src="../imagens/produtos/blimp.png" alt="Blimp" class="img-responsive">
						<span class="grama"></span>						
						<h2>Blimp</h2>
					</a>
				</li>
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="portais.php" title="Portais">
						<img src="../imagens/produtos/portal.png" alt="Portais" class="img-responsive">
						<span class="asfalto"></span>						
						<h2>Portais</h2>
					</a>
				</li>	
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="mascotes.php" title="Mascotes">
						<img src="../imagens/produtos/mascote.png" alt="Mascotes" class="img-responsive">
						<span class="grama"></span>						
						<h2>Mascotes</h2>
					</a>
				</li>	
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="totem.php" title="Totens">
						<img src="../imagens/produtos/totems.png" alt="Totens" class="img-responsive">
						<span class="asfalto"></span>						
						<h2>Totens</h2>
					</a>
				</li>
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="bola.php" title="Bola">
						<img src="../imagens/produtos/bolas.png" alt="Bola" class="img-responsive">
						<span class="grama"></span>						
						<h2>Bola</h2>
					</a>
				</li>	
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="tela-de-projecao.php" title="Telas de Projeção">
						<img src="../imagens/produtos/telas-projecaos.png" alt="Telas de Projeção" class="img-responsive">
						<span class="asfalto"></span>						
						<h2>Telas de Projeção</h2>
					</a>
				</li>
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="stands.php" title="Stands">
						<img src="../imagens/produtos/stand.png" alt="Stands" class="img-responsive">
						<span class="grama"></span>						
						<h2>Stands</h2>
					</a>
				</li>
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="logotipo.php" title="Logotipos">
						<img src="../imagens/produtos/logotipos.png" alt="Logotipos" class="img-responsive">
						<span class="asfalto"></span>						
						<h2>Logotipos</h2>
					</a>
				</li>										
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="fantasia.php" title="Fantasias">
						<img src="../imagens/produtos/fantasias.png" alt="Fantasias" class="img-responsive">
						<span class="grama"></span>						
						<h2>Fantasias</h2>
					</a>
				</li>				
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="painel.php" title="Painéis">
						<img src="../imagens/produtos/paineis.png" alt="Painéis" class="img-responsive">
						<span class="grama"></span>						
						<h2>Painéis</h2>
					</a>
				</li>
				
			</ul>
		</section>
		<!-- end produtos -->	
	
	<?php require_once './includes/duvidas-frequentes.php'; ?>
	</div>
	
	<div class="container-fluid" id="mapa-interna">
		<div id="bg-interna" class="hidden-xs hidden-sm"></div>
		<div id="map_canvas"></div>
		<div class="container z-index">
			<?php require_once './includes/form-contato.php'; ?>
		</div>
	</div>
	
	<!-- Google Code for Covers&atilde;o Conversion Page -->
	<script type="text/javascript">
	/* <![CDATA[ */
	var google_conversion_id = 1000192907;
	var google_conversion_language = "en";
	var google_conversion_format = "2";
	var google_conversion_color = "ffffff";
	var google_conversion_label = "BEVeCKmew1YQi_f23AM";
	var google_remarketing_only = false;
	/* ]]> */
	</script>
	<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
	</script>
	<noscript>
	<div style="display:inline;">
	<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1000192907/?label=BEVeCKmew1YQi_f23AM&amp;guid=ON&amp;script=0"/>
	</div>
	</noscript>

<!-- Google Code for Convers&otilde;es formul&aacute;rio de contato Conversion Page   novo-->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 948560181;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "uUZ_CIy2n14QtcKnxAM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/948560181/?label=uUZ_CIy2n14QtcKnxAM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>


	<?php require_once './includes/footer.php'; ?>
	