	<?php 
		//SEO
		$title = '';
		$description = 'Erro 404 Página não encontrada';
		$bg = "<div id=\"bg-interna-1\" class=\"hidden-xs hidden-sm\"></div>
			   <div id=\"bg-interna-2\" class=\"hidden-xs hidden-sm\"></div>";
	?>
	<?php require_once './includes/header.php'; ?>
		
		<section class="rows clearfix">			
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">				
				<div class="rows" id="info-produto">					
					<h1 class="text-center">Erro 404.... <br /> Página não encontrada.</h1>					
					<p>
						<ol class="breadcrumb">
							<li><a href="index.php">Home</a></li>					
							<li class="active">404</li>
						</ol>						
					</p>										
				</div>
			</div>		
		</section>

		<!-- produtos -->
		<section class="rows clearfix"><br /><br /><br /><br />
			<h2 class="title-interna"><span id="line-3" class="hidden-xs hidden-sm"></span>Nossos <span>produtos</span></h2>
		</section>
		<section class="rows">
			<ul class="lista-produto">				
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="inflaveis/roof-tops.php" title="">
						<img src="imagens/produtos/roof-tops.png" alt="" class="img-responsive">
						<span class="grama"></span>						
						<h2>Roof Tops</h2>
					</a>
				</li>		
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="inflaveis/tendas.php" title="">
						<img src="imagens/produtos/roof-tops.png" alt="" class="img-responsive">
						<span class="asfalto"></span>						
						<h2>Tendas</h2>
					</a>
				</li>		
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="inflaveis/tuneis.php" title="">
						<img src="imagens/produtos/tuneis.png" alt="" class="img-responsive">
						<span class="grama"></span>						
						<h2>Túneis</h2>
					</a>
				</li>		
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="inflaveis/replicas.php" title="">
						<img src="imagens/produtos/replicas.png" alt="" class="img-responsive">
						<span class="asfalto"></span>						
						<h2>Réplicas</h2>
					</a>
				</li>		
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="inflaveis/blimp.php" title="">
						<img src="imagens/produtos/blimp.png" alt="" class="img-responsive">
						<span class="grama"></span>						
						<h2>Blimp</h2>
					</a>
				</li>
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="inflaveis/portais.php" title="">
						<img src="imagens/produtos/portal.png" alt="" class="img-responsive">
						<span class="asfalto"></span>						
						<h2>Portais</h2>
					</a>
				</li>	
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="inflaveis/mascotes.php" title="">
						<img src="imagens/produtos/mascote.png" alt="" class="img-responsive">
						<span class="grama"></span>						
						<h2>Mascotes</h2>
					</a>
				</li>	
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="inflaveis/totem.php" title="">
						<img src="imagens/produtos/roof-tops.png" alt="" class="img-responsive">
						<span class="asfalto"></span>						
						<h2>Totens</h2>
					</a>
				</li>
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="inflaveis/bola.php" title="">
						<img src="imagens/produtos/roof-tops.png" alt="" class="img-responsive">
						<span class="grama"></span>						
						<h2>Bola</h2>
					</a>
				</li>	
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="inflaveis/tela-de-projecao.php" title="">
						<img src="imagens/produtos/roof-tops.png" alt="" class="img-responsive">
						<span class="asfalto"></span>						
						<h2>Telas de Projeção</h2>
					</a>
				</li>
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="inflaveis/stands.php" title="">
						<img src="imagens/produtos/roof-tops.png" alt="" class="img-responsive">
						<span class="grama"></span>						
						<h2>Stands</h2>
					</a>
				</li>
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="inflaveis/logotipo.php" title="">
						<img src="imagens/produtos/roof-tops.png" alt="" class="img-responsive">
						<span class="asfalto"></span>						
						<h2>Logotipos</h2>
					</a>
				</li>										
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="inflaveis/fantasia.php" title="">
						<img src="imagens/produtos/roof-tops.png" alt="" class="img-responsive">
						<span class="grama"></span>						
						<h2>Fantasias</h2>
					</a>
				</li>				
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="inflaveis/painel.php" title="">
						<img src="imagens/produtos/roof-tops.png" alt="" class="img-responsive">
						<span class="grama"></span>						
						<h2>Painéis</h2>
					</a>
				</li>
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="inflaveis/mini.php" title="">
						<img src="imagens/produtos/mini.png" alt="" class="img-responsive">
						<span class="asfalto"></span>						
						<h2>Mini-infláveis</h2>
					</a>
				</li>
			</ul>
		</section>
		<!-- end produtos -->	
	
	<?php require_once './includes/duvidas-frequentes.php'; ?>
	</div>
	
	<div class="container-fluid" id="mapa-interna">
		<div id="bg-interna" class="hidden-xs hidden-sm"></div>
		<div id="map_canvas"></div>
		<div class="container z-index">
			<?php require_once './includes/form-contato.php'; ?>
		</div>
	</div>

	<?php require_once './includes/footer.php'; ?>
	