$(document).ready(function(){
	event.stopPropagation(); // Stop stuff happening
    event.preventDefault(); // Totally stop stuff happening
	var sitePath = window.location.pathname.split("/");
	
	if(sitePath.lastIndexOf("index.php") == 1 || sitePath.lastIndexOf("") == 1){
		//parallax	
		var scene = document.getElementById('parallax');
		var parallax = new Parallax(scene);	

		//$("#nav .nav li:nth-child(4)").click(function(a){
		//	a.preventDefault();
		//	var scroll = $("section.rows.clearfix").offset().top
		//	$("html, body").animate({
		//		scrollTop: scroll
		//	}, 500);
		//});		
	}else{
		//$("#nav .nav li:nth-child(4)").click(function(a){
		//	a.preventDefault();
		//	var scroll = $("section.rows.clearfix").eq(1).offset().top
		//	$("html, body").animate({
		//		scrollTop: scroll
		//	}, 500);
		//});		
	}	

	$("#nav .nav .contato").click(function(a){
		a.preventDefault();
		var scroll = $("#form-footer").offset().top
		$("html, body").animate({
			scrollTop: scroll
		}, 500);
	});

	$("#info-produto > a").click(function(a){
		a.preventDefault();
		var scroll = $("#form-footer").offset().top
		$("html, body").animate({
			scrollTop: scroll
		}, 500);
	});

	$('.tel').mask('(99) 9999-9999?9');

	jQuery.validator.addMethod("testEmail", function(value, element) {
	  return this.optional(element) || /[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\.[a-zA-Z]{2,4}/i.test(value);
	}, "Digite e-mail valido.");



	$("#form-footer").validate({
		rules: {
			    nome: {
			      required: true,
			      minlength: 2
			    },
			    empresa: {
			      required: true
			    },
			    email: {
			      required: true,
			      email: true,
			      testEmail: true
			    },
			    tel: {
			      required: true
			    },
			    cel: {
			      required: true
			    },
			    estado: {
			      required: true
			    },
			    cidade: {
			      required: true
			    },
			    produto: {
			      required: true
			    },
			    medidas: {
			      required: true
			    },
			    qtdade: {
			      required: true,
			      number: true
			    },
			    msg: {
			      required: true
			    }
		},messages: {
			  	 nome: {
			  	 	required: "Digite seu nome.",
					minlength: "Digite seu nome completo."
			  	 },
			  	 empresa: {
			  	 	required: "Digite nome da empresa"
			  	 },
			  	 email: {
			  	 	required: "Digite seu e-mail.",
					email: "Digite e-mail valido."
			  	 }, 
			  	 tel: {
			  	 	required: "Digite seu telefone."
			  	 }, 
			  	 cel: {
			  	 	required: "Digite seu celular."
			  	 }, 
			  	 estado: {
			  	 	required: "Selecione estado"
			  	 }, 
			  	 cidade: {
			  	 	required: "Digite sua cidade."
			  	 }, 
			  	 produto: {
			  	 	required: "Selecione o produto"
			  	 }, 
			  	 medidas: {
			  	 	required: "Informe medida"
			  	 }, 
			  	 qtdade: {
			  	 	required: "Informe quantidade",
			  	 	number: "Digite apenas numero"
			  	 }, 
			  	 msg: {
			  	 	required: "Digite a mensagem"
			  	 }
		},submitHandler: function( form ){
				var host =  "http://www.lackinflaveis.com.br/includes/envia-contato.php";
				var data = new FormData($(form)[0]);
				
				$.ajax({
					type: 'POST',
					url: host,
	                data: data,
	                cache: false,
	                async:false,
	                processData: false,
	                contentType: false,
					success: function( data ){													
						window.location ="http://www.lackinflaveis.com.br/obrigado.php";
					}
			});
		return false;
		}
	});

$("#form-catalogo").validate({
		rules: {
			    nome: {
			      required: true,
			      minlength: 2
			    },
			    email: {
			      required: true,
			      email: true,
			      testEmail: true
			    }
		},messages: {
			  	 nome: {
			  	 	required: "Digite seu nome.",
					minlength: "Digite seu nome completo."
			  	 },
			  	 email: {
			  	 	required: "Digite seu e-mail.",
					email: "Digite e-mail valido."
			  	 }
		},submitHandler: function( form ){
				var dados = $( form ).serialize();
				var host =  "http://www.lackinflaveis.com.br/includes/envia-catalogo.php";
				$.ajax({
					type: "POST",
					url: host,
					async: true,
					data: dados,
					success: function( data ){				
						window.location ="http://www.lackinflaveis.com.br/sucesso-catalogo.php";
					}
			});

		return false;
		}
	});
	
	$(".box-expresso > div > button").click(function(){		
		
		if($(this).hasClass('active')){
			$(".box-expresso").animate({
				right:"-225px"
			},500);
			$(this).removeClass("active");
		}else{
			$(".box-expresso").animate({
				right:"0px"
			},500);
			$(this).addClass("active");
		}		
	});

	$("#form-expresso").validate({
		rules: {
			    nome: {
			      required: true,
			      minlength: 2
			    },
			    email: {
			      required: true,
			      email: true,
			      testEmail: true
			    },
			    msg: {
			      required: true
			    }
		},messages: {
			  	 nome: {
			  	 	required: "Digite seu nome.",
					minlength: "Digite seu nome completo."
			  	 },
			  	 email: {
			  	 	required: "Digite seu e-mail.",
					email: "Digite e-mail valido."
			  	 },
			  	 email: {
			  	 	required: "Digite sua mensagem"
			  	 }
		},submitHandler: function( form ){
				var dados = $( form ).serialize();
				var host =  "http://www.lackinflaveis.com.br/includes/envia-expresso.php";
				$.ajax({
					type: "POST",
					url: host,
					async: true,
					data: dados,
					success: function( data ){				
						window.location ="http://www.lackinflaveis.com.br/sucesso-expresso.php";
					}
			});

		return false;
		}
	});

	var slider = $('#galeria-foto').lightSlider({
      gallery:true,
      slideWidth:270,
      minSlide:1,
      maxSlide:1,
      thumbWidth:112.2,
      controls:false,
      slideMargin:0,
      thumbMargin:10,
      auto: false
    });  

    $('#slide-v').click(function(){
        slider.goToPrevSlide(); 
    });
    $('#slide-a').click(function(){
        slider.goToNextSlide(); 
    });

    $(".fancybox-thumb").fancybox({
		prevEffect	: 'none',
		nextEffect	: 'none',
		helpers	: {
			title	: {
				type: 'outside'
			},
			thumbs	: {
				width	: 100,
				height	: 100
			}
		}
	});

	//$("#download-link").click(function(e){
	//	e.preventDefault();		    	
    //	window.open('http://www.lackinflaveis.com.br/arquivos/catalogo-lack-inflaveis.pdf');
    //	window.location = "http://www.lackinflaveis.com.br/sucesso-catalogo.php";
	//});

	// MENU DROPDOWN
	$('.navbar li.dropdown').hover(function(){
		$('ul', this).stop().slideToggle(500);
	});

	$('.navbar-toggle').click(function(){
		$('.navbar').toggleClass('open');
	});

});