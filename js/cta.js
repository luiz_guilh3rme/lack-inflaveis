function WrapperCtaVisible() {
    $(".call-cta-wrapper").addClass("visible");
    $(".call-cta-wrapper").css({
        "opacity": "1",
        "visibility": "visible"
    });
}
function WrapperCtaInvisible() {
    $(".call-cta-wrapper").removeClass("visible");
    $(".call-cta-wrapper").css({
        "opacity": "0",
        "visibility": "hidden"
    });
}
  
function handleWhatsappCta() {
	$(this).parent('.whatsapp-cta').addClass('clicked');
}

function handleCallCta() {
	$(this).addClass('clicked');
	$(".call-tooltip").addClass('clicked');
	$(".open-call-tooltip").addClass('clicked');
}
function callTooltipCtaVisible(e) {
	$(".close-call-cta").removeClass('clicked');
	$(".call-tooltip").removeClass('clicked');
	$(".open-call-tooltip").removeClass('clicked');
	e.preventDefault();
}

function handleMobileCta() {
	$(this).parent('.whatsapp-tooltip').addClass('clicked');
}

function openPhoneCta() {
	// duhh
	$('.open-cta').on('click', function () {
		$('.overlay').fadeIn(400, function () {
			$('.form-wrapper-all').fadeIn();
		});


		// create new date on button click
		var now = new Date(),
			days = ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
			day = days[now.getDay()],
			hour = now.getHours(),
      picker = $('.form-pickers[data-instance=00]');

		// check if it is outside business hours
		if (day === 'Domingo' || day === 'Sábado' || hour > 18 || hour < 9) {
			picker[0].disabled = true;
			picker[0].title = "Disponível apenas em horário comercial";
			picker[0].innerHTML = "<i class='fa fa-phone'></i> APENAS  EM HORÁRIO COMERCIAL";
    }
	});
}

function formPickers() {
	$('.form-pickers').removeClass('active');
	$(this).addClass('active');
	var instance = $(this).data('instance');

	$('.instance:not([data-instance=' + instance + '])').fadeOut(400, function () {
		setTimeout(function () {
			$('.instance[data-instance=' + instance + ']').fadeIn()
		}, 400)
	});
}

function closePhoneCta() {
	$('.close-modal').on('click', function () {
		$('.form-wrapper-all').fadeOut(400, function () {
			$('.overlay').fadeOut();
		});
	});
}

function randomizeRequests(min, max) {
  var random = Math.floor(Math.random() * (max - min + 1)) + min;
  return random;
}

function printNumbers() {
  var numbers = document.querySelectorAll('.number');
  for (var i = 0; i < numbers.length; i++) {
      numbers[i].innerHTML = randomizeRequests(1, 12);
  }
}
function disabledButton(){
    var submitButton = $("form button[type='submit']");            
    submitButton.html('ENVIANDO...');
    submitButton.prop('disabled', true);
}

printNumbers();
openPhoneCta();
closePhoneCta();
WrapperCtaVisible();

$(".form-pickers").on("click", formPickers);
$('.close-cta').on('click', handleWhatsappCta);
$('.close-call-cta').on('click', handleCallCta);
$('.open-call-tooltip').on('click', callTooltipCtaVisible);

$("#form-deixe-mensagem").validate({
    rules: {
            leaveMessageUserName: {
              required: true,
              minlength: 2
            },
            leaveMessageUserEmail: {
              required: true,
              email: true,
              testEmail: true
            },
            leaveMessageUserPhone: {
              required: true
            }
    },messages: {
                leaveMessageUserName: {
                    required: "Digite seu nome.",
                    minlength: "Digite seu nome completo."
               },
               leaveMessageUserEmail: {
                   required: "Digite seu e-mail.",
                    email: "Digite e-mail valido."
               },
               leaveMessageUserName:{
                required: "Digite um número de telefone."
               }
    },submitHandler: function( form ){
            var dados = $( form ).serialize();
            disabledButton();
            var host =  "https://www.lackinflaveis.com.br/includes/envia-deixe-mensagem.php";
            $.ajax({
                type: "POST",
                url: host,
                async: true,
                data: dados,
                success: function( data ){				
                    window.location ="https://www.lackinflaveis.com.br/obrigado.php";
                }
        });

    return false;
    }
});

$("#form-we-call").validate({
    rules: {
            weCallUserName: {
              required: true,
              minlength: 2
            },
            weCallUserPhone: {
              required: true
            }
    },messages: {
                weCallUserName: {
                    required: "Digite seu nome.",
                    minlength: "Digite seu nome completo."
               },
               weCallUserPhone:{
                required: "Digite um número de telefone."
               }
    },submitHandler: function( form ){
            var dados = $( form ).serialize();
            disabledButton();
            var host =  "https://www.lackinflaveis.com.br/includes/envia-ligue-agora.php";
            $.ajax({
                type: "POST",
                url: host,
                async: true,
                data: dados,
                success: function( data ){				
                    window.location ="https://www.lackinflaveis.com.br/obrigado.php";
                }
        });

    return false;
    }
});

$("#form-schedule").validate({
    rules: {
        scheduleUserName: {
              required: true,
              minlength: 2
            },
            scheduleUserEmail: {
              required: true,
              email: true,
              testEmail: true
            },
            scheduleUserDate: {
              required: true
            },
            scheduleUserPhone: {
              required: true
            }
    },messages: {
                scheduleUserName: {
                    required: "Digite seu nome.",
                    minlength: "Digite seu nome completo."
               },
               scheduleUserEmail: {
                   required: "Digite seu e-mail.",
                    email: "Digite e-mail valido."
               },
               scheduleUserDate: {
                   required: "Selecione uma data."
               },
               scheduleUserName:{
                required: "Digite um número de telefone."
               }
    },submitHandler: function( form ){
            var dados = $( form ).serialize();
            disabledButton();
            var host =  "https://www.lackinflaveis.com.br/includes/envia-ligue-depois.php";
            $.ajax({
                type: "POST",
                url: host,
                async: true,
                data: dados,
                success: function( data ){				
                    window.location ="https://www.lackinflaveis.com.br/obrigado.php";
                }
        });

    return false;
    }
});