	<?php 
		//SEO
		$title = 'Dúvidas Frequentes | Lack Infláveis Peça Já Seu Orçamento ';
		$description = 'Acesse agora e tire suas dúvidas e solcite seu orçamento. Lack Infláveis Empresa Especializada em Infláveis  Confira';
		$canonicalTag = '<link rel="canonical” href="http://www.lackinflaveis.com.br/duvidas-frequentes.php"/>';
		$bg = "<div id=\"bg-interna-1\"></div>
			   <div id=\"bg-interna-2\" class=\"hidden-xs hidden-sm\"></div>";
	?>
	<?php require_once './includes/header.php'; ?>
		
		<section class="rows clearfix">			
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">				
				<div class="rows" id="info-produto">					
					<h1 class="text-center">Dúvidas frequentes</h1>				
					
					<ol class="breadcrumb">
						<li><a href="/">Home</a></li>					
						<li class="active"><strong>Dúvidas frequentes</strong></li>
					</ol>
					<p>
						<h2>Dúvidas sobre produtos infláveis</h2>
						<h3>Qual a diferença de um inflável de ar para o de gás hélio?</h3>
						R: Infláveis de ar são inflados por um motoventilador e são fixados por cordas no solo ou em outros pontos fixos (árvores, postes, grades, etc). Infláveis de gás hélio flutuam no céu fixado por cordas a uma altura de até 30m.<br /><br /><br /><br /><br /><br /><br /><br />

						<h3>Quais infláveis utilizam o gás hélio?</h3>
						R: Somente os infláveis que são produzidos no material em PVC, são eles: Blimp e zeppelin.

						<h3>Um inflável de ar frio, por exemplo o roof top, pode ser cheio com gás hélio e, consequentemente, flutuar?</h3>
						Não. Infláveis de ar e de gás hélio são produtos diferentes, onde os métodos de produção e matéria prima são totalmente diferentes um do outro.

						<h3>Qual a vida útil de um inflável?</h3>
						R: Existem diversos infláveis produzidos pela Lack que estão sendo utilizados há anos. Mas o principal requisito é o cuidado que terá com o inflável, assim você ira prolongar a vida útil por anos.

						<h3>Qual o consumo do motor de um inflável?</h3>
						R: O motoventilador é 220V, sendo 230W de potência.

						<h3>O inflável precisa ficar com o motor ligado?</h3>
						R: Sim, as peças de tecido do inflável são costuradas, assim o motor precisa ficar ligado constantemente. A vantagem desse método é que ele impede que os atos de vandalismo ou imprevistos façam com que o inflável venha a murchar. Isso apenas não se aplica para Bola Show e Blimp, pois são produzidos em PVC e não utilizam motoventilador.

						<h3>O que pode assegurar a perfeição na reprodução do formato do inflável?</h3>
						R: Os infláveis são produzidos com auxilio de software 3D assegurando a precisão na reprodução de imagens tridimensionais. E sempre exigimos que seja mantido o padrão de qualidade já estabelecido em todo o nosso processo de fabricação.

						<h3>Qual modelo de inflável trará melhores resultados para meu projeto de Marketing?</h3>
						R: Um representante no atendimento indicará qual opção de tamanho e modelo, ira melhor atende-lo dentro das suas necessidades.

						<h3>Como enviar uma imagem para a produção do inflável?</h3>
						Os arquivos podem ser enviados em diversos tipos de arquivos, porém os principais são: .cdr, .psd, .ai, .jpg, .tif, .pdf e .eps.

						<h3>Quais produtos não são necessários o layout?</h3>
						São as réplicas em formatos especiais, mascotes e fantasias 3D, nesses casos solicitaremos a imagem para o cliente ou a amostra física do produto para tirarmos as medidas e reproduzir com a maior finalidade e detalhes possíveis.

						<h2>Roof Tops</h2>
						<h3>Qual a diferença de um inflável de ar para o de gás hélio?</h3>
						Infláveis de ar são inflados por um motoventilador e são fixados por cordas no solo ou em outros pontos fixos (árvores, postes, grades, etc). Infláveis de gás hélio flutuam no céu fixado por cordas a uma altura de até 30m.

						<h3>O Roof Top pode ser inflado com gás hélio e, consequentemente, flutuar?</h3>
						Não. Ele é um inflável produzido para infla-lo por um motoventilador que já o acompanha e ser instalado no solo, alguma base, em cima de caçambas de automóveis, etc.

						<h3>O inflável precisa ficar com o motor ligado? </h3>
						Sim, as peças de tecido do inflável são costuradas, assim o motor precisa ficar ligado constantemente. A vantagem desse método é que ele impede que os atos de vandalismo ou imprevistos façam com que o inflável venha a murchar. Isso apenas não se aplica para Bola Show e Blimp, pois são produzidos em PVC e não utilizam motoventilador.

						<h2>Tendas; Túneis; Portais; Totens; Telas de Projeção; Stands; Logotipos; Painéis.</h2>
						<h3>Qual a vida útil de um inflável?</h3>
						Existem diversos infláveis produzidos pela Lack que estão sendo utilizados há anos. Mas o principal requisito é o cuidado que terá com o inflável, assim você ira prolongar a vida útil por anos.

						<h3>O inflável precisa ficar com o motor ligado?</h3>
						Sim, as peças de tecido do inflável são costuradas, assim o motor precisa ficar ligado constantemente. A vantagem desse método é que ele impede que os atos de vandalismo ou imprevistos façam com que o inflável venha a murchar. Isso apenas não se aplica para Bola Show e Blimp, pois são produzidos em PVC e não utilizam motoventilador.

						<h3>Qual modelo de inflável trará melhores resultados para meu projeto de Marketing?</h3>
						Um representante no atendimento indicará qual opção de tamanho e modelo, ira melhor atende-lo dentro das suas necessidades.

						<h2>Blimp</h2>
						<h3>Qual a diferença de um inflável de ar para o de gás hélio?</h3>
						Infláveis de ar são inflados por um motoventilador e são fixados por cordas no solo ou em outros pontos fixos (árvores, postes, grades, etc). Infláveis de gás hélio flutuam no céu fixado por cordas a uma altura de até 30m.

						<h3>Qual a dimensão do blimp para flutuar com gás hélio?</h3>
						Devera medir no mínimo 2,5m de diâmetro. Que é a dimensão necessária para que carregue a cubagem mínima de gás hélio para fazer com o que blimp flutue.

						<h3>O Blimp além de ser inflado com gás hélio para flutuar, pode inflar com ar normal?</h3>
						Sim. Utilizando um equipando como: soprador, compressor, etc. Você pode inflar e utilizar ele no solo, em alguma base, torre, como cenografia, etc

						<h2>Bola</h2>
						<h3>A bola pode ser inflada com gás hélio?</h3>
						Não. Apesar de ser produzida com o mesmo material do blimp, a bola é produzida a partir de 1m de diâmetro. Para que possa flutuar ela deveria ter uma dimensão maior, se tornando o blimp.

						<h3>Qual a vida útil de um inflável?</h3>
						Existem diversos infláveis produzidos pela Lack que estão sendo utilizados há anos. Mas o principal requisito é o cuidado que terá com o inflável, assim você ira prolongar a vida útil por anos. 

						<h3>A bola acompanha motoventilador?</h3>
						Não. A bola devera ser inflada por sua válvula ou entrada de ar utilizando um equipando como: soprador, compressor, etc.

						<h2>Fantasias; Réplicas; Mascotes</h2>
						<h3>O que pode assegurar a perfeição na reprodução do formato do inflável?</h3>
						Os infláveis são produzidos com auxilio de software 3D assegurando a precisão na reprodução de imagens tridimensionais. E sempre exigimos que seja mantido o padrão de qualidade já estabelecido em todo o nosso processo de fabricação.

						<h3>Qual modelo de inflável trará melhores resultados para meu projeto de Marketing?</h3>
						Um representante no atendimento indicará qual opção de tamanho e modelo, ira melhor atende-lo dentro das suas necessidades.

						<h3>Quais produtos não são necessários o layout?</h3>
						São as réplicas em formatos especiais, mascotes e fantasias 3D, nesses casos solicitaremos a imagem para o cliente ou a amostra física do produto para tirarmos as medidas e reproduzir com a maior finalidade e detalhes possíveis.

						<h2>Mini-infláveis</h2>
						<h3>Em qual situação é aconselhado utilizar o mini-inflável?</h3>
						São utilizados como brinde promocional de sua campanha, podendo animar torcidas em estádios, show e realçar o seu evento.

						<h3>Qual modelo de inflável trará melhores resultados para meu projeto de Marketing?</h3>
						Um representante no atendimento indicará qual opção de tamanho e modelo, ira melhor atende-lo dentro das suas necessidades.

						<h3>O que pode assegurar a perfeição na reprodução do formato do inflável?</h3>
						Os infláveis são produzidos com auxilio de software 3D assegurando a precisão na reprodução de imagens tridimensionais. E sempre exigimos que seja mantido o padrão de qualidade já estabelecido em todo o nosso processo de fabricação.

						
					</p>									
				</div>
			</div>		
		</section>

		<!-- produtos -->
		<section class="rows clearfix">
			<h2 class="title-interna"><span id="line-3" class="hidden-xs hidden-sm"></span>Nossos <span>produtos</span></h2>
		</section>
		<section class="rows">
			<ul class="lista-produto">				
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="roof-tops.php" title="Roof Tops">
						<img src="../imagens/produtos/roof-tops.png" alt="Roof Tops" class="img-responsive">
						<span class="grama"></span>						
						<h2>Roof Tops</h2>
					</a>
				</li>		
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="tendas.php" title="Tendas">
						<img src="../imagens/produtos/tendas.png" alt="Tendas" class="img-responsive">
						<span class="asfalto"></span>						
						<h2>Tendas</h2>
					</a>
				</li>		
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="tuneis.php" title="Túneis">
						<img src="../imagens/produtos/tuneis.png" alt="Túneis" class="img-responsive">
						<span class="grama"></span>						
						<h2>Túneis</h2>
					</a>
				</li>		
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="replicas.php" title="Réplicas">
						<img src="../imagens/produtos/replicas.png" alt="Réplicas" class="img-responsive">
						<span class="asfalto"></span>						
						<h2>Réplicas</h2>
					</a>
				</li>		
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="blimp.php" title="Blimp">
						<img src="../imagens/produtos/blimp.png" alt="Blimp" class="img-responsive">
						<span class="grama"></span>						
						<h2>Blimp</h2>
					</a>
				</li>
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="portais.php" title="Portais">
						<img src="../imagens/produtos/portal.png" alt="Portais" class="img-responsive">
						<span class="asfalto"></span>						
						<h2>Portais</h2>
					</a>
				</li>	
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="mascotes.php" title="Mascotes">
						<img src="../imagens/produtos/mascote.png" alt="Mascotes" class="img-responsive">
						<span class="grama"></span>						
						<h2>Mascotes</h2>
					</a>
				</li>	
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="totem.php" title="Totens">
						<img src="../imagens/produtos/totems.png" alt="Totens" class="img-responsive">
						<span class="asfalto"></span>						
						<h2>Totens</h2>
					</a>
				</li>
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="bola.php" title="Bola">
						<img src="../imagens/produtos/bolas.png" alt="Bola" class="img-responsive">
						<span class="grama"></span>						
						<h2>Bola</h2>
					</a>
				</li>	
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="tela-de-projecao.php" title="Telas de Projeção">
						<img src="../imagens/produtos/telas-projecaos.png" alt="Telas de Projeção" class="img-responsive">
						<span class="asfalto"></span>						
						<h2>Telas de Projeção</h2>
					</a>
				</li>
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="stands.php" title="Stands">
						<img src="../imagens/produtos/stand.png" alt="Stands" class="img-responsive">
						<span class="grama"></span>						
						<h2>Stands</h2>
					</a>
				</li>
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="logotipo.php" title="Logotipos">
						<img src="../imagens/produtos/logotipos.png" alt="Logotipos" class="img-responsive">
						<span class="asfalto"></span>						
						<h2>Logotipos</h2>
					</a>
				</li>										
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="fantasia.php" title="Fantasias">
						<img src="../imagens/produtos/fantasias.png" alt="Fantasias" class="img-responsive">
						<span class="grama"></span>						
						<h2>Fantasias</h2>
					</a>
				</li>				
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="painel.php" title="Painéis">
						<img src="../imagens/produtos/paineis.png" alt="Painéis" class="img-responsive">
						<span class="grama"></span>						
						<h2>Painéis</h2>
					</a>
				</li>
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="mini-inflaveis.php" title="Mini-infláveis">
						<img src="../imagens/produtos/mini.png" alt="Mini-infláveis" class="img-responsive">
						<span class="asfalto"></span>						
						<h2>Mini-infláveis</h2>
					</a>
				</li>
			</ul>
		</section>
		<!-- end produtos -->	
	
	<?php require_once './includes/duvidas-frequentes.php'; ?>
	</div>
	
	<div class="container-fluid" id="mapa-interna">
		<div id="bg-interna" class="hidden-xs hidden-sm"></div>
		<div id="map_canvas"></div>
		<div class="container z-index">
			<?php require_once './includes/form-contato.php'; ?>
		</div>
	</div>

	<?php require_once './includes/footer.php'; ?>
	