<!DOCTYPE HTML>
<html lang="pt-br">
<head>
  	<meta charset="UTF-8">
	<title><?php echo $title; ?></title>
	<meta name="description" content="<?php echo $description; ?>">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<link href='http://fonts.googleapis.com/css?family=Lobster|Antic' rel='stylesheet' type='text/css'>
	<link type="text/css" rel="stylesheet" href="/min/b=css&amp;f=bootstrap.min.css,jquery.fancybox.css,jquery.fancybox-thumbs.css,style.css" />

</head>
<body id="home">
	<?php echo $bg; ?>
	
	<div class="box-expresso hidden-xs hidden-sm">
		<div class="contato-expresso">
			<button onClick="javascript:_gaq.push(['_trackEvent','entre em contato','click']);"><span class="hide">Enviar</span></button>
			<div style="text-align:center;width:80px;"><!-- LiveZilla Chat Button Link Code (ALWAYS PLACE IN BODY ELEMENT) --><!-- LiveZilla Tracking Code (ALWAYS PLACE IN BODY ELEMENT) --><div id="livezilla_tracking" style="display:none"></div><script type="text/javascript">
var script = document.createElement("script");script.async=true;script.type="text/javascript";var src = "http://lackinflaveis.com.br/chat/server.php?a=1341a&request=track&output=jcrpt&nse="+Math.random();setTimeout("script.src=src;document.getElementById('livezilla_tracking').appendChild(script)",1);</script><noscript><img src="http://lackinflaveis.com.br/chat/server.php?a=1341a&amp;request=track&amp;output=nojcrpt" width="0" height="0" style="visibility:hidden;" alt=""></noscript><!-- http://www.LiveZilla.net Tracking Code --><a href="javascript:void(window.open('http://lackinflaveis.com.br/chat/chat.php?a=fa6d8','','width=590,height=760,left=0,top=0,resizable=yes,menubar=no,location=no,status=yes,scrollbars=yes'))" class="lz_cbl" onClick="javascript:_gaq.push(['_trackEvent','Click botão enviar','Enviar']);"><img src="http://lackinflaveis.com.br/chat/image.php?a=ae0e7&amp;id=3&amp;type=inlay" width="80" height="80" style="border:0px;" alt="LiveZilla Live Chat Software"></a></div>
			<div class="rows">
				<form action="" method="post" name="form-expresso" id="form-expresso" class="col-lg-12 col-md-12">											
					<h4>Entre em contato</h4>											
					<div class="form-group">						
						<input type="text" class="form-control" name="nome" placeholder="Nome">					
					</div>
					<div class="form-group">						
						<input type="email" class="form-control" name="email" placeholder="E-mail">						
					</div>
					<div class="form-group">						
						<input type="tel" class="form-control tel" name="tel" placeholder="Telefone">						
					</div>
					<div class="form-group">					
						<input type="text" class="form-control" name="msg" placeholder="Mensagem">								
					</div>
					<div class="form-group">
						<button type="submit" name="enviar" class="btn btn-success">Enviar</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div class="container">
		<!-- header -->	
		<section class="rows">
			<header class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="header-top">
				<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
					<a href="../index.php" title="Lack infláveis" id="logo">
						<span id="line-1" class="hidden-xs hidden-sm"></span>
						<span class="hide">Lack infláveis</span>
					</a>
				</div>
				<button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<nav class="navbar-collapse collapse" id="nav">
					<ul class="nav navbar-nav">
						<li><a href="/" title="Home">Home</a></li>
						<li><a href="../empresa.php" title="Lack Infláveis">Lack Infláveis</a></li>
						<li><a href="/inflaveis/" title="Produtos">Produtos</a></li>
						<li><a href="../contato.php" title="Contato">Contato</a></li>
						<li class="visible-xs"><a href="http://www.facebook.com/LackInflaveis" title="Facebook Lack Infláveis" target="_blank">Facebook Lack Infláveis</a></li>
						<li class="visible-xs"><a href="http://instagram.com/lackinflaveis" title="Instagram Lack Infláveis" target="_blank"><span class="hide">Instagram Lack Infláveis</span></a></li>
						<li class="visible-xs"><a href="#" title="G+ Lack Infláveis" target="_blank">G+ Lack Infláveis</a></li>
						<li class="visible-xs"><a href="#" title="Flickr Lack Infláveis" target="_blank">Flickr Lack Infláveis</a></li>						
					</ul>
				</nav>
				<ul class="hidden-xs hidden-sm list-inline" id="social">
					<li><a href="http://www.facebook.com/LackInflaveis" title="Facebook Lack Infláveis" target="_blank"><span class="hide">Facebook Lack Infláveis</span></a></li>
					<li><a href="http://instagram.com/lackinflaveis" title="Instagram Lack Infláveis" target="_blank"><span class="hide">Instagram Lack Infláveis</span></a></li>
					<li><a href="http://plus.google.com/u/0/+LackInflaveisBR/about" title="G+ Lack Infláveis" target="_blank"><span class="hide">G+ Lack Infláveis</span></a></li>
					<li><a href="http://www.flickr.com/photos/lackinflaveis/" title="Flickr Lack Infláveis" target="_blank"><span class="hide">Flickr Lack Infláveis</span></a></li>		
				</ul>
			</header>			
		</section>
		<!-- end header -->	