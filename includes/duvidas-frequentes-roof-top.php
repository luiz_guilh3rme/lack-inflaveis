		<section class="rows">
			<h3 class="title-interna-2 col-lg-12 col-md-12 col-sm-12 col-xs-12">Dúvidas<span>frequentes</span></h3>		
			<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
				<p class="text-duvida"><span>Qual a diferença de um inflável de ar para o de gás hélio?</span><br /> 
					 Infláveis de ar são inflados por um motoventilador e são fixados por cordas no solo ou em outros pontos fixos (árvores, postes, grades, etc). Infláveis de gás hélio flutuam no céu fixado por cordas a uma altura de até 30m.</p>

				<p class="text-duvida"><span>O Roof Top pode ser inflado com gás hélio e, consequentemente, flutuar?</span><br /> 
					Não. Ele é um inflável produzido para infla-lo por um motoventilador que já o acompanha e ser instalado no solo, alguma base, em cima de caçambas de automóveis, etc.</p>

				<p class="text-duvida"><span>O inflável precisa ficar com o motor ligado? </span><br /> 
					Sim, as peças de tecido do inflável são costuradas, assim o motor precisa ficar ligado constantemente. A vantagem desse método é que ele impede que os atos de vandalismo ou imprevistos façam com que o inflável venha a murchar. Isso apenas não se aplica para Bola Show e Blimp, pois são produzidos em PVC e não utilizam motoventilador.</p>
				<a href="../duvidas-frequentes.php" title="Ver mais" class="mais-interna">Ver mais</a>
			</div>
			<span class="col-lg-3 col-md-3 hidden-xs hidden-sm" id="balao-interna"></span>
		</section>