<div id="map"></div>
<style>
    /* Always set the map height explicitly to define the size of the div
    * element that contains the map. */
    #map {
        height: 300px;
        width: 100%;
        margin-top: -300px;
        float: left;
    }
    /* Optional: Makes the sample page fill the window. */
    html, body {
        height: 100%;
        margin: 0;
        padding: 0;
    }
    #link-maps-blank{
        bottom: 98px;
    }
</style>
<!-- footer -->	
<div class="container-fluid nav-footer" id="bg-5">
    <div class="container">
        <div class="rows">
            <nav>
                <ul>
                    <li class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                        <a href="index.php" title="Home">Home</a>														
                    </li>
                    <li class="col-lg-3 col-md-3 col-sm-6 col-xs-12">							
                        <a href="empresa.php" title="Lack Inflaveis">Lack Inflaveis</a>
                    </li>
                    <li class="col-lg-3 col-md-3 col-sm-6 col-xs-12">							
                        <a href="/inflaveis/" title="Produtos">Produtos</a>
                        <div class="rows">
                            <ul>
                                <li class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                    <a href="/inflaveis/roof-tops.php" title="Roof Tops">Roof Tops</a>
                                    <a href="/inflaveis/tendas.php" title="Tendas">Tendas</a>
                                    <a href="/inflaveis/tuneis.php" title="Túneis">Túneis</a>
                                    <a href="/inflaveis/replicas.php" title="Réplicas">Réplicas</a>
                                    <a href="/inflaveis/blimp.php" title="Blimps">Blimps</a>
                                    <a href="/inflaveis/portais.php" title="Portais">Portais</a>
                                    <a href="/inflaveis/mascotes.php" title="Mascotes">Mascotes</a>
                                    <a href="/inflaveis/totem.php" title="Totens">Totens</a>
                                </li>
                                <li class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                    <a href="/inflaveis/bola.php" title="Bolas">Bolas</a>
                                    <a href="/inflaveis/tela-de-projecao.php" title="Telas de Projeção">Telas de Projeção</a>
                                    <a href="/inflaveis/stands.php" title="Stands">Stands</a>
                                    <a href="/inflaveis/logotipo.php" title="Logotipos">Logotipos</a>
                                    <a href="/inflaveis/fantasia.php" title="Fantasias">Fantasias</a>
                                    <!--<a href="mini-inflaveis.php" title="Mini Infláveis">Mini Infláveis</a>-->										
                                    <a href="/inflaveis/painel.php" title="Painel">Painel</a>								
                                </li>
                            </ul>
                        </div>
                    </li>						
                    <li class="col-lg-4 col-md-4 col-sm-6 col-xs-12">							
                        <span>Redes  Sociais</span>
                        <div class="rows" id="footer-social">
                            <ul class="clearfix">
                                <li>
                                    <a href="http://www.facebook.com/LackInflaveis" title="Facebook Lack Infláveis" target="_blank">
                                        <span class="hide">Facebook Lack Infláveis</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://instagram.com/lackinflaveis" title="Instagran Lack Infláveis" target="_blank">
                                        <span class="hide">Instagran Lack Infláveis</span>
                                    </a>
                                </li>
                                <li style="position: absolute; visibility: hidden; top:-99999px;">
                                    <!-- <a href="http://plus.google.com/u/0/+LackInflaveisBR/about" title="G+ Lack Infláveis" target="_blank">
                                     <span class="hide">G+ Lack Infláveis</span> 
                                   </a> -->
                                </li>
                                <li>
                                    <a href="http://www.flickr.com/photos/lackinflaveis/" title="Flickr Lack Infláveis" target="_blank" class="no-padding-right">
                                        <span class="hide">Flickr Lack Infláveis</span>
                                    </a>
                                </li>		
                            </ul>
                            <p>

                                <span><a class="linksTel" href="tel:1123041304">(011) 2304.1304</a></span>
                                <span>
                                    <a class="linksTel"  href="tel:1123045901">(011) 2304.5901</a>
                                    <br /> 
                                    <a  class="linksTel" href="tel:11931512176">(011) 93151.2176</a>
                                </span>

                                <span>contato@lackinflaveis.com.br</span><br />
                                <span>
                                    Rua Geraldo Augusto da Silva, 280
                                    Parque Continental I - Guarulhos - SP
                                </span>
                                <span id="agencia-3xceler" class="clearfix">
                                    <span><a href="https://www.3xceler.com.br/criacao-de-sites" target="_blank">Criação de Sites</a>:</span>

                                    <img src="../imagens/logo-3xceler.png" alt="Agencia 3xceler">

                                </span>
                            </p>
                        </div>
                    </li>
                </ul>	
            </nav>
        </div>
    </div>


    <div class=" visible-sm visible-xs rodapeContato">
        <div class="primeiro"><a target="_blank" href="https://api.whatsapp.com/send?phone=5511931512176"><i class="fab fa-whatsapp-square"></i>mande um whatsApp</a></div>
        <div class="segundo"><a class="open-modal" href="#"><i class="fab fa-whatsapp-square"></i>nós te ligamos</a></div>
    </div>

<?php include 'includes/overlay.php'; ?>

    <!-- Modal blimp -->
    <?php include_once 'catalogo.php'; ?>
    <?php include_once 'manual.php'; ?>
    <!-- Modal -->    


</div>

<!-- endfooter -->


<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCzUmvytZ8zXu5xH_DGxuO9Vhqy_zmEVCk&sensor=false"></script>
<script>
    var image = 'https://lackinflaveis.com.br/imagens/icon-maps.png';
    var mapOptions = {
        arrows: true,
        zoom: 16,
        center: new google.maps.LatLng(-23.4388618, -46.5545337),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById('map'), mapOptions);
    var myPos = new google.maps.LatLng(-23.4388618, -46.5545337);
    var myMarker = new google.maps.Marker({position: myPos, map: map, icon: image});
</script>




<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
    function zopin() {
        window.$zopim || (function (d, s) {
            var z = $zopim = function (c) {
                z._.push(c)
            }, $ = z.s =
                    d.createElement(s), e = d.getElementsByTagName(s)[0];
            z.set = function (o) {
                z.set.
                        _.push(o)
            };
            z._ = [];
            z.set._ = [];
            $.async = !0;
            $.setAttribute("charset", "utf-8");
            $.src = "//v2.zopim.com/?4Gbj7NeRyCFiDmXeTNZendcfzh3yK2lX";
            z.t = +new Date;
            $.
                    type = "text/javascript";
            e.parentNode.insertBefore($, e)
        })(document, "script");
    }
</script>
<!--End of Zopim Live Chat Script-->



<script type="text/javascript" src="js/scripts.min.js"></script>


<!--
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" src="js/slick-jquery-migrate-1.2.1.min.js"></script>
-->

<script type="text/javascript" src="js/slick.min.js"></script>





<script>
    // tell the embed parent frame the height of the content
    if (window.parent && window.parent.parent) {
        window.parent.parent.postMessage(["resultsFrame", {
                height: document.body.getBoundingClientRect().height,
                slug: "xghc52wu"
            }], "*")
    }
</script>
<script>

    window.onload = function () {
        $('.slider-for').slick({
            arrows: false, dots: false, infinite: true, speed: 500, fade: true,
            autoplay: true, autoplaySpeed: 3000, slidesToShow: 1, slidesToScroll: 1
        });
        $('.slider-nav > div').click(function () {
            $('.slider-for').slick('slickGoTo', $(this).index());
        });

        $('.left').click(function () {
            console.log("clicou");
            $('.slider-for').slick('slickPrev');
        });

        $('.right').click(function () {
            console.log("clicou");
            $('.slider-for').slick('slickNext');
        })

    }

</script>
<!----------------------------------------------------------->
<!----------------------------------------------------------->
<script>
    console.log("chamando o zopin");
    $('document').ready(function () {
        console.log("chamando o zopin 2");
        var janela;
        janela = $(window).width();
        if (janela > 1000) {
            zopin();
        } else {
        }
    })
</script>
<script>
            (function (a, e, c, f, g, b, d) {
                var h = {ak: "948560181", cl: "1ZJeCO6jomUQtcKnxAM"};
                a[c] = a[c] || function () {
                    (a[c].q = a[c].q || []).push(arguments)
                };
                a[f] || (a[f] = h.ak);
                b = e.createElement(g);
                b.async = 1;
                b.src = "//www.gstatic.com/wcm/loader.js";
                d = e.getElementsByTagName(g)[0];
                d.parentNode.insertBefore(b, d);
                a._googWcmGet = function (b, d, e) {
                    a[c](2, b, h, d, null, new Date, e)
                }
            })(window, document, "_googWcmImpl", "_googWcmAk", "script");
    _googWcmGet('tel-1', '11-2304-1304');
</script>








</body>
</html>