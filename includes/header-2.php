<?php require_once 'compress-html.php'; ?>
<!DOCTYPE HTML>

<html lang="pt-br">



    <?php
    require_once 'Mobile_Detect.php';

    $detect = new Mobile_Detect;
    ?>



    <head>


      <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WQBGHWK');</script>
<!-- End Google Tag Manager -->


        <meta charset="UTF-8">

        <title><?php echo $title; ?></title>

        <meta name="description" content="<?php echo $description; ?>">

        <meta name="apple-mobile-web-app-capable" content="yes">

        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

        <link href='https://fonts.googleapis.com/css?family=Lobster|Antic' rel='stylesheet' type='text/css'>

        <link type="text/css" rel="stylesheet" href="../css/styles.min.css" />

        <link rel="icon" type="image/x-icon" href="/favicon.ico" />

        <link rel="stylesheet" type="text/css" href="../css/slick.css"/>
        <link rel="stylesheet" type="text/css" href="../css/slick-theme.css"/>





    </head>

    <body id="home">

       <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WQBGHWK"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


        <?php echo $bg; ?>








        <div class="box-expresso hidden-xs hidden-sm">
            <span>
                <a onclick='ga("gtag_UA_33166676_1.send", "event", "Contato", "click", "Whats")' href="./../wpp.php"  target="_blank">
                    <img src="https://www.lackinflaveis.com.br/imagens/icon-whats.png" alt="contato pelo whatsapp"> 
                </a>
            </span>    
        </div>









        <div class="container">

            <!-- header -->	

            <section class="rows">

                <header class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="header-top">
                    <div class="fixed-wrapper">

                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">

                            <a href="../index.php" title="Lack infláveis" id="logo">

                                <span id="line-1" class="hidden-xs hidden-sm"></span>

                                <span class="hide">Lack infláveis</span>

                            </a>

                        </div>

                        <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">

                            <span class="icon-bar"></span>

                            <span class="icon-bar"></span>

                            <span class="icon-bar"></span>

                        </button>

                        <?php if ($detect->isMobile()): ?>

                            <nav class="navbar navbar-slide" id="nav">

                            <?php else: ?>

                                <nav class="navbar navbar-desk" id="nav">

                                <?php endif ?>

                                <ul class="nav navbar-nav">

                                    <li><a href="/" title="Home">Home</a></li>

                                    <li><a href="../empresa.php" title="Lack Infláveis">Lack Infláveis</a></li>

                                    <li class="dropdown">

                                        <a title="Produtos">Produtos</a>



                                        <ul>

                                            <li> <a href="/inflaveis/roof-tops.php" title="Roof Tops">Roof Tops</a> </li>		

                                            <li>

                                                <a href="/inflaveis/tendas.php" title="Tendas">Tendas</a>

                                            </li>		

                                            <li>

                                                <a href="/inflaveis/tuneis.php" title="Túneis">Túneis</a>

                                            </li>		

                                            <li>

                                                <a href="/inflaveis/replicas.php" title="Réplicas">Réplicas</a>

                                            </li>		

                                            <li>

                                                <a href="/inflaveis/blimp.php" title="Blimp">Blimp</a>

                                            </li>

                                            <li>

                                                <a href="/inflaveis/portais.php" title="Portais">Portais</a>

                                            </li>	

                                            <li>

                                                <a href="/inflaveis/mascotes.php" title="Mascotes">Mascotes</a>

                                            </li>	

                                            <li>

                                                <a href="/inflaveis/totem.php" title="Totens">Totens</a>

                                            </li>

                                            <li>

                                                <a href="/inflaveis/bola.php" title="">Bola</a>

                                            </li>	

                                            <li>

                                                <a href="/inflaveis/tela-de-projecao.php" title="Telas de Projeção">Telas de Projeção</a>

                                            </li>

                                            <li>

                                                <a href="/inflaveis/stands.php" title="Stands">Stands</a>

                                            </li>

                                            <li>

                                                <a href="/inflaveis/logotipo.php" title="Logotipos">Logotipos</a>

                                            </li>										

                                            <li>

                                                <a href="/inflaveis/fantasia.php" title="Fantasias">Fantasias</a>

                                            </li>				

                                            <li>

                                                <a href="/inflaveis/painel.php" title="Painéis">Painéis</a>

                                            </li>

                                        </ul>

                                    </li>
                                    <li><a href="#form-footer" title="Contato">Contato</a></li>
                                    <li><a href="tel:551123041304" target="_BLANK" title="Ligue para nós">011 2304.1304</a></li>
                                    <li>
                                        <button class="open-modal" data-toggle="modal" data-target="#myModal">
                                            Nós te Ligamos
                                        </button>
                                    </li>				

                                </ul>

                            </nav>

                            <!--   <ul class="hidden-xs hidden-sm list-inline" id="social">
      
                                  <li><a href="http://www.facebook.com/LackInflaveis" title="Facebook Lack Infláveis" target="_blank"><span class="hide">Facebook Lack Infláveis</span></a></li>
      
                                  <li><a href="http://instagram.com/lackinflaveis" title="Instagram Lack Infláveis" target="_blank"><span class="hide">Instagram Lack Infláveis</span></a></li>
      
                                  <li><a href="http://plus.google.com/u/0/+LackInflaveisBR/about" title="G+ Lack Infláveis" target="_blank"><span class="hide">G+ Lack Infláveis</span></a></li>
      
                                  <li><a href="http://www.flickr.com/photos/lackinflaveis/" title="Flickr Lack Infláveis" target="_blank"><span class="hide">Flickr Lack Infláveis</span></a></li>		
      
                              </ul>
                            -->
                    </div>
                </header>			

            </section>

            <!-- end header -->	