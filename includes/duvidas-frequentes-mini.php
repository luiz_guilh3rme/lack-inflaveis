		<section class="rows">
			<h3 class="title-interna-2 col-lg-12 col-md-12 col-sm-12 col-xs-12">Dúvidas<span>frequentes</span></h3>		
			<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
				<p class="text-duvida"><span>Em qual situação é aconselhado utilizar o mini-inflável?</span><br /> 
					 São utilizados como brinde promocional de sua campanha, podendo animar torcidas em estádios, show e realçar o seu evento.</p>

				<p class="text-duvida"><span>Qual modelo de inflável trará melhores resultados para meu projeto de Marketing?</span><br /> 
					Um representante no atendimento indicará qual opção de tamanho e modelo, ira melhor atende-lo dentro das suas necessidades.</p>

				<p class="text-duvida"><span>O que pode assegurar a perfeição na reprodução do formato do inflável?</span><br /> 
					Os infláveis são produzidos com auxilio de software 3D assegurando a precisão na reprodução de imagens tridimensionais. E sempre exigimos que seja mantido o padrão de qualidade já estabelecido em todo o nosso processo de fabricação.</p>
				<a href="../duvidas-frequentes.php" title="Ver mais" class="mais-interna">Ver mais</a>
			</div>
			<span class="col-lg-3 col-md-3 hidden-xs hidden-sm" id="balao-interna"></span>
		</section>