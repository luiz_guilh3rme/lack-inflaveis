<div id="map"></div>
<style>
    /* Always set the map height explicitly to define the size of the div
    * element that contains the map. */
    #map {
        height: 300px;
        width: 100%;
        margin-top: -300px;
        float: left;
    }
    /* Optional: Makes the sample page fill the window. */
    html, body {
        height: 100%;
        margin: 0;
        padding: 0;
    }
    #link-maps-blank{
        bottom: 98px;
    }
</style>
<!-- footer -->
<div class="container-fluid nav-footer" id="bg-5">
    <div class="container">
        <div class="rows">
            <nav>
                <ul>
                    <li class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                        <a href="index.php" title="Home">Home</a>
                    </li>
                    <li class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <a href="empresa.php" title="Lack Inflaveis">Lack Inflaveis</a>
                    </li>
                    <li class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <a href="/inflaveis/" title="Produtos">Produtos</a>
                        <div class="rows">
                            <ul>
                                <li class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                    <a href="roof-tops.php" title="Roof Tops">Roof Tops</a>
                                    <a href="tendas.php" title="Tendas">Tendas</a>
                                    <a href="tuneis.php" title="Túneis">Túneis</a>
                                    <a href="replicas.php" title="Réplicas">Réplicas</a>
                                    <a href="blimp.php" title="Blimps">Blimps</a>
                                    <a href="portais.php" title="Portais">Portais</a>
                                    <a href="mascotes.php" title="Mascotes">Mascotes</a>
                                    <a href="totem.php" title="Totens">Totens</a>
                                </li>
                                <li class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                    <a href="bola.php" title="Bolas">Bolas</a>
                                    <a href="tela-de-projecao.php" title="Telas de Projeção">Telas de Projeção</a>
                                    <a href="stands.php" title="Stands">Stands</a>
                                    <a href="logotipo.php" title="Logotipos">Logotipos</a>
                                    <a href="fantasia.php" title="Fantasias">Fantasias</a>
                                    <!--<a href="mini-inflaveis.php" title="Mini Infláveis">Mini Infláveis</a>-->
                                    <a href="painel.php" title="Painel">Painel</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <span>Redes  Sociais</span>
                        <div class="rows" id="footer-social">
                            <ul class="clearfix">
                                <li>
                                    <a href="http://www.facebook.com/LackInflaveis" title="Facebook Lack Infláveis" target="_blank">
                                        <span class="hide">Facebook Lack Infláveis</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://instagram.com/lackinflaveis" title="Instagran Lack Infláveis" target="_blank">
                                        <span class="hide">Instagran Lack Infláveis</span>
                                    </a>
                                </li>
                                <li style="position: absolute; visibility: hidden; top:-99999px;">
                                    <!-- <a href="http://plus.google.com/u/0/+LackInflaveisBR/about" title="G+ Lack Infláveis" target="_blank">
                                     <span class="hide">G+ Lack Infláveis</span> 
                                 </a> -->
                                </li>
                                <li>
                                    <a href="http://www.flickr.com/photos/lackinflaveis/" title="Flickr Lack Infláveis" target="_blank" class="no-padding-right">
                                        <span class="hide">Flickr Lack Infláveis</span>
                                    </a>
                                </li>
                            </ul>
                            <p>
                                <span>
                                    <a class="linksTel" href="tel:1123041304">(011) 2304.1304</a>
                                    <br />
                                    <a class="linksTel" href="tel:1123045901">(011) 2304.5901</a>
                                    <br /> 
                                    <a class="linksTel" href="tel:11931512176">(011) 93151.2176 Whatsapp</a>
                                </span>
                                <span>contato@lackinflaveis.com.br</span><br />
                                <span>
                                    Rua Geraldo Augusto da Silva, 280
                                    Parque Continental I - Guarulhos - SP
                                </span>
                                <span id="agencia-3xceler" class="clearfix">
                                    <span><a href="https://www.3xceler.com.br/criacao-de-sites" target="_blank">Criação de Sites</a>:</span>

                                    <img src="../imagens/logo-3xceler.png" alt="Agencia 3xceler">

                                </span>
                            </p>
                        </div>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
    
    <div class="contact-footer">
        <a href="#" class="contact-link-cta open-modal" data-toggle="modal" data-target="#myModal">Mande um Whatsapp</a>
        <a href="#" class="contact-link-cta cta-bd open-modal" data-toggle="modal" data-target="#myModal">Nós te ligamos</a>
    </div>


<?php include 'includes/overlay.php'; ?>
    <!-- Modal blimp -->
    <?php include_once 'catalogo.php'; ?>
    <?php include_once 'manual.php'; ?>
    <!-- Modal -->    


</div>
<!-- endfooter -->
<?php     
    include("../includes/components/call-cta-interna.php");
    include("../includes/components/call-cta-forms.php");
?>



<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCzUmvytZ8zXu5xH_DGxuO9Vhqy_zmEVCk"></script>
<script>
    var image = 'https://lackinflaveis.com.br/imagens/icon-maps.png';
    var mapOptions = {
        zoom: 16,
        center: new google.maps.LatLng(-23.4388618, -46.5545337),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById('map'), mapOptions);
    var myPos = new google.maps.LatLng(-23.4388618, -46.5545337);
    var myMarker = new google.maps.Marker({position: myPos, map: map, icon: image});
</script>





<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
    function zopin() {
        window.$zopim || (function (d, s) {
            var z = $zopim = function (c) {
                z._.push(c)
            }, $ = z.s =
                    d.createElement(s), e = d.getElementsByTagName(s)[0];
            z.set = function (o) {
                z.set.
                        _.push(o)
            };
            z._ = [];
            z.set._ = [];
            $.async = !0;
            $.setAttribute("charset", "utf-8");
            $.src = "//v2.zopim.com/?4Gbj7NeRyCFiDmXeTNZendcfzh3yK2lX";
            z.t = +new Date;
            $.
                    type = "text/javascript";
            e.parentNode.insertBefore($, e)
        })(document, "script");
    }
</script>
<!--End of Zopim Live Chat Script-->


<script type="text/javascript" src="../js/scripts.min.js"></script>
<script type="text/javascript" src="../js/jquery.lazyload.js"></script>



<script type="text/javascript" src="../js/slick.min.js"></script>

<script>
    window.onload = function () {


        $('.slider-for').slick({
            arrows: false, dots: false, infinite: true, speed: 500, fade: true,
            autoplay: true, autoplaySpeed: 3000, slidesToShow: 1, slidesToScroll: 1
        });
        $('.slider-nav > div').click(function () {
            $('.slider-for').slick('slickGoTo', $(this).index());
        });

        $('.left').click(function () {
            console.log("clicou");
            $('.slider-for').slick('slickPrev');
        });

        $('.right').click(function () {
            console.log("clicou");
            $('.slider-for').slick('slickNext');
        })

    }
</script>


    
    
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WQBGHWK');</script>
<!-- End Google Tag Manager -->


<?php include('overlay.php'); ?>
</body>
</html>