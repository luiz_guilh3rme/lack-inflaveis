 <div class="overlay">
 <button class="close-modal" aria-label="Fechar Modal">
        &times;
    </button>
    <div class="form-wrapper-all">
        <div class="form-picker">
            <button class="form-pickers" data-instance="00">
                <i class="fa fa-phone"></i>
                ME LIGUE AGORA
            </button>
            <button class="form-pickers form-pickers-margin " data-instance="01">
                <i class="fa fa-clock alt"></i>
                ME LIGUE DEPOIS
            </button>
            <button class="form-pickers active" data-instance="02">
                <i class="fa fa-comments alt"></i>
                DEIXE UMA MENSAGEM
            </button>
        </div>
        <div class="instance" data-instance="01">
            <form class="leave-message schedule-time" id="form-schedule" method="POST">
                <legend class="leave-title">
                    Gostaria de agendar e receber uma
                    chamada em outro horário?
                </legend>
                <div class="row">
                    <div class="form-group col-lg-6">
                        <label class="sr-only" for="scheduleUserName">Seu nome:</label>
                        <input type="text" class="form-control" id="scheduleUserName" name="scheduleUserName" placeholder="Seu nome *" required />
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="sr-only" for="scheduleUserDate">Dia da ligação:</label>
                        <input type="date" class="form-control" id="scheduleUserDate" name="scheduleUserDate" placeholder="Dia da ligação *" required />
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="sr-only" for="scheduleUserHour">Horário da ligação:</label>
                        <input type="time" class="form-control" id="scheduleUserHour" name="scheduleUserHour" placeholder="Horário da ligação *" required />
                    </div>       
                    <div class="form-group col-lg-6">
                        <label class="sr-only" for="scheduleUserPhone">Telefone:</label>
                        <input type="tel" class="form-control" id="scheduleUserPhone" name="scheduleUserPhone" placeholder="Informe seu telefone *" required />
                    </div>       
                </div>
                <button class="btn btn-lack btn-block" type="submit">Me ligue depois</button>
                <p class="callers">Você já é a <span class="number">5</span> pessoa a solicitar uma ligação.</p>
            </form>
        </div>
        <div class="instance active" data-instance="02">
            <form class="leave-message" method="POST" id="form-deixe-mensagem">
                <legend class="leave-title">
                    Deixe sua mensagem! Entraremos em
                    contato o mais rápido possível.
                </legend>
                <div class="form-row">
                    <div class="form-group col-12">
                        <label class="sr-only" for="leaveMessageUserName">Seu nome:</label>
                        <input type="text" class="form-control" id="leaveMessageUserName" name="leaveMessageUserName" placeholder="Seu nome *" required />
                    </div>
                    <div class="form-group col-12">
                        <label class="sr-only" for="leaveMessageUserPhone">Telefone:</label>
                        <input type="tel" class="form-control" id="leaveMessageUserPhone" name="leaveMessageUserPhone" placeholder="Telefone *" required />
                    </div>       
                    <div class="form-group col-12">
                        <label class="sr-only" for="leaveMessageUserEmail">Seu nome:</label>
                        <input type="email" class="form-control" id="leaveMessageUserEmail" name="leaveMessageUserEmail" placeholder="E-mail *" required />
                    </div>
                    <div class="form-group col-12">
                        <label class="sr-only" for="leaveMessageUserMessage">Mensagem:</label>
                        <textarea class="form-control" id="leaveMessageUserMessage" rows="6" name="leaveMessageUserMessage" placeholder="Mensagem *" required ></textarea>
                    </div>
                </div>
                <button class="btn btn-lack btn-block" type="submit">Me ligue depois</button>
                <p class="callers">Você já é a <span class="number">5</span> pessoa a solicitar uma ligação.</p>
            </form>
        </div>
        <div class="instance" data-instance="00">
            <form class="leave-message" id="form-we-call" method="POST">
                <legend class="leave-title">
                    <span class="variant">
                        NÓS TE LIGAMOS!
                    </span>
                    Informe seu telefone que entraremos em
                    contato o mais rápido possível.
                </legend>
                <div class="form-row">
                    <div class="form-group col-12">
                        <label class="sr-only" for="weCallUserName">Seu nome:</label>
                        <input type="text" class="form-control" id="weCallUserName" name="weCallUserName" placeholder="Seu nome *" required />
                    </div>
                    <div class="form-group col-12">
                        <label class="sr-only" for="weCallUserPhone">Telefone:</label>
                        <input type="tel" class="form-control" id="weCallUserPhone" name="weCallUserPhone" placeholder="Telefone *" required />
                    </div>       
                </div>
                <button class="btn btn-lack btn-block" type="submit">Me ligue agora</button>
                <p class="callers">Você já é a <span class="number">5</span> pessoa a solicitar uma ligação.</p>
            </form>
        </div>
    </div>
 </div>