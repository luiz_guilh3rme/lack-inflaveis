       <form action="" method="post" id="formIndex">
            <div  class="col-lg-12">
                <h2 class="titulos">Solicite orçamento</h2>
                <small class="subtitulo">Entraremos em contato com você o mais breve possível</small>
                <div class="newLine">
                    <input type="hidden" name="redirect" value="/sucesso-expresso.php">
                    <div class="col-md-4">
                        <input  type="text" name="nome"  placeholder="Nome">
                    </div>
                    <div class="col-md-2">
                        <input type="tel" class="tel valid" name="cel" placeholder="Cel" aria-required="true" aria-invalid="false">
                    </div>
                    <div class="col-md-2">
                        <input required="required" type="tel" class="tel valid" name="tel" placeholder="Tel" aria-required="true" aria-invalid="false">
                    </div>
                    <div class="col-md-4 textarea">
                        <textarea name="mensagem" placeholder="mensagem"></textarea>
                    </div>
                </div>
                <div class="newLine">
                    <div class="col-md-4">
                        <input type="text"   name="email" placeholder="e-mail">
                    </div>
                    <div class="col-md-4">
                        <select  required="required"  name="produto" class="form-control" id="produto">
                            <option value="" select>Produto</option>
                            <option value="Roof Tops">Roof Tops</option>
                            <option value="Tendas">Tendas</option>
                            <option value="Túneis">Túneis</option>
                            <option value="Réplicas">Réplicas</option>
                            <option value="Blimp">Blimp</option>
                            <option value="Portais">Portais</option>
                            <option value="Mascotes">Mascotes</option>
                            <option value="Totens">Totens</option>
                            <option value="Bola">Bola</option>
                            <option value="Telas de Projeção">Telas de Projeção</option>
                            <option value="Stands">Stands</option>
                            <option value="Logotipos">Logotipos</option>
                            <option value="Fantasias">Fantasias</option>
                            <option value="Painéis">Painéis</option>
                        </select>
                    </div>
                    <div class="col-md-4">
                        <input type="submit" class="btn" value="enviar" >
                    </div>
                </div><!-- newline -->
            </div><!-- col-lg-12 -->
        </form>