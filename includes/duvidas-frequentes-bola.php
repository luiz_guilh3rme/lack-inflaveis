		<section class="rows">
			<h3 class="title-interna-2 col-lg-12 col-md-12 col-sm-12 col-xs-12">Dúvidas<span>frequentes</span></h3>		
			<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
				<p class="text-duvida"><span>A bola pode ser inflada com gás hélio?</span><br /> 
					 Não. Apesar de ser produzida com o mesmo material do blimp, a bola é produzida a partir de 1m de diâmetro. Para que possa flutuar ela deveria ter uma dimensão maior, se tornando o blimp.</p>

				<p class="text-duvida"><span>Qual a vida útil de um inflável?</span><br /> 
					Existem diversos infláveis produzidos pela Lack que estão sendo utilizados há anos. Mas o principal requisito é o cuidado que terá com o inflável, assim você ira prolongar a vida útil por anos. </p>

				<p class="text-duvida"><span>A bola acompanha motoventilador?</span><br /> 
					Não. A bola devera ser inflada por sua válvula ou entrada de ar utilizando um equipando como: soprador, compressor, etc.</p>
				<a href="../duvidas-frequentes.php" title="Ver mais" class="mais-interna">Ver mais</a>
			</div>
			<span class="col-lg-3 col-md-3 hidden-xs hidden-sm" id="balao-interna"></span>
		</section>