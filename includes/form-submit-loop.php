<?php 

if (!$_POST) 
exit('$_POST');

if ( $_POST ) {   
    require 'PHPMailer/PHPMailerAutoload.php';

    try {
        $mail = new PHPMailer(true);
        $mail->isHTML(true);
        $mail->CharSet = 'UTF-8';
        $mail->setLanguage('pt', 'PHPMailer/language/phpmailer.lang-pt.php');
        $mail->setFrom('postmaster@lackinflaveis.com.br', 'Postmaster'); 
        $mail->addAddress('contato@lackinflaveis.com.br'); 
        $mail->Subject = 'Formulário de Contato | Lack Infláveis';

        $body = '<table style="font-size: 16px; border-collapse: collapse; border: 1px solid; width: 400px; font-family: Helvetica, sans-serif;">';

        foreach ( $_POST as $field => $f ) {
            if ( $field !== 'redirect' ) {

                $body .= 
                '<tr>
                <td style="padding: 10px; border: 1px solid;"><b>'.$field.'</b></td>
                <td style="padding: 10px; border: 1px solid;">'.$f.'</td>
                </tr>'; 
                
            }
        }

        $body .= '</table>'; 

        $mail->msgHTML($body); 
        $mail->send();
        // print_r($body);

        
    } catch(Exception $e)  {
        echo '<pre>'; 
        print_r($e);
        echo '</pre>';
    }
    
}