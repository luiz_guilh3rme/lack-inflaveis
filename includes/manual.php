<div class="modal fade" id="Modal" tabindex="-1" role="dialog" aria-labelledby="Manual" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
        <h4 class="modal-title">Manual</h4>
      </div>
      <div class="modal-body">
        <img src="../arquivos/manual.jpg" alt="Manual" class="img-responsive" />
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>