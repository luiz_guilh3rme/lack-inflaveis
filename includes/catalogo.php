<div class="modal fade" id="catalogo" tabindex="-1" role="dialog" aria-labelledby="catalogo" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
        <h4 class="modal-title title-3">Nossos <span>catálogos</span></h4>
      </div>
      <div class="modal-body" id="cat">
        <p>Apresentamos a você nosso catálogo virtual, para que conheça todas as opções que colocamos à sua disposição</p>
        <form name="form-catalogo" id="form-catalogo" action="" method="post" role="form" class="clearfix">
          <input type="hidden" name="url" value="<?= "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>">
          <input type="text" name="nome" placeholder="Nome" class="form-control">
          <input type="email" name="email" placeholder="E-mail" class="form-control">
          <button type="submit" class="btn btn-success" onClick="javascript:_gaq.push(['_trackEvent','Catalogo','Clique']);">Baixar o catálogo</button>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>