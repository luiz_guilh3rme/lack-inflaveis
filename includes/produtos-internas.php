	<!-- produtos -->
		<section class="rows clearfix">
			<h2 class="title-interna"><span id="line-3" class="hidden-xs hidden-sm"></span>Outros <span>produtos</span></h2>
		</section>
		<section class="rows">
			<ul class="lista-produto">				
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="roof-tops.php" title="Roof Tops">
						<img src="../imagens/produtos/roof-tops.png" alt="Roof Tops" class="img-responsive">
						<span class="grama"></span>						
						<h2>Roof Tops</h2>
					</a>
				</li>		
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="tendas.php" title="Tendas">
						<img src="../imagens/produtos/tendas.png" alt="Tendas" class="img-responsive">
						<span class="asfalto"></span>						
						<h2>Tendas</h2>
					</a>
				</li>		
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="tuneis.php" title="Túneis">
						<img src="../imagens/produtos/tuneis.png" alt="Túneis" class="img-responsive">
						<span class="grama"></span>						
						<h2>Túneis</h2>
					</a>
				</li>		
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="replicas.php" title="Réplicas">
						<img src="../imagens/produtos/replicas.png" alt="Réplicas" class="img-responsive">
						<span class="asfalto"></span>						
						<h2>Réplicas</h2>
					</a>
				</li>		
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="blimp.php" title="Blimp">
						<img src="../imagens/produtos/blimp.png" alt="Blimp" class="img-responsive">
						<span class="grama"></span>						
						<h2>Blimp</h2>
					</a>
				</li>
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="portais.php" title="Portais">
						<img src="../imagens/produtos/portal.png" alt="Portais" class="img-responsive">
						<span class="asfalto"></span>						
						<h2>Portais</h2>
					</a>
				</li>	
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="mascotes.php" title="Mascotes">
						<img src="../imagens/produtos/mascote.png" alt="Mascotes" class="img-responsive">
						<span class="grama"></span>						
						<h2>Mascotes</h2>
					</a>
				</li>	
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="totem.php" title="Totens">
						<img src="../imagens/produtos/totems.png" alt="Totens" class="img-responsive">
						<span class="asfalto"></span>						
						<h2>Totens</h2>
					</a>
				</li>
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="bola.php" title="Bola">
						<img src="../imagens/produtos/bolas.png" alt="Bola" class="img-responsive">
						<span class="grama"></span>						
						<h2>Bola</h2>
					</a>
				</li>	
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="tela-de-projecao.php" title="Telas de Projeção">
						<img src="../imagens/produtos/telas-projecaos.png" alt="Telas de Projeção" class="img-responsive">
						<span class="asfalto"></span>						
						<h2>Telas de Projeção</h2>
					</a>
				</li>
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="stands.php" title="Stands">
						<img src="../imagens/produtos/stand.png" alt="Stands" class="img-responsive">
						<span class="grama"></span>						
						<h2>Stands</h2>
					</a>
				</li>
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="logotipo.php" title="Logotipos">
						<img src="../imagens/produtos/logotipos.png" alt="Logotipos" class="img-responsive">
						<span class="asfalto"></span>						
						<h2>Logotipos</h2>
					</a>
				</li>										
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="fantasia.php" title="Fantasias">
						<img src="../imagens/produtos/fantasias.png" alt="Fantasias" class="img-responsive">
						<span class="grama"></span>						
						<h2>Fantasias</h2>
					</a>
				</li>				
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="painel.php" title="Painéis">
						<img src="../imagens/produtos/paineis.png" alt="Painéis" class="img-responsive">
						<span class="grama"></span>						
						<h2>Painéis</h2>
					</a>
				</li>
				<!--<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="mini-inflaveis.php" title="Mini-infláveis">
						<img src="../imagens/produtos/mini.png" alt="Mini-infláveis" class="img-responsive">
						<span class="asfalto"></span>						
						<h2>Mini-infláveis</h2>
					</a>
				</li>-->
			</ul>
		</section>
	<!-- end produtos -->	