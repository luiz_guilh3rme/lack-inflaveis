		<section class="rows">
			<h3 class="title-interna-2 col-lg-12 col-md-12 col-sm-12 col-xs-12">Dúvidas<span>frequentes</span></h3>		
			<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
				<p class="text-duvida"><span>Qual a diferença de um inflável de ar para o de gás hélio?</span><br /> 
					 Infláveis de ar são inflados por um motoventilador e são fixados por cordas no solo ou em outros pontos fixos (árvores, postes, grades, etc). Infláveis de gás hélio flutuam no céu fixado por cordas a uma altura de até 30m.</p>

				<p class="text-duvida"><span>Qual a dimensão do blimp para flutuar com gás hélio?</span><br /> 
					Devera medir no mínimo 2,5m de diâmetro. Que é a dimensão necessária para que carregue a cubagem mínima de gás hélio para fazer com o que blimp flutue.</p>

				<p class="text-duvida"><span>O Blimp além de ser inflado com gás hélio para flutuar, pode inflar com ar normal?</span><br /> 
					Sim. Utilizando um equipando como: soprador, compressor, etc. Você pode inflar e utilizar ele no solo, em alguma base, torre, como cenografia, etc</p>
				<a href="../duvidas-frequentes.php" title="Ver mais" class="mais-interna">Ver mais</a>
			</div>
			<span class="col-lg-3 col-md-3 hidden-xs hidden-sm" id="balao-interna"></span>
		</section>