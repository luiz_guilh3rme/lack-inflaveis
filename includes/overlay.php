<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<!--<div id="overlay">-->
	<div class="modal-form">
		<img src="imagens/balloon-prop.png" aria-hidden="true" class="balloon-prop">
		<form class="modal-fields">
			<legend class="form-title">
				Preencha os campos abaixo e entraremos
				em contato o mais breve possível
			</legend>
			<label for="" class="field-wrapper full">
				<p class="field-description">Nome:</p>
				<input class="field" type="text" name="nome">
			</label><label for="" class="field-wrapper full">
				<p class="field-description">Telefone:</p>
				<input class="field" type="text" name="tel">
			</label>
			<button class="submit-modal">
				ENVIAR
			</button>
		</form>
		<button class="close-modal-form" aria-label="Fechar Modal de Nós te Ligamos">
			&times;
		</button>
	</div>
</div>
