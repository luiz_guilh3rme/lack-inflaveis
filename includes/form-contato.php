<form action="" method="post" name="form-footer" id="form-footer" enctype="multipart/form-data" class="col-lg-5 col-md-5 col-sm-12 col-xs-12">

	<input type="hidden" name="url" value="<?= "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>">

	<a href="http://goo.gl/kLKqu9" title="Veja mapa completo" target="_blank" id="link-maps-blank" class="hidden-xs hidden-sm">

		<span class="glyphicon glyphicon-map-marker"></span>

		Veja mapa completo			

	</a>

	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

		<div class="rows">

			<h4 class="col-lg-12 col-md-12 col-sm-12 col-xs-12">Entre em contato</h4>

		</div>

	</div>

	<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">

		<div class="rows">

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<input type="hidden" name="redirect" value="/obrigado.php">
				<input type="text" class="form-control" name="nome" placeholder="Nome" required />

			</div>

		</div>

	</div>

	<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">

		<div class="rows">

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

				<input type="text" class="form-control" name="empresa" placeholder="Empresa" required />

			</div>

		</div>

	</div>

	<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">

		<div class="rows">

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

				<input type="email" class="form-control" name="email" placeholder="E-mail" required />

			</div>

		</div>

	</div>

	<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">

		<div class="rows">

			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">

				<input type="tel" class="form-control tel" name="tel" placeholder="Tel" required />

			</div>

			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">

				<input type="tel" class="form-control tel" name="cel" placeholder="Cel" required />

			</div>

		</div>

	</div>

	<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">

		<div class="rows">

			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no-padding-right">

				<select name="estado" class="form-control" id="estado" required>

					<option value="" select>Estado</option>

					<option value="AC">Acre</option>

		            <option value="AL">Alagoas</option>

		            <option value="AM">Amazonas</option>

		            <option value="AP">Amapá</option>

		            <option value="BA">Bahia</option>

		            <option value="CE">Ceará</option>

		            <option value="DF">Distrito Federal</option>

		            <option value="ES">Espírito Santo</option>

		            <option value="GO">Goiás</option>

		            <option value="MA">Maranhão</option>

		            <option value="MG">Minas Gerais</option>

		            <option value="MS">Mato Grosso do Sul</option>

		            <option value="MT">Mato Grosso</option>

		            <option value="PA">Pará</option>

		            <option value="PB">Paraíba</option>

		            <option value="PE">Pernambuco</option>

		            <option value="PI">Piauí</option>

		            <option value="PR">Paraná</option>

		            <option value="RJ">Rio de Janeiro</option>

		            <option value="RN">Rio Grande do Norte</option>

		            <option value="RO">Rondônia</option>

		            <option value="RR">Roraima</option>

		            <option value="RS">Rio Grande do Sul</option>

		            <option value="SC">Santa Catarina</option>

		            <option value="SE">Sergipe</option>

		            <option value="SP">São Paulo</option>

		            <option value="TO">Tocantins</option>

				</select>

			</div>

			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">

				<input type="text" class="form-control" name="cidade" placeholder="Cidade" required />										

			</div>

		</div>

	</div>

	<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">

		<div class="rows">

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

				<select name="produto" class="form-control" id="produto" required>

					<option value="" select>Produto</option>

					<option value="Roof Tops">Roof Tops</option>

					<option value="Tendas">Tendas</option>

					<option value="Túneis">Túneis</option>

					<option value="Réplicas">Réplicas</option>

					<option value="Blimp">Blimp</option>

					<option value="Portais">Portais</option>

					<option value="Mascotes">Mascotes</option>

					<option value="Totens">Totens</option>

					<option value="Bola">Bola</option>

					<option value="Telas de Projeção">Telas de Projeção</option>

					<option value="Stands">Stands</option>

					<option value="Logotipos">Logotipos</option>

					<option value="Fantasias">Fantasias</option>

					<option value="Painéis">Painéis</option>

				</select>

			</div>

		</div>

	</div>

	<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">

		<div class="rows">

			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">

				<input type="text" class="form-control" name="medidas" placeholder="Medidas">

			</div>

			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no-padding-left">

				<input type="text" class="form-control" name="qtdade" placeholder="Qtdade" required />

			</div>

		</div>

	</div>

	<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">

		<div class="rows">

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

				<textarea name="msg" class="form-control" id="" cols="10" rows="4" placeholder="mensagem" required></textarea>

			</div>

		</div>

	</div>

	<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">

		<div class="rows">

			<!-- <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">

				<span class="upload form-control">anexos <span class="glyphicon glyphicon-chevron-up pull-right"></span></span>

				<input type="file" class="form-control upload-file" name="arquivo[]" placeholder="Anexos">

			</div> -->

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

				<button type="submit" name="enviar" class="btn btn-success" onClick="javascript:_gaq.push(['_trackEvent','Contato','Clique']);">Enviar</button>

			</div>

		</div>

	</div>

</form>