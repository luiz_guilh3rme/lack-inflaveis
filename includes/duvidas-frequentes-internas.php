		<section class="rows">
			<h3 class="title-interna-2 col-lg-12 col-md-12 col-sm-12 col-xs-12">Dúvidas<span>frequentes</span></h3>		
			<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
				<p class="text-duvida"><span>Qual a diferença de um inflável de ar para o de gás hélio?</span><br /> 
					 Infláveis de ar são inflados por um motoventilador e são fixados por cordas no solo ou em outros pontos fixos (árvores, postes, grades, etc). Infláveis de gás hélio flutuam no céu fixado por cordas a uma altura de até 30m.</p>

				<p class="text-duvida"><span>Quais infláveis utilizam o gás hélio?</span><br /> 
					Somente os infláveis que são produzidos no material em PVC, são eles: Blimp e zeppelin.</p>

				<p class="text-duvida"><span>Um inflável de ar frio, por exemplo o roof top, pode ser cheio com gás hélio e, consequentemente, flutuar?</span><br /> 
					Não. Infláveis de ar e de gás hélio são produtos diferentes, onde os métodos de produção e matéria prima são totalmente diferentes um do outro.</p>
				<a href="../duvidas-frequentes.php" title="Ver mais" class="mais-interna">Ver mais</a>
			</div>
			<span class="col-lg-3 col-md-3 hidden-xs hidden-sm" id="balao-interna"></span>
		</section>