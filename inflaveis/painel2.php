	<?php 
		//SEO
	$title = 'Balões Roof Tops | Lack Infláveis';
	$description = 'Balões Roof Tops  Empresa Especializada em Infláveis. Aproveite acesse e agora e solicite já o seu orçamento online do seu Inflável!';
	$canonicalTag = '<link rel="canonical” href="http://www.lackinflaveis.com.br/inflaveis/roof-tops.php"/>';
	$bg = "<div id=\"bg-interna-1\"></div>
	<div id=\"bg-interna-2\" class=\"hidden-xs\"></div>";
	?>
	<?php require_once '../includes/header-2.php'; ?>




	<section class="rows">
		<!------------------------------------>

		<div class="hidden-xs hidden-sm col-md-12" id="slider-for">
			<div class="slider-for">
				<div>
					<div class="col-md-6 textoSlide">
						<p>
							<strong>painel</strong>
							Uma boa publicidade surte mais efeito quando está no mesmo lugar que seu público-alvo. Utilizando um painel inflável, 
                                                        você o leva para onde precisar! Sua variedade de tamanhos - a partir de 2m - permite peças menores 
                                                        (ideais para instalação em pontos-de-venda) até peças maiores (para instalações externas). 
                                                        O material utilizado tem grande durabilidade e a arte é impressa digitalmente no mesmo.
							<span>
								<a href="#" title="Ver o manual" data-toggle="modal" data-target="#Modal"><img src="../imagens/manual_Icon.jpg" class="icones">Ver o manual</a>
								<a href="#" title="Baixar o catálogo" data-toggle="modal" data-target="#catalogo"><img src="../imagens/catalogo_Icon.jpg" class="icones">Baixar o catálogo</a>
							</span>
						</p>
					</div>
					<div class="col-md-6 SliderContainer"><img src="../imagens/produtos/painel/01.jpg" class="imagensSlide" alt="infláveis" title="infláveis"  width="98%" height="auto"></div>
				</div>
				<!--<div>
					<div class="col-md-6 textoSlide">
						<p>
							<strong>painel</strong>
							Uma boa publicidade surte mais efeito quando está no mesmo lugar que seu público-alvo. Utilizando um painel inflável, 
                                                        você o leva para onde precisar! Sua variedade de tamanhos - a partir de 2m - permite peças menores 
                                                        (ideais para instalação em pontos-de-venda) até peças maiores (para instalações externas). 
                                                        O material utilizado tem grande durabilidade e a arte é impressa digitalmente no mesmo.
							<span>
								<a href="#" title="Ver o manual" data-toggle="modal" data-target="#Modal"><img src="../imagens/manual_Icon.jpg" class="icones">Ver o manual</a>
								<a href="#" title="Baixar o catálogo" data-toggle="modal" data-target="#catalogo"><img src="../imagens/catalogo_Icon.jpg" class="icones">Baixar o catálogo</a>
							</span>
						</p>
					</div>
					<div class="col-md-6 SliderContainer">
						<img src="../imagens/produtos/painel/02.jpg" alt="infláveis"  class="imagensSlide"  title="infláveis" width="98%" height="auto">
					</div>
				</div>
				<div>
					<div class="col-md-6 textoSlide">
						<p>
							<strong>painel</strong>
							Uma boa publicidade surte mais efeito quando está no mesmo lugar que seu público-alvo. Utilizando um painel inflável, 
                                                        você o leva para onde precisar! Sua variedade de tamanhos - a partir de 2m - permite peças menores 
                                                        (ideais para instalação em pontos-de-venda) até peças maiores (para instalações externas). 
                                                        O material utilizado tem grande durabilidade e a arte é impressa digitalmente no mesmo.
							<span>
								<a href="#" title="Ver o manual" data-toggle="modal" data-target="#Modal"><img src="../imagens/manual_Icon.jpg" class="icones">Ver o manual</a>
								<a href="#" title="Baixar o catálogo" data-toggle="modal" data-target="#catalogo"><img src="../imagens/catalogo_Icon.jpg" class="icones">Baixar o catálogo</a>
							</span>
						</p>
					</div>
					<div class="col-md-6 SliderContainer"><img src="../imagens/produtos/painel/03.jpg"  class="imagensSlide"  alt="infláveis" title="infláveis"  width="98%" height="auto"></div>
				</div>-->
			</div>
                    
                    
                    
                    

			<img src="../imagens/arrow2.png" class="left">
			<img src="../imagens/arrow1.png" class="right">

			<div class='slider-nav'>
				<div><img src="../imagens/produtos/painel/01.jpg" width="100px"></div>
				<div><img src="../imagens/produtos/painel/02.jpg" width="100px"></div>
				<div><img src="../imagens/produtos/painel/03.jpg" width="100px"></div>
			</div>        
		</div>
		<!------------------------------------->


		<section class="form-footer row hidden-xs hidden-sm clearfix form">
			<form action="" method="post" id="formIndex">
				<div  class="col-lg-12">
					<h2 class="titulos">Solicite orçamento</h2>
					<small class="subtitulo">Entraremos em contato com você o mais breve possível</small>

					<div class="newLine">
						<div class="col-md-4">
							<input  type="text" name="nome"  placeholder="Nome">
						</div>
						<div class="col-md-2">
							<!--<input type="text" name="telefone" placeholder="telefone">-->
							<input type="tel" class="tel valid" name="cel" placeholder="Cel" aria-required="true" aria-invalid="false">
						</div>
						<div class="col-md-2">
							<!---<input type="text"   id="cel" name="celular" placeholder="celular">-->
							<input required="required" type="tel" class="tel valid" name="tel" placeholder="Tel" aria-required="true" aria-invalid="false">
						</div>
						<div class="col-md-4 textarea">
							<textarea name="mensagem" placeholder="mensagem"></textarea>
						</div>
					</div>
					<div class="newLine">
						<div class="col-md-4">
							<input type="text"   name="email" placeholder="e-mail">
						</div>
						<div class="col-md-4">
							<select  required="required"  name="produto" class="form-control" id="produto">
								<option value="" select>Produto</option>
								<option value="Roof Tops">Roof Tops</option>
								<option value="Tendas">Tendas</option>
								<option value="Túneis">Túneis</option>
								<option value="Réplicas">Réplicas</option>
								<option value="Blimp">Blimp</option>
								<option value="Portais">Portais</option>
								<option value="Mascotes">Mascotes</option>
								<option value="Totens">Totens</option>
								<option value="Bola">Bola</option>
								<option value="Telas de Projeção">Telas de Projeção</option>
								<option value="Stands">Stands</option>
								<option value="Logotipos">Logotipos</option>
								<option value="Fantasias">Fantasias</option>
								<option value="Painéis">Painéis</option>
							</select>
						</div>
						<div class="col-md-2">

							<span class="upload form-control"> 
								<label class="anexos" for="ArquivoUp">anexos</label>
								<input type="file" class="hidden" id="ArquivoUp" name="arquivo[]" placeholder="Anexos">
							</span>

						</div>
						<div class="col-md-2">
							<input type="submit" class="btn" value="enviar" >
						</div>
					</div>
				</div>
			</form>
		</section>



		<!-- produtos -->
                
               
                <section class="col-lg-12 col-md-12 col-sm-12 col-xs-12 galeria-produto"><div class="rows"><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/painel/04.jpg" title="Painel Inflável Dosatak - Arte em impressão digital frente e verso. Medida Final 1,3m de largura x 2,0m de altura"><span><img src="../imagens/produtos/painel/04.jpg" alt="Painel Inflável Dosatak - Arte em impressão digital frente e verso. Medida Final 1,3m de largura x 2,0m de altura"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/painel/05.jpg" title="Painel Inflável Controle de Qualidade SL - Arte em impressão digital frente e verso. Medida Final 3,0m de diâmetro"><span><img src="../imagens/produtos/painel/05.jpg" alt="Painel Inflável Controle de Qualidade SL - Arte em impressão digital frente e verso. Medida Final 3,0m de diâmetro"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/painel/06.jpg" title="Painel Inflável Combate - Arte em impressão digital frente e verso. Medida Final 2,5m de largura x 2,5m de altura"><span><img src="../imagens/produtos/painel/06.jpg" alt="Painel Inflável Combate - Arte em impressão digital frente e verso. Medida Final 2,5m de largura x 2,5m de altura"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/painel/07.jpg" title="Painel Inflável Bem Cuidar - Arte em impressão digital frente e verso. Medida Final 1,7m de largura x 2,0m de altura"><span><img src="../imagens/produtos/painel/07.jpg" alt="Painel Inflável Bem Cuidar - Arte em impressão digital frente e verso. Medida Final 1,7m de largura x 2,0m de altura"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/painel/08.jpg" title="Painel Inflável BelTools - Arte em impressão digital frente e verso. Medida Final 2,15m de largura x 2,50m de altura"><span><img src="../imagens/produtos/painel/08.jpg" alt="Painel Inflável BelTools - Arte em impressão digital frente e verso. Medida Final 2,15m de largura x 2,50m de altura"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/painel/09.jpg" title="Painel Cubo Inflável Eskala - Arte em impressão Digital Total. Medida Final 2,0m de largura x 2,0m de altura x 2,0m de comprimento"><span><img src="../imagens/produtos/painel/09.jpg" alt="Painel Cubo Inflável Eskala - Arte em impressão Digital Total. Medida Final 2,0m de largura x 2,0m de altura x 2,0m de comprimento"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/painel/10.jpg" title="Chute a Gol Inflável RIC TV Xanxerê (lateral) - Arte aplicada na frente e gol. Medida Final 3,00m de largura x 2,45m de altura"><span><img src="../imagens/produtos/painel/10.jpg" alt="Chute a Gol Inflável RIC TV Xanxerê (lateral) - Arte aplicada na frente e gol. Medida Final 3,00m de largura x 2,45m de altura"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/painel/11.jpg" title="Chute a Gol Inflável RIC TV Xanxerê - Arte aplicada na frente e gol. Medida Final 3,00m de largura x 2,45m de altura"><span><img src="../imagens/produtos/painel/11.jpg" alt="Chute a Gol Inflável RIC TV Xanxerê - Arte aplicada na frente e gol. Medida Final 3,00m de largura x 2,45m de altura"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/painel/12.jpg" title="Chute a Gol Inflável LG - Arte aplicada na frente e gol. Medida Final 3,0m de largura x 3,0m de comprimento"><span><img src="../imagens/produtos/painel/12.jpg" alt="Chute a Gol Inflável LG - Arte aplicada na frente e gol. Medida Final 3,0m de largura x 3,0m de comprimento"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/painel/painel-001.jpg" title="Painel Inflável Supermercado Tonhão - Arte em impressão digital frente e verso. Medida Final 4,2m de largura x 6,0m de altura"><span><img src="../imagens/produtos/painel/painel-001.jpg" alt="Painel Inflável Supermercado Tonhão - Arte em impressão digital frente e verso. Medida Final 4,2m de largura x 6,0m de altura"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/painel/painel-003.jpg" title="Painel Inflável Ao Gosto - Arte em impressão digital frente e verso. Medida Final 2,0m de largura x 2,0m de altura"><span><img src="../imagens/produtos/painel/painel-003.jpg" alt="Painel Inflável Ao Gosto - Arte em impressão digital frente e verso. Medida Final 2,0m de largura x 2,0m de altura"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/painel/painel-004.jpg" title="Painel Inflável Agrobella. Medida Final 5,0m de largura x 3,0m de altura"><span><img src="../imagens/produtos/painel/painel-004.jpg" alt="Painel Inflável Agrobella. Medida Final 5,0m de largura x 3,0m de altura"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/painel/painel-008.jpg" title="Painel Inflável Supermercado Tonhão - Arte em impressão digital frente e verso. Medida Final 2,8m de largura x 4,0m de altura"><span><img src="../imagens/produtos/painel/painel-008.jpg" alt="Painel Inflável Supermercado Tonhão - Arte em impressão digital frente e verso. Medida Final 2,8m de largura x 4,0m de altura"></span></a></div></section>                
                
                
                <!-- end produtos -->


		<?php require_once './../includes/produtos-internas.php'; ?>
		<?php require_once './../includes/duvidas-frequentes-roof-top.php'; ?>

	</div>





</div>

<div class="container-fluid" id="mapa-interna">
	<div id="bg-interna" class="hidden-xs hidden-sm"></div>
	<div id="map_canvas"></div>
	<div class="container z-index">
		<?php require_once './../includes/form-contato.php'; ?>
	</div>
</div>

<?php require_once '../includes/footer-2.php'; ?>
