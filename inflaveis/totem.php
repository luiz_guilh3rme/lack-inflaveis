	<?php 
		//SEO
	$title = 'Balões Roof Tops | Lack Infláveis';
	$description = 'Balões Roof Tops  Empresa Especializada em Infláveis. Aproveite acesse e agora e solicite já o seu orçamento online do seu Inflável!';
	$canonicalTag = '<link rel="canonical” href="http://www.lackinflaveis.com.br/inflaveis/roof-tops.php"/>';
	$bg = "<div id=\"bg-interna-1\"></div>
	<div id=\"bg-interna-2\" class=\"hidden-xs\"></div>";
	?>
	<?php require_once '../includes/header-2.php'; ?>




<span class="bradcrumb">
    produtos <span>totem</span>
</span>

	<section class="rows totem">
		<!------------------------------------>

		<div class="col-md-12" id="slider-for">
			<div class="slider-for">
				<div>
					<div class="col-sm-12 col-md-6 textoSlide">
						<p>
							<strong>totem</strong>
							Em eventos com grande público, como feiras e exposições, chamar a atenção para sua marca pode ser difícil em meio a tanto movimento. 
                                                        Nessa situação, o totem inflável é uma excelente forma de obter a visibilidade necessária, pois é uma peça que se sobressai na multidão. 
                                                        Pode ter de 2m até mais de 5m de altura e ainda receber iluminação interna para eventos noturnos.
							<span>
								<a href="#" title="Ver o manual" data-toggle="modal" data-target="#Modal"><img src="../imagens/manual_Icon.jpg" class="icones">Ver o manual</a>
								<a href="#" title="Baixar o catálogo" data-toggle="modal" data-target="#catalogo"><img src="../imagens/catalogo_Icon.jpg" class="icones">Baixar o catálogo</a>
							</span>
						</p>
					</div>
					<div class="col-sm-12 col-md-6 SliderContainer"><img src="../imagens/produtos/totem/01.jpg" class="imagensSlide" alt="infláveis" title="infláveis"  width="98%" height="auto"></div>
				</div>
				<div>
					<div class="col-sm-12 col-md-6 textoSlide">
						<p>
							<strong>totem</strong>
							Em eventos com grande público, como feiras e exposições, chamar a atenção para sua marca pode ser difícil em meio a tanto movimento. 
                                                        Nessa situação, o totem inflável é uma excelente forma de obter a visibilidade necessária, pois é uma peça que se sobressai na multidão. 
                                                        Pode ter de 2m até mais de 5m de altura e ainda receber iluminação interna para eventos noturnos.
							<span>
								<a href="#" title="Ver o manual" data-toggle="modal" data-target="#Modal"><img src="../imagens/manual_Icon.jpg" class="icones">Ver o manual</a>
								<a href="#" title="Baixar o catálogo" data-toggle="modal" data-target="#catalogo"><img src="../imagens/catalogo_Icon.jpg" class="icones">Baixar o catálogo</a>
							</span>
						</p>
					</div>
					<div class="col-sm-12 col-md-6 SliderContainer">
						<img src="../imagens/produtos/totem/02.jpg" alt="infláveis"  class="imagensSlide"  title="infláveis" width="98%" height="auto">
					</div>
				</div>
				<div>
					<div class="col-sm-12 col-md-6 textoSlide">
						<p>
							<strong>totem</strong>
							Em eventos com grande público, como feiras e exposições, chamar a atenção para sua marca pode ser difícil em meio a tanto movimento. 
                                                        Nessa situação, o totem inflável é uma excelente forma de obter a visibilidade necessária, pois é uma peça que se sobressai na multidão. 
                                                        Pode ter de 2m até mais de 5m de altura e ainda receber iluminação interna para eventos noturnos.
							<span>
								<a href="#" title="Ver o manual" data-toggle="modal" data-target="#Modal"><img src="../imagens/manual_Icon.jpg" class="icones">Ver o manual</a>
								<a href="#" title="Baixar o catálogo" data-toggle="modal" data-target="#catalogo"><img src="../imagens/catalogo_Icon.jpg" class="icones">Baixar o catálogo</a>
							</span>
						</p>
					</div>
					<div class="col-sm-12 col-md-6 SliderContainer"><img src="../imagens/produtos/totem/03.jpg"  class="imagensSlide"  alt="infláveis" title="infláveis"  width="98%" height="auto"></div>
				</div>
			</div>

			<img src="../imagens/arrow2.png" class="hidden-xs left">
			<img src="../imagens/arrow1.png" class="hidden-xs right">

			<div class='slider-nav hidden-xs'>
				<div><img src="../imagens/produtos/totem/01.jpg" width="100px"></div>
				<div><img src="../imagens/produtos/totem/02.jpg" width="100px"></div>
				<div><img src="../imagens/produtos/totem/03.jpg" width="100px"></div>
			</div>        
		</div>
		<!------------------------------------->


		<section class="form-footer row hidden-xs hidden-sm clearfix form">
			<?php
				include_once '../includes/components/form_footer.php';
			?>
		</section>



		<!-- produtos -->
                
               <section class="col-lg-12 col-md-12 col-sm-12 col-xs-12 galeria-produto"><div class="rows"><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/totem/04.jpg" title="Totem Inflável Rio do Sul. Dimensão 2,0m de altura x 0,7m de diâmetro."><span><img src="../imagens/produtos/totem/04.jpg" alt="Totem Inflável Rio do Sul. Dimensão 2,0m de altura x 0,7m de diâmetro."></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/totem/05.jpg" title="Totem Inflável Eskala. Dimensão 3,0m de altura x 1,0m de diâmetro."><span><img src="../imagens/produtos/totem/05.jpg" alt="Totem Inflável Eskala. Dimensão 3,0m de altura x 1,0m de diâmetro."></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/totem/06.jpg" title="Totem Inflável Copel Telecom. Dimensão 3,0m de altura x 1,0m de diâmetro."><span><img src="../imagens/produtos/totem/06.jpg" alt="Totem Inflável Copel Telecom. Dimensão 3,0m de altura x 1,0m de diâmetro."></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/totem/07.jpg" title="Totem Inflável Cooperatividade. Dimensão 3,0m de altura x 1,1m de diâmetro."><span><img src="../imagens/produtos/totem/07.jpg" alt="Totem Inflável Cooperatividade. Dimensão 3,0m de altura x 1,1m de diâmetro."></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/totem/08.jpg" title="Totem Inflável AIDS. Dimensão 4,0m de altura x 1,5m de diâmetro."><span><img src="../imagens/produtos/totem/08.jpg" alt="Totem Inflável AIDS. Dimensão 4,0m de altura x 1,5m de diâmetro."></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/totem/09.jpg" title="Totem Ball Inflável Estancia Bahia. Dimensão 3,0m de altura x 1,0m de diâmetro."><span><img src="../imagens/produtos/totem/09.jpg" alt="Totem Ball Inflável Estancia Bahia. Dimensão 3,0m de altura x 1,0m de diâmetro."></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/totem/10.jpg" title="Totem Inflável Blitz. Dimensão 2,5m de altura x 0,8m de diâmetro."><span><img src="../imagens/produtos/totem/10.jpg" alt="Totem Inflável Blitz. Dimensão 2,5m de altura x 0,8m de diâmetro."></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/totem/11.jpg" title="Totem Inflável TIM. Dimensão 2,50m de altura x 0,65m de diâmetro."><span><img src="../imagens/produtos/totem/11.jpg" alt="Totem Inflável TIM. Dimensão 2,50m de altura x 0,65m de diâmetro."></span></a></div></section>
                
                
                <!-- end produtos -->


		<?php require_once './../includes/produtos-internas.php'; ?>
		<?php require_once './../includes/duvidas-frequentes-roof-top.php'; ?>

	</div>





</div>

<div class="container-fluid" id="mapa-interna">
	<div id="bg-interna" class="hidden-xs hidden-sm"></div>
	<div id="map_canvas"></div>
	<div class="container z-index">
		<?php require_once './../includes/form-contato.php'; ?>
	</div>
</div>

<?php require_once '../includes/footer-map-interna.php'; ?>	
<?php require_once '../includes/manual.php'; ?>	
<?php require_once '../includes/catalogo.php'; ?>	
<?php require_once '../includes/footer-2.php'; ?>
