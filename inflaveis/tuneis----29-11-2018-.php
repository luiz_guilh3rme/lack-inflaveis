	<?php 
		//SEO
	$title = 'Balões Roof Tops | Lack Infláveis';
	$description = 'Balões Roof Tops  Empresa Especializada em Infláveis. Aproveite acesse e agora e solicite já o seu orçamento online do seu Inflável!';
	$canonicalTag = '<link rel="canonical” href="http://www.lackinflaveis.com.br/inflaveis/roof-tops.php"/>';
	$bg = "<div id=\"bg-interna-1\"></div>
	<div id=\"bg-interna-2\" class=\"hidden-xs\"></div>";
	?>
	<?php require_once '../includes/header-2.php'; ?>





<span class="bradcrumb">
    produtos <span>tuneis</span>
</span>



	<section class="rows">
		<!------------------------------------>

		<div class="hidden-xs hidden-sm col-md-12" id="slider-for">
			<div class="slider-for">
				<div>
					<div class="col-md-6 textoSlide">
						<p>
							<strong>túneis inflaveis</strong>
							Os túneis infláveis são um diferencial em qualquer evento. Semelhantes a um portal, eles criam a sensação de passagem para um novo ambiente, um novo mundo, interferindo positivamente na percepção de seus clientes e fortalecendo os laços entre eles e sua marca. Disponíveis em vários modelos e tamanho, inclusive em curva.
							<span>
								<a href="#" title="Ver o manual" data-toggle="modal" data-target="#Modal"><img src="../imagens/manual_Icon.jpg" class="icones">Ver o manual</a>
								<a href="#" title="Baixar o catálogo" data-toggle="modal" data-target="#catalogo"><img src="../imagens/catalogo_Icon.jpg" class="icones">Baixar o catálogo</a>
							</span>
						</p>
					</div>
					<div class="col-md-6 SliderContainer"><img src="../imagens/produtos/tunel/01.jpg" class="imagensSlide" alt="infláveis" title="infláveis"  width="98%" height="auto"></div>
				</div>
				<div>
					<div class="col-md-6 textoSlide">
						<p>
							<strong>túneis inflaveis</strong>
							Os túneis infláveis são um diferencial em qualquer evento. Semelhantes a um portal, eles criam a sensação de passagem para um novo ambiente, um novo mundo, interferindo positivamente na percepção de seus clientes e fortalecendo os laços entre eles e sua marca. Disponíveis em vários modelos e tamanho, inclusive em curva.
							<span>
								<a href="#" title="Ver o manual" data-toggle="modal" data-target="#Modal"><img src="../imagens/manual_Icon.jpg" class="icones">Ver o manual</a>
								<a href="#" title="Baixar o catálogo" data-toggle="modal" data-target="#catalogo"><img src="../imagens/catalogo_Icon.jpg" class="icones">Baixar o catálogo</a>
							</span>
						</p>
					</div>
					<div class="col-md-6 SliderContainer">
						<img src="../imagens/produtos/tunel/02.jpg" alt="infláveis"  class="imagensSlide"  title="infláveis" width="98%" height="auto">
					</div>
				</div>
				<div>
					<div class="col-md-6 textoSlide">
						<p>
							<strong>túneis inflaveis</strong>
							Os túneis infláveis são um diferencial em qualquer evento. Semelhantes a um portal, eles criam a sensação de passagem para um novo ambiente, um novo mundo, interferindo positivamente na percepção de seus clientes e fortalecendo os laços entre eles e sua marca. Disponíveis em vários modelos e tamanho, inclusive em curva.
							<span>
								<a href="#" title="Ver o manual" data-toggle="modal" data-target="#Modal"><img src="../imagens/manual_Icon.jpg" class="icones">Ver o manual</a>
								<a href="#" title="Baixar o catálogo" data-toggle="modal" data-target="#catalogo"><img src="../imagens/catalogo_Icon.jpg" class="icones">Baixar o catálogo</a>
							</span>
						</p>
					</div>
					<div class="col-md-6 SliderContainer"><img src="../imagens/produtos/tunel/03.jpg"  class="imagensSlide"  alt="infláveis" title="infláveis"  width="98%" height="auto"></div>
				</div>
			</div>

			<img src="../imagens/arrow2.png" class="left">
			<img src="../imagens/arrow1.png" class="right">

			<div class='slider-nav'>
				<div><img src="../imagens/produtos/tunel/01.jpg" width="100px"></div>
				<div><img src="../imagens/produtos/tunel/02.jpg" width="100px"></div>
				<div><img src="../imagens/produtos/tunel/03.jpg" width="100px"></div>
			</div>        
		</div>
		<!------------------------------------->


		<section class="form-footer row hidden-xs hidden-sm clearfix form">
			<form action="" method="post" id="formIndex">
				<div  class="col-lg-12">
					<h2 class="titulos">Solicite orçamento</h2>
					<small class="subtitulo">Entraremos em contato com você o mais breve possível</small>

					<div class="newLine">
						<div class="col-md-4">
							<input  type="text" name="nome"  placeholder="Nome">
						</div>
						<div class="col-md-2">
							<!--<input type="text" name="telefone" placeholder="telefone">-->
							<input type="tel" class="tel valid" name="cel" placeholder="Cel" aria-required="true" aria-invalid="false">
						</div>
						<div class="col-md-2">
							<!---<input type="text"   id="cel" name="celular" placeholder="celular">-->
							<input required="required" type="tel" class="tel valid" name="tel" placeholder="Tel" aria-required="true" aria-invalid="false">
						</div>
						<div class="col-md-4 textarea">
							<textarea name="mensagem" placeholder="mensagem"></textarea>
						</div>
					</div>
					<div class="newLine">
						<div class="col-md-4">
							<input type="text"   name="email" placeholder="e-mail">
						</div>
						<div class="col-md-4">
							<select  required="required"  name="produto" class="form-control" id="produto">
								<option value="" select>Produto</option>
								<option value="Roof Tops">Roof Tops</option>
								<option value="Tendas">Tendas</option>
								<option value="Túneis">Túneis</option>
								<option value="Réplicas">Réplicas</option>
								<option value="Blimp">Blimp</option>
								<option value="Portais">Portais</option>
								<option value="Mascotes">Mascotes</option>
								<option value="Totens">Totens</option>
								<option value="Bola">Bola</option>
								<option value="Telas de Projeção">Telas de Projeção</option>
								<option value="Stands">Stands</option>
								<option value="Logotipos">Logotipos</option>
								<option value="Fantasias">Fantasias</option>
								<option value="Painéis">Painéis</option>
							</select>
						</div>
						<div class="col-md-2">

							<span class="upload form-control"> 
								<label class="anexos" for="ArquivoUp">anexos</label>
								<input type="file" class="hidden" id="ArquivoUp" name="arquivo[]" placeholder="Anexos">
							</span>

						</div>
						<div class="col-md-2">
							<input type="submit" class="btn" value="enviar" >
						</div>
					</div>
				</div>
			</form>
		</section>



		<!-- produtos -->
		<section class="col-lg-12 col-md-12 col-sm-12 col-xs-12 galeria-produto"><div class="rows"><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/tunel/04.jpg" title="Túnel Inflável BorgWaner – Com arte aplicada em impressão digital. Dimensão 3,0m de comprimento."><span><img src="../imagens/produtos/tunel/04.jpg" alt="Túnel Inflável BorgWaner – Com arte aplicada em impressão digital. Dimensão 3,0m de comprimento."></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/tunel/tunel-002.jpg" title="Túnel Inflável Castrol – Arte aplicada em impressão digital. Medida Final 6,0m de comprimento x 3,5m de largura x 2,7 de altura"><span><img src="../imagens/produtos/tunel/tunel-002.jpg" alt="Túnel Inflável Castrol – Arte aplicada em impressão digital. Medida Final 6,0m de comprimento x 3,5m de largura x 2,7 de altura"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/tunel/tunel-006.jpg" title="Túnel Inflável Branco. Medida Final 6,0m de comprimento x 3,5m de largura x 2,7 de altura"><span><img src="../imagens/produtos/tunel/tunel-006.jpg" alt="Túnel Inflável Branco. Medida Final 6,0m de comprimento x 3,5m de largura x 2,7 de altura"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/tunel/tunel-010.jpg" title="Túnel Inflável Projeto especial Sabesp. Dimensão 15,0m de comprimento"><span><img src="../imagens/produtos/tunel/tunel-010.jpg" alt="Túnel Inflável Projeto especial Sabesp. Dimensão 15,0m de comprimento"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/tunel/tunel-011.jpg" title="Impressão de arte para produção no Túnel Saraiva"><span><img src="../imagens/produtos/tunel/tunel-011.jpg" alt="Impressão de arte para produção no Túnel Saraiva"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/tunel/tunel-012.jpg" title="Túnel Saraiva Inflável – Arte aplicada em impressão digital. Medida Final 3,0m de comprimento x 3,5m de largura x 2,7 de altura"><span><img src="../imagens/produtos/tunel/tunel-012.jpg" alt="Túnel Saraiva Inflável – Arte aplicada em impressão digital. Medida Final 3,0m de comprimento x 3,5m de largura x 2,7 de altura"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/tunel/tunel-013.jpg" title="Túnel Inflável Saraiva – Arte aplicada em impressão digital. Medida Final 3,0m de comprimento x 3,5m de largura x 2,7 de altura"><span><img src="../imagens/produtos/tunel/tunel-013.jpg" alt="Túnel Inflável Saraiva – Arte aplicada em impressão digital. Medida Final 3,0m de comprimento x 3,5m de largura x 2,7 de altura"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/tunel/tunel-015.jpg" title="Túnel Inflável Ducati - Medida Final 5,0m comprimento x 11,0m largura x 4,0m altura"><span><img src="../imagens/produtos/tunel/tunel-015.jpg" alt="Túnel Inflável Ducati - Medida Final 5,0m comprimento x 11,0m largura x 4,0m altura"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/tunel/tunel-016.jpg" title="Túnel Inflável Ducati - Medida Final 11,0m largura x 4,0m altura x 5,0m comprimento"><span><img src="../imagens/produtos/tunel/tunel-016.jpg" alt="Túnel Inflável Ducati - Medida Final 11,0m largura x 4,0m altura x 5,0m comprimento"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/tunel/tunel-017.jpg" title="Túnel Ducati Inflável - Medida Final 11,0m largura x 4,0m altura x 5,0m comprimento"><span><img src="../imagens/produtos/tunel/tunel-017.jpg" alt="Túnel Ducati Inflável - Medida Final 11,0m largura x 4,0m altura x 5,0m comprimento"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/tunel/tunel-018.jpg" title="Túnel Inflável Companhia de Negócios - Medida Final 6,0m comprimento"><span><img src="../imagens/produtos/tunel/tunel-018.jpg" alt="Túnel Inflável Companhia de Negócios - Medida Final 6,0m comprimento"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/tunel/tunel-020.jpg" title="Túnel Companhia de Negócios Inflável - Medida Final 6,0m comprimento"><span><img src="../imagens/produtos/tunel/tunel-020.jpg" alt="Túnel Companhia de Negócios Inflável - Medida Final 6,0m comprimento"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/tunel/tunel-022.jpg" title="Túnel Inflável SIPAT - Medida Final 6,0m comprimento"><span><img src="../imagens/produtos/tunel/tunel-022.jpg" alt="Túnel Inflável SIPAT - Medida Final 6,0m comprimento"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/tunel/tunel-023.jpg" title="Túnel Scuderia Inflável – Arte aplicada em impressão digital. Medida Final 3,0m de comprimento x 3,5m de largura x 2,7 de altura"><span><img src="../imagens/produtos/tunel/tunel-023.jpg" alt="Túnel Scuderia Inflável – Arte aplicada em impressão digital. Medida Final 3,0m de comprimento x 3,5m de largura x 2,7 de altura"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/tunel/tunel-024.jpg" title="Túnel Inflável Scuderia – Arte aplicada em impressão digital. Medida Final 3,0m de comprimento x 3,5m de largura x 2,7 de altura"><span><img src="../imagens/produtos/tunel/tunel-024.jpg" alt="Túnel Inflável Scuderia – Arte aplicada em impressão digital. Medida Final 3,0m de comprimento x 3,5m de largura x 2,7 de altura"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/tunel/tunel-027.jpg" title="Túnel Inflável Azul – Arte aplicada em impressão digital. Medida Final 3,0m de comprimento x 3,5m de largura x 2,7 de altura"><span><img src="../imagens/produtos/tunel/tunel-027.jpg" alt="Túnel Inflável Azul – Arte aplicada em impressão digital. Medida Final 3,0m de comprimento x 3,5m de largura x 2,7 de altura"></span></a></div></section>
		<!-- end produtos -->


		<?php require_once './../includes/produtos-internas.php'; ?>
		<?php require_once './../includes/duvidas-frequentes-roof-top.php'; ?>

	</div>





</div>

<div class="container-fluid" id="mapa-interna">
	<div id="bg-interna" class="hidden-xs hidden-sm"></div>
	<div id="map_canvas"></div>
	<div class="container z-index">
		<?php require_once '../includes/form-contato.php'; ?>
	</div>
</div>


<?php require_once '../includes/footer-map-interna.php'; ?>	
<?php require_once '../includes/manual.php'; ?>	
<?php require_once '../includes/catalogo.php'; ?>	

<?php require_once '../includes/footer-2.php'; ?>
