	<?php 
		//SEO
		$title = 'Infláveis | Lack Infláveis Peça Já Seu Orçamento ';
		$description = 'Esta procurando por Infláveis Promocionais? Entre aqui na Lack Infláveis e confira os nossos diversos produtos!';
		$canonicalTag = '<link rel="canonical” href="http://www.lackinflaveis.com.br/inflaveis/"/>';
		$bg = "<div id=\"bg-interna-1\"></div>
			   <div id=\"bg-interna-2\" class=\"hidden-xs\"></div>";
	?>
	<?php require_once '../includes/header.php'; ?>
		
		<section class="rows clearfix">			
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">				
				<div class="rows" id="info-produto">					
					<h1 class="text-center">Infláveis</h1>
					<ol class="breadcrumb">
						<li><a href="/">Home</a></li>					
						<li class="active"><strong>Infláveis</strong></li>
					</ol>
					<p>
						O impacto visual é fundamental para a formação de qualquer tipo de relação. Ao apresentar sua marca em lugares novos, 
						é preciso contar com um suporte que crie uma relação agradável de identidade visual com aqueles que estiverem conhecendo 
						sua empresa. E os infláveis se encaixam como uma grande ferramenta de interatividade para auxilia-lo nesta oportunidade.
					</p>
					<p>
						Um evento personalizado e caracterizado fará toda a diferença para o seu negócio, e para isso, conte com os infláveis como 
						peça chave para trabalhar sua publicidade. A Lack Infláveis trabalha com uma extensa variedade de infláveis para atender ao 
						seu negócio em qualquer tipo de ação publicitária. Conheça os modelos abaixo e solicite um orçamento:
					</p>
					</br></br>										
				</div>
			</div>		
		</section>

		<!-- produtos -->
		<section class="rows clearfix">
			<h2 class="title-interna"><span id="line-3" class="hidden-xs hidden-sm"></span>Nossos <span>produtos</span></h2>
		</section>
		<section class="rows">
			<ul class="lista-produto">				
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="/inflaveis/roof-tops.php" title="Roof Tops">
						<img src="../imagens/produtos/roof-tops.png" alt="Roof Tops" class="img-responsive">
						<span class="grama"></span>						
						<h2>Roof Tops</h2>
					</a>
				</li>		
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="/inflaveis/tendas.php" title="Tendas">
						<img src="../imagens/produtos/tendas.png" alt="Tendas" class="img-responsive">
						<span class="asfalto"></span>						
						<h2>Tendas</h2>
					</a>
				</li>		
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="/inflaveis/tuneis.php" title="Túneis">
						<img src="../imagens/produtos/tuneis.png" alt="Túneis" class="img-responsive">
						<span class="grama"></span>						
						<h2>Túneis</h2>
					</a>
				</li>		
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="/inflaveis/replicas.php" title="Réplicas">
						<img src="../imagens/produtos/replicas.png" alt="Réplicas" class="img-responsive">
						<span class="asfalto"></span>						
						<h2>Réplicas</h2>
					</a>
				</li>		
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="/inflaveis/blimp.php" title="Blimp">
						<img src="../imagens/produtos/blimp.png" alt="Blimp" class="img-responsive">
						<span class="grama"></span>						
						<h2>Blimp</h2>
					</a>
				</li>
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="/inflaveis/portais.php" title="Portais">
						<img src="../imagens/produtos/portal.png" alt="Portais" class="img-responsive">
						<span class="asfalto"></span>						
						<h2>Portais</h2>
					</a>
				</li>	
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="/inflaveis/mascotes.php" title="Mascotes">
						<img src="../imagens/produtos/mascote.png" alt="Mascotes" class="img-responsive">
						<span class="grama"></span>						
						<h2>Mascotes</h2>
					</a>
				</li>	
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="/inflaveis/totem.php" title="Totens">
						<img src="../imagens/produtos/totems.png" alt="Totens" class="img-responsive">
						<span class="asfalto"></span>						
						<h2>Totens</h2>
					</a>
				</li>
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="/inflaveis/bola.php" title="">
						<img src="../imagens/produtos/bolas.png" alt="Bolas" class="img-responsive">
						<span class="grama"></span>						
						<h2>Bola</h2>
					</a>
				</li>	
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="/inflaveis/tela-de-projecao.php" title="Telas de Projeção">
						<img src="../imagens/produtos/telas-projecaos.png" alt="Telas de Projeção" class="img-responsive">
						<span class="asfalto"></span>						
						<h2>Telas de Projeção</h2>
					</a>
				</li>
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="/inflaveis/stands.php" title="Stands">
						<img src="../imagens/produtos/stand.png" alt="Stands" class="img-responsive">
						<span class="grama"></span>						
						<h2>Stands</h2>
					</a>
				</li>
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="/inflaveis/logotipo.php" title="Logotipos">
						<img src="../imagens/produtos/logotipos.png" alt="Logotipos" class="img-responsive">
						<span class="asfalto"></span>						
						<h2>Logotipos</h2>
					</a>
				</li>										
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="/inflaveis/fantasia.php" title="Fantasias">
						<img src="../imagens/produtos/fantasias.png" alt="Fantasias" class="img-responsive">
						<span class="grama"></span>						
						<h2>Fantasias</h2>
					</a>
				</li>				
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="/inflaveis/painel.php" title="Painéis">
						<img src="../imagens/produtos/paineis.png" alt="Painéis" class="img-responsive">
						<span class="grama"></span>						
						<h2>Painéis</h2>
					</a>
				</li>
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a href="/inflaveis/mini-inflaveis.php" title="Mini-infláveis">
						<img src="../imagens/produtos/mini.png" alt="Mini-infláveis" class="img-responsive">
						<span class="asfalto"></span>						
						<h2>Mini-infláveis</h2>
					</a>
				</li>
			</ul>
		</section>
		<!-- end produtos -->	
	
	</div>
	
	<div class="container-fluid" id="mapa-interna">
		<div id="bg-interna" class="hidden-xs hidden-sm"></div>
		<div id="map_canvas"></div>
		<div class="container z-index">
			<?php require_once '../includes/form-contato.php'; ?>
		</div>
	</div>

	<?php require_once '../includes/footer.php'; ?>
	