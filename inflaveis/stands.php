	<?php 
		//SEO
	$title = 'Balões Roof Tops | Lack Infláveis';
	$description = 'Balões Roof Tops  Empresa Especializada em Infláveis. Aproveite acesse e agora e solicite já o seu orçamento online do seu Inflável!';
	$canonicalTag = '<link rel="canonical” href="http://www.lackinflaveis.com.br/inflaveis/roof-tops.php"/>';
	$bg = "<div id=\"bg-interna-1\"></div>
	<div id=\"bg-interna-2\" class=\"hidden-xs\"></div>";
	?>
	<?php require_once '../includes/header-2.php'; ?>


<span class="bradcrumb ">
    produtos <span>stands</span>
</span>


	<section class="rows stands">
		<!------------------------------------>

		<div class="col-md-12" id="slider-for">
			<div class="slider-for">
				<div>
					<div class="col-sm-12 col-md-6 textoSlide">
						<p>
							<strong>stands</strong>
							Se sua empresa participa de muitos eventos, o stand inflável é a opção ideal devido à sua praticidade e durabilidade. 
                                                        Produzido no formato e nas dimensões que seu projeto requer, esse stand pode receber impressões digitais, além da aplicação 
                                                        removível ou permanente de imagens em sua estrutura. Permite ainda a instalação de balcões e cortinas, de acordo com sua necessidade.
							<span>
								<a href="#" title="Ver o manual" data-toggle="modal" data-target="#Modal"><img src="../imagens/manual_Icon.jpg" class="icones">Ver o manual</a>
								<a href="#" title="Baixar o catálogo" data-toggle="modal" data-target="#catalogo"><img src="../imagens/catalogo_Icon.jpg" class="icones">Baixar o catálogo</a>
							</span>
						</p>
					</div>
					<div class="col-sm-12 col-md-6 SliderContainer"><img src="../imagens/produtos/stand/01.jpg" class="imagensSlide" alt="infláveis" title="infláveis"  width="98%" height="auto"></div>
				</div>
				<div>
					<div class="col-sm-12 col-md-6 textoSlide">
						<p>
							<strong>stands</strong>
							Se sua empresa participa de muitos eventos, o stand inflável é a opção ideal devido à sua praticidade e durabilidade. 
                                                        Produzido no formato e nas dimensões que seu projeto requer, esse stand pode receber impressões digitais, além da aplicação 
                                                        removível ou permanente de imagens em sua estrutura. Permite ainda a instalação de balcões e cortinas, de acordo com sua necessidade.
							<span>
								<a href="#" title="Ver o manual" data-toggle="modal" data-target="#Modal"><img src="../imagens/manual_Icon.jpg" class="icones">Ver o manual</a>
								<a href="#" title="Baixar o catálogo" data-toggle="modal" data-target="#catalogo"><img src="../imagens/catalogo_Icon.jpg" class="icones">Baixar o catálogo</a>
							</span>
						</p>
					</div>
					<div class="col-sm-12 col-md-6 SliderContainer">
						<img src="../imagens/produtos/stand/02.jpg" alt="infláveis"  class="imagensSlide"  title="infláveis" width="98%" height="auto">
					</div>
				</div>
				<div>
					<div class="col-sm-12 col-md-6 textoSlide">
						<p>
							<strong>stands</strong>
							Se sua empresa participa de muitos eventos, o stand inflável é a opção ideal devido à sua praticidade e durabilidade. 
                                                        Produzido no formato e nas dimensões que seu projeto requer, esse stand pode receber impressões digitais, além da aplicação 
                                                        removível ou permanente de imagens em sua estrutura. Permite ainda a instalação de balcões e cortinas, de acordo com sua necessidade.
							<span>
								<a href="#" title="Ver o manual" data-toggle="modal" data-target="#Modal"><img src="../imagens/manual_Icon.jpg" class="icones">Ver o manual</a>
								<a href="#" title="Baixar o catálogo" data-toggle="modal" data-target="#catalogo"><img src="../imagens/catalogo_Icon.jpg" class="icones">Baixar o catálogo</a>
							</span>
						</p>
					</div>
					<div class="col-sm-12 col-md-6 SliderContainer"><img src="../imagens/produtos/stand/03.jpg"  class="imagensSlide"  alt="infláveis" title="infláveis"  width="98%" height="auto"></div>
				</div>
			</div>

			<img src="../imagens/arrow2.png" class="hidden-xs left">
			<img src="../imagens/arrow1.png" class="hidden-xs right">

			<div class='slider-nav hidden-xs'>
				<div><img src="../imagens/produtos/stand/01.jpg" width="100px"></div>
				<div><img src="../imagens/produtos/stand/02.jpg" width="100px"></div>
				<div><img src="../imagens/produtos/stand/03.jpg" width="100px"></div>
			</div>        
		</div>
		<!------------------------------------->


		<section class="form-footer row hidden-xs hidden-sm clearfix form">
			<?php
				include_once '../includes/components/form_footer.php';
			?>
		</section>



		<!-- produtos -->
                
                <section class="col-lg-12 col-md-12 col-sm-12 col-xs-12 galeria-produto"><div class="rows"><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/stand/04.jpg" title="Stand Inflável Prefeitura de Alvorada - Medida Final 3,0m de largura x 3,0m de comprimento"><span><img src="../imagens/produtos/stand/04.jpg" alt="Stand Inflável Prefeitura de Alvorada - Medida Final 3,0m de largura x 3,0m de comprimento"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/stand/05.jpg" title="Stand Inflável Pitú - Medida Final 4,0m de largura x 4,0m de comprimento"><span><img src="../imagens/produtos/stand/05.jpg" alt="Stand Inflável Pitú - Medida Final 4,0m de largura x 4,0m de comprimento"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/stand/06.jpg" title="Stand Inflável Prefeitura de Bagé - Medida Final 3,0m de largura x 3,0m de comprimento"><span><img src="../imagens/produtos/stand/06.jpg" alt="Stand Inflável Prefeitura de Bagé - Medida Final 3,0m de largura x 3,0m de comprimento"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/stand/07.jpg" title="Stand Inflável Loja Clássico - Medida Final 3,5m de largura x 3,5m de comprimento"><span><img src="../imagens/produtos/stand/07.jpg" alt="Stand Inflável Loja Clássico - Medida Final 3,5m de largura x 3,5m de comprimento"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/stand/08.jpg" title="Stand Inflável Loja Bruno e Marrone. Arte aplicada em Impressão Digital Total - Medida Final 3,5m de largura x 3,5m de comprimento"><span><img src="../imagens/produtos/stand/08.jpg" alt="Stand Inflável Loja Bruno e Marrone. Arte aplicada em Impressão Digital Total - Medida Final 3,5m de largura x 3,5m de comprimento"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/stand/09.jpg" title="Stand Inflável Clássico - Medida Final 3,5m de largura x 3,5m de comprimento"><span><img src="../imagens/produtos/stand/09.jpg" alt="Stand Inflável Clássico - Medida Final 3,5m de largura x 3,5m de comprimento"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/stand/10.jpg" title="Stand Inflável CDL Joaçaba - Medida Final 3,0m de largura x 4,0m de comprimento"><span><img src="../imagens/produtos/stand/10.jpg" alt="Stand Inflável CDL Joaçaba - Medida Final 3,0m de largura x 4,0m de comprimento"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/stand/11.jpg" title="Stand Inflável Bruno e Marrone. Arte aplicada em Impressão Digital Total - Medida Final 3,5m de largura x 3,5m de comprimento"><span><img src="../imagens/produtos/stand/11.jpg" alt="Stand Inflável Bruno e Marrone. Arte aplicada em Impressão Digital Total - Medida Final 3,5m de largura x 3,5m de comprimento"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/stand/12.jpg" title="Stand Inflável Anhanguera Educacional - Medida Final 3,0m de largura x 3,0m de comprimento"><span><img src="../imagens/produtos/stand/12.jpg" alt="Stand Inflável Anhanguera Educacional - Medida Final 3,0m de largura x 3,0m de comprimento"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/stand/stands-001.jpg" title="Stand Inflável Prefeitura de Alvorada - Medida Final 3,0m de largura x 3,0m de comprimento"><span><img src="../imagens/produtos/stand/stands-001.jpg" alt="Stand Inflável Prefeitura de Alvorada - Medida Final 3,0m de largura x 3,0m de comprimento"></span></a></div></section>
                
                <!-- end produtos -->


		<?php require_once './../includes/produtos-internas.php'; ?>
		<?php require_once './../includes/duvidas-frequentes-roof-top.php'; ?>

	</div>





</div>

<div class="container-fluid" id="mapa-interna">
	<div id="bg-interna" class="hidden-xs hidden-sm"></div>
	<div id="map_canvas"></div>
	<div class="container z-index">
		<?php require_once './../includes/form-contato.php'; ?>
	</div>
</div>


<?php require_once '../includes/footer-map-interna.php'; ?>	
<?php require_once '../includes/manual.php'; ?>	
<?php require_once '../includes/catalogo.php'; ?>	
<?php require_once '../includes/footer-2.php'; ?>
