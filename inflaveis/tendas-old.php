	<?php 
		//SEO
		$title = 'Tendas Infláveis | Lack Infláveis';
		$description = 'Tendas Infláveis Empresa Especializada em Infláveis. Aproveite acesse e agora e solicite já o seu orçamento online do seu Inflável!';
		$canonicalTag = '<link rel="canonical” href="http://www.lackinflaveis.com.br/inflaveis/tendas.php"/>';
		$bg = "<div id=\"bg-interna-1\"></div>
			   <div id=\"bg-interna-2\" class=\"hidden-xs\"></div>";
	?>
	<?php require_once '../includes/header-2.php'; ?>
		
		<section class="rows clearfix">			
			<div id="slide-internas" class="carousel slide col-lg-7 col-md-7 col-sm-12 col-xs-12" data-ride="carousel" data-pause="hover">
				<div class="carousel-inner">
					<div class="active item"><img src="../imagens/produtos/tendas/01.jpg" alt="Tenda Inflável BRK - Modelo Casa com Teto Inflado - Medida Final 4,0m de largura x 4,0m de comprimento" class="img-responsive" /></div>
					<div class="item"><img src="../imagens/produtos/tendas/02.jpg" alt="Tenda Inflável Braço do Norte - Modelo Versátil - Medida Final 5,0m de largura x 5,0m de comprimento" class="img-responsive" /></div>
					<div class="item"><img src="../imagens/produtos/tendas/03.jpg" alt="Tenda Inflável Biofisio - Modelo Casa - Medida Final 4,0m de largura x 4,0m de comprimento" class="img-responsive" /></div>
				</div>
				<a class="carousel-control left"  href="#slide-internas" data-slide="prev" role="button" title="Voltar"><span class="hide">Voltar</span></a>
				<a class="carousel-control right" href="#slide-internas" data-slide="next" role="button" title="Avançar"><span class="hide">Avançar</span></a>

				<ul class="carousel-indicators">
					<li data-target="#slide-internas" data-slide-to="0" class="col-lg-4 col-md-4 col-sm-6 col-xs-6"><img src="../imagens/produtos/tendas/01.jpg" alt="Tenda Inflável BRK - Modelo Casa com Teto Inflado - Medida Final 4,0m de largura x 4,0m de comprimento" class="img-responsive" /></li>
					<li data-target="#slide-internas" data-slide-to="1" class="col-lg-4 col-md-4 col-sm-6 col-xs-6"><img src="../imagens/produtos/tendas/02.jpg" alt="Tenda Inflável Braço do Norte - Modelo Versátil - Medida Final 5,0m de largura x 5,0m de comprimento" class="img-responsive" /></li>
					<li data-target="#slide-internas" data-slide-to="2" class="col-lg-4 col-md-4 col-sm-6 col-xs-6"><img src="../imagens/produtos/tendas/03.jpg" alt="Tenda Inflável Biofisio - Modelo Casa - Medida Final 4,0m de largura x 4,0m de comprimento" class="img-responsive" /></li>
				</ul>				
			</div>	
			<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">				
				<div class="rows" id="info-produto">
					<ol class="breadcrumb">
						<li><a href="/">Home</a></li>		
						<li><a href="/inflaveis/">Infláveis</a></li>					
						<li class="active"><strong>Tendas </strong></li>
					</ol>
					<h1>Tendas </h1>
					<p>As tendas infláveis são um diferencial em qualquer evento. Semelhantes a um portal, elas criam a sensação de passagem para um novo ambiente, um novo mundo, interferindo positivamente na percepção de seus clientes e fortalecendo os laços entre eles e sua marca. Disponíveis em vários modelos, inclusive em formato iglu.</p>
					<ul class="clearfix">
						<li class="col-lg-6 col-md-6 col-sm-12 col-xs-12"><a href="#" title="Baixar o manual" data-toggle="modal" data-target="#Modal">ver o manual</a></li>
						<li class="col-lg-6 col-md-6 col-sm-12 col-xs-12"><a href="#" title="Baixar o catálogo" data-toggle="modal" data-target="#catalogo">baixar o catálogo</a></li> 
					</ul>
					<a href="#" title="Entre em contato">Entre em contato</a>
				</div>
			</div>		
		</section>
		<section class="col-lg-12 col-md-12 col-sm-12 col-xs-12 galeria-produto">
			<div class="rows">
				<a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/tendas/04.jpg" title="Tenda Inflável Auto Panambi Volks - Modelo Versátil com logo inflável aplicado - Medida Final 5,0m de largura x 5,0m de comprimento">
					<span><img src="../imagens/produtos/tendas/04.jpg" alt="Tenda Inflável Auto Panambi Volks - Modelo Versátil com logo inflável aplicado - Medida Final 5,0m de largura x 5,0m de comprimento"></span>
				</a>
				<a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/tendas/05.jpg" title="Tenda Inflável Anhanguera Florianópolis - Modelo Stilus - Medida Final 3,0m de largura x 3,0m de comprimento">
					<span><img src="../imagens/produtos/tendas/05.jpg" alt="Tenda Inflável Anhanguera Florianópolis - Modelo Stilus - Medida Final 3,0m de largura x 3,0m de comprimento"></span>
				</a>
				<a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/tendas/06.jpg" title="Tenda Inflável Anhanguera - Modelo Versátil - Medida Final 5,0m de largura x 5,0m de comprimento">
					<span><img src="../imagens/produtos/tendas/06.jpg" alt="Tenda Inflável Anhanguera - Modelo Versátil - Medida Final 5,0m de largura x 5,0m de comprimento"></span>
				</a>
				<a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/tendas/07.jpg" title="Tenda Inflável Anhanguera - Modelo Casa - Medida Final 3,0m de largura x 3,0m de comprimento">
					<span><img src="../imagens/produtos/tendas/07.jpg" alt="Tenda Inflável Anhanguera - Modelo Casa - Medida Final 3,0m de largura x 3,0m de comprimento"></span>
				</a>
				<a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/tendas/08.jpg" title="Tenda Inflável Anhanguera - Modelo Aranha - Medida Final 4,0m de largura x 4,0m de comprimento">
					<span><img src="../imagens/produtos/tendas/08.jpg" alt="Tenda Inflável Anhanguera - Modelo Aranha - Medida Final 4,0m de largura x 4,0m de comprimento"></span>
				</a>
				<a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/tendas/09.jpg" title="Tenda Inflável ACE - Modelo Stilus - Medida Final 4,0m de largura x 4,0m de comprimento">
					<span><img src="../imagens/produtos/tendas/09.jpg" alt="Tenda Inflável ACE - Modelo Stilus - Medida Final 4,0m de largura x 4,0m de comprimento"></span>
				</a>

				<a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/tendas/10.jpg" title="Tenda Inflável 3VS Nutrition - Modelo Stilus - Medida Final 3,0m de largura x 3,0m de comprimento">
					<span><img src="../imagens/produtos/tendas/10.jpg" alt="Tenda Inflável 3VS Nutrition - Modelo Stilus - Medida Final 3,0m de largura x 3,0m de comprimento"></span>
				</a>
				<a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/tendas/11.jpg" title="Tenda Inflável  Universidade LaSalle - Modelo Versátil - Medida Final 4,0m de largura x 4,0m de comprimento">
					<span><img src="../imagens/produtos/tendas/11.jpg" alt="Tenda Inflável  Universidade LaSalle - Modelo Versátil - Medida Final 4,0m de largura x 4,0m de comprimento"></span>
				</a>
				<a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/tendas/12.jpg" title="Tenda Inflável  Temper Vidros - Modelo Versátil com logo inflável aplicado - Medida Final 4,0m de largura x 4,0m de comprimento">
					<span><img src="../imagens/produtos/tendas/12.jpg" alt="Tenda Inflável  Temper Vidros - Modelo Versátil com logo inflável aplicado - Medida Final 4,0m de largura x 4,0m de comprimento"></span>
				</a>
				<a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/tendas/13.jpg" title="Tenda Inflável  Sou Único - Modelo Versátil - Medida Final 4,0m de largura x 4,0m de comprimento">
					<span><img src="../imagens/produtos/tendas/13.jpg" alt="Tenda Inflável  Sou Único - Modelo Versátil - Medida Final 4,0m de largura x 4,0m de comprimento"></span>
				</a>
				<a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/tendas/14.jpg" title="Tenda Inflável  Ray Just - Modelo Versátil - Medida Final 5,0m de largura x 5,0m de comprimento">
					<span><img src="../imagens/produtos/tendas/14.jpg" alt="Tenda Inflável  Ray Just - Modelo Versátil - Medida Final 5,0m de largura x 5,0m de comprimento"></span>
				</a>
				
				<a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/tendas/15.jpg" title="Tenda Inflável  Nissan Niscar - Modelo Versátil - Medida Final 4,0m de largura x 4,0m de comprimento">
					<span><img src="../imagens/produtos/tendas/15.jpg" alt="Tenda Inflável  Nissan Niscar - Modelo Versátil - Medida Final 4,0m de largura x 4,0m de comprimento"></span>
				</a>
				<a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/tendas/16.jpg" title="Tenda Inflável  Mirrai Toyota - Modelo Versátil - Medida Final 4,0m de largura x 4,0m de comprimento">
					<span><img src="../imagens/produtos/tendas/16.jpg" alt="Tenda Inflável  Mirrai Toyota - Modelo Versátil - Medida Final 4,0m de largura x 4,0m de comprimento"></span>
				</a>
				<a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/tendas/17.jpg" title="Tenda Inflável  Lei Seca São Jose dos Campos - Modelo Iglu - Medida Final 3,0m de largura x 2,5m de altura x 2,5m de comprimento">
					<span><img src="../imagens/produtos/tendas/17.jpg" alt="Tenda Inflável  Lei Seca São Jose dos Campos - Modelo Iglu - Medida Final 3,0m de largura x 2,5m de altura x 2,5m de comprimento"></span>
				</a>
				<a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/tendas/18.jpg" title="Tenda Inflável  Agrobella - Modelo Versátil com Arte em Impressão Digital Total - Medida Final 6,0m de largura x 6,0m de comprimento">
					<span><img src="../imagens/produtos/tendas/18.jpg" alt="Tenda Inflável  Agrobella - Modelo Versátil com Arte em Impressão Digital Total - Medida Final 6,0m de largura x 6,0m de comprimento"></span>
				</a>
				<a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/tendas/19.jpg" title="Tenda Inflável  Agrobella - Modelo Versátil com Arte aplicada em Impressão Digital Total - Medida Final 6,0m de largura x 6,0m de comprimento">
					<span><img src="../imagens/produtos/tendas/19.jpg" alt="Tenda Inflável  Agrobella - Modelo Versátil com Arte aplicada em Impressão Digital Total - Medida Final 6,0m de largura x 6,0m de comprimento"></span>
				</a>
				<a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/tendas/20.jpg" title="Tenda Inflável EMS Suplevit - Modelo Casa Castelinho em Impressão Digital Total de Arte - Medida Final 3,0m de largura x 3,0m de comprimento">
					<span><img src="../imagens/produtos/tendas/20.jpg" alt="Tenda Inflável EMS Suplevit - Modelo Casa Castelinho em Impressão Digital Total de Arte - Medida Final 3,0m de largura x 3,0m de comprimento"></span>
				</a>


				<a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/tendas/21.jpg" title="Tenda Inflável EMS Suplevit - Modelo Casa Castelinho com Arte em Impressão Digital Total - Medida Final 3,0m de largura x 3,0m de comprimento">
					<span><img src="../imagens/produtos/tendas/21.jpg" alt="Tenda Inflável EMS Suplevit - Modelo Casa Castelinho com Arte em Impressão Digital Total - Medida Final 3,0m de largura x 3,0m de comprimento"></span>
				</a>
				<a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/tendas/22.jpg" title="Tenda Inflável EMS Suplevit - Modelo Arco - Medida Final 4,0m de largura x 4,0m de comprimento">
					<span><img src="../imagens/produtos/tendas/22.jpg" alt="Tenda Inflável EMS Suplevit - Modelo Arco - Medida Final 4,0m de largura x 4,0m de comprimento"></span>
				</a>
				<a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/tendas/23.jpg" title="Tenda Inflável EMS Apevitin - Modelo Arco - Medida Final 4,0m de largura x 4,0m de comprimento">
					<span><img src="../imagens/produtos/tendas/23.jpg" alt="Tenda Inflável EMS Apevitin - Modelo Arco - Medida Final 4,0m de largura x 4,0m de comprimento"></span>
				</a>
				<a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/tendas/24.jpg" title="Tenda Inflável EMS - Modelo Aranha - Medida Final 4,0m de largura x 4,0m de comprimento">
					<span><img src="../imagens/produtos/tendas/24.jpg" alt="Tenda Inflável EMS - Modelo Aranha - Medida Final 4,0m de largura x 4,0m de comprimento"></span>
				</a>
				<a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/tendas/25.jpg" title="Tenda Inflável Elma Chips - Modelo Versátil - Medida Final 5,0m de largura x 5,0m de comprimento">
					<span><img src="../imagens/produtos/tendas/25.jpg" alt="Tenda Inflável Elma Chips - Modelo Versátil - Medida Final 5,0m de largura x 5,0m de comprimento"></span>
				</a>
				<a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/tendas/26.jpg" title="Tenda Inflável Drogavet - Modelo Aranha com Arte aplicada no teto em Impressão Digital Total - Medida Final 3,0m de largura x 3,0m de comprimento">
					<span><img src="../imagens/produtos/tendas/26.jpg" alt="Tenda Inflável Drogavet - Modelo Aranha com Arte aplicada no teto em Impressão Digital Total - Medida Final 3,0m de largura x 3,0m de comprimento"></span>
				</a>


				<a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/tendas/27.jpg" title="Tenda Inflável Douglas Med - Modelo Versátil com logo inflável aplicado - Medida Final 3,0m de largura x 3,0m de comprimento">
					<span><img src="../imagens/produtos/tendas/27.jpg" alt="Tenda Inflável Douglas Med - Modelo Versátil com logo inflável aplicado - Medida Final 3,0m de largura x 3,0m de comprimento"></span>
				</a>
				<a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/tendas/28.jpg" title="Tenda Inflável Direcional - Modelo Aranha - Medida Final 3,0m de largura x 3,0m de comprimento">
					<span><img src="../imagens/produtos/tendas/28.jpg" alt="Tenda Inflável Direcional - Modelo Aranha - Medida Final 3,0m de largura x 3,0m de comprimento"></span>
				</a>
				<a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/tendas/29.jpg" title="Tenda Inflável Decisão - Modelo Aranha - Medida Final 3,0m de largura x 3,0m de comprimento">
					<span><img src="../imagens/produtos/tendas/29.jpg" alt="Tenda Inflável Decisão - Modelo Aranha - Medida Final 3,0m de largura x 3,0m de comprimento"></span>
				</a>
				<a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/tendas/30.jpg" title="Tenda Inflável CPFL Energia - Modelo Casa com Cortina Removíveis em zíper - Medida Final 4,0m de largura x 4,0m de comprimento">
					<span><img src="../imagens/produtos/tendas/30.jpg" alt="Tenda Inflável CPFL Energia - Modelo Casa com Cortina Removíveis em zíper - Medida Final 4,0m de largura x 4,0m de comprimento"></span>
				</a>
				<a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/tendas/31.jpg" title="Tenda Inflável Construtora Tenda - Modelo Casa - Medida Final 3,0m de largura x 3,0m de comprimento">
					<span><img src="../imagens/produtos/tendas/31.jpg" alt="Tenda Inflável Construtora Tenda - Modelo Casa - Medida Final 3,0m de largura x 3,0m de comprimento"></span>
				</a>


				<a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/tendas/32.jpg" title="Tenda Inflável Colbeck - Modelo Aranha - Medida Final 5,0m de largura x 5,0m de comprimento">
					<span><img src="../imagens/produtos/tendas/32.jpg" alt="Tenda Inflável Colbeck - Modelo Aranha - Medida Final 5,0m de largura x 5,0m de comprimento"></span>
				</a>
				<a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/tendas/33.jpg" title="Tenda Inflável Cetem - Modelo Versátil - Medida Final 3,0m de largura x 3,0m de comprimento">
					<span><img src="../imagens/produtos/tendas/33.jpg" alt="Tenda Inflável Cetem - Modelo Versátil - Medida Final 3,0m de largura x 3,0m de comprimento"></span>
				</a>
				<a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/tendas/34.jpg" title="Tenda Inflável CDL Florianópolis - Modelo Versátil - Medida Final 4,0m de largura x 4,0m de comprimento">
					<span><img src="../imagens/produtos/tendas/34.jpg" alt="Tenda Inflável CDL Florianópolis - Modelo Versátil - Medida Final 4,0m de largura x 4,0m de comprimento"></span>
				</a>

				<a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/tendas/35.jpg" title="Tenda Inflável CDL Florianópolis - Modelo Versátil - Medida Final 4,0m de largura x 4,0m de comprimento">
					<span><img src="../imagens/produtos/tendas/35.jpg" alt="Tenda Inflável CDL Florianópolis - Modelo Versátil - Medida Final 4,0m de largura x 4,0m de comprimento"></span>
				</a>

				<a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/tendas/tenda-001.jpg" title="Iglu Inflável Delonghi - Arte impressão digital total. Janelas e cortinas em PVC. Medida Final 2,4m x 3m de diâmetro com porta x 1.8m de altura">
					<span><img src="../imagens/produtos/tendas/tenda-001.jpg" alt="Iglu Inflável Delonghi - Arte impressão digital total. Janelas e cortinas em PVC. Medida Final 2,4m x 3m de diâmetro com porta x 1.8m de altura"></span>
				</a>
				<a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/tendas/tenda-002.jpg" title="Iglu Inflável Delonghi - Arte impressão digital total. Medida Final 2,4m x 3m de diâmetro com porta x 1.8m de altura">
					<span><img src="../imagens/produtos/tendas/tenda-002.jpg" alt="Iglu Inflável Delonghi - Arte impressão digital total. Medida Final 2,4m x 3m de diâmetro com porta x 1.8m de altura"></span>
				</a>
				<a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/tendas/tenda-005.jpg" title="Iglu Inflável Delonghi - Arte impressão digital total. Cortinas em PVC transparente. Medida Final 2,4m x 3m de diâmetro com porta x 1.8m de altura">
					<span><img src="../imagens/produtos/tendas/tenda-005.jpg" alt="Iglu Inflável Delonghi - Arte impressão digital total. Cortinas em PVC transparente. Medida Final 2,4m x 3m de diâmetro com porta x 1.8m de altura"></span>
				</a>
				<a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/tendas/tenda-010.jpg" title="Tenda Inflável Samsung - Modelo Aranha com Arte em Impressão Digital Total - Medida Final 3,0m de largura x 3,0m de comprimento">
					<span><img src="../imagens/produtos/tendas/tenda-010.jpg" alt="Tenda Inflável Samsung - Modelo Aranha com Arte em Impressão Digital Total - Medida Final 3,0m de largura x 3,0m de comprimento"></span>
				</a>
				<a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/tendas/tenda-035.jpg" title="Tenda Inflável CNA - Modelo Casa - Medida Final 3,0m de largura x 3,0m de comprimento">
					<span><img src="../imagens/produtos/tendas/tenda-035.jpg" alt="Tenda Inflável CNA - Modelo Casa - Medida Final 3,0m de largura x 3,0m de comprimento"></span>
				</a>
				<a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/tendas/tenda-043.jpg" title="Detalhe do Motoventilador e Manual de Instruções embutidos na coluna da Tenda.">
					<span><img src="../imagens/produtos/tendas/tenda-043.jpg" alt="Detalhe do Motoventilador e Manual de Instruções embutidos na coluna da Tenda."></span>
				</a>
				<a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/tendas/tenda-044.jpg" title="Tenda Inflável Amarela - Modelo Aranha - Medida Final 3,0m de largura x 3,0m de comprimento">
					<span><img src="../imagens/produtos/tendas/tenda-044.jpg" alt="Tenda Inflável Amarela - Modelo Aranha - Medida Final 3,0m de largura x 3,0m de comprimento"></span>
				</a>
				
			</div>
		</section>
	<?php require_once '../includes/produtos-internas.php'; ?>
	
	<?php require_once '../includes/duvidas-frequentes-outros.php'; ?>
	</div>
	
	<?php require_once '../includes/footer-map-interna.php'; ?>	
	<?php require_once '../includes/manual.php'; ?>	
	<?php require_once '../includes/catalogo.php'; ?>	

	<?php require_once '../includes/footer-2.php'; ?>
	