	<?php 
		//SEO
	$title = 'Balões Roof Tops | Lack Infláveis';
	$description = 'Balões Roof Tops  Empresa Especializada em Infláveis. Aproveite acesse e agora e solicite já o seu orçamento online do seu Inflável!';
	$canonicalTag = '<link rel="canonical” href="http://www.lackinflaveis.com.br/inflaveis/roof-tops.php"/>';
	$bg = "<div id=\"bg-interna-1\"></div>
	<div id=\"bg-interna-2\" class=\"hidden-xs\"></div>";
	?>
	<?php require_once '../includes/header-2.php'; ?>



<span class="bradcrumb">
    produtos <span>mascotes</span>
</span>


	<section class="rows mascote">
		<!------------------------------------>

		<div class="col-md-12" id="slider-for">
			<div class="slider-for">
				<div>
					<div class="col-sm-12 col-md-6 textoSlide">
						<p>
							<strong>mascotes</strong>
							Sua empresa tem um mascote? Que tal transformá-lo em um inflável gigante e levá-lo a eventos como exposições, feiras ao ar livre e eventos empresariais? Nossa tecnologia permite a produção de infláveis em uma ampla gama de formatos, inclusive em 3D, garantindo que seu mascote marcará presença em qualquer lugar.
							<span>
								<a href="#" title="Ver o manual" data-toggle="modal" data-target="#Modal"><img src="../imagens/manual_Icon.jpg" class="icones">Ver o manual</a>
								<a href="#" title="Baixar o catálogo" data-toggle="modal" data-target="#catalogo"><img src="../imagens/catalogo_Icon.jpg" class="icones">Baixar o catálogo</a>
							</span>
						</p>
					</div>
					<div class="col-sm-12 col-md-6 SliderContainer"><img src="../imagens/produtos/mascote/01.jpg" class="imagensSlide" alt="infláveis" title="infláveis"  width="98%" height="auto"></div>
				</div>
				<div>
					<div class="col-sm-12 col-md-6 textoSlide">
						<p>
							<strong>mascotes</strong>
							Sua empresa tem um mascote? Que tal transformá-lo em um inflável gigante e levá-lo a eventos como exposições, 
                                                        feiras ao ar livre e eventos empresariais? Nossa tecnologia permite a produção de infláveis em uma ampla gama de formatos, 
                                                        inclusive em 3D, garantindo que seu mascote marcará presença em qualquer lugar.
							<span>
								<a href="#" title="Ver o manual" data-toggle="modal" data-target="#Modal"><img src="../imagens/manual_Icon.jpg" class="icones">Ver o manual</a>
								<a href="#" title="Baixar o catálogo" data-toggle="modal" data-target="#catalogo"><img src="../imagens/catalogo_Icon.jpg" class="icones">Baixar o catálogo</a>
							</span>
						</p>
					</div>
					<div class="col-sm-12 col-md-6 SliderContainer">
						<img src="../imagens/produtos/mascote/02.jpg" alt="infláveis"  class="imagensSlide"  title="infláveis" width="98%" height="auto">
					</div>
				</div>
				<div>
					<div class="col-sm-12 col-md-6  textoSlide">
						<p>
							<strong>mascotes</strong>
							Sua empresa tem um mascote? Que tal transformá-lo em um inflável gigante e levá-lo a eventos como exposições, 
                                                        feiras ao ar livre e eventos empresariais? Nossa tecnologia permite a produção de infláveis em uma ampla gama de formatos, 
                                                        inclusive em 3D, garantindo que seu mascote marcará presença em qualquer lugar.
							<span>
								<a href="#" title="Ver o manual" data-toggle="modal" data-target="#Modal"><img src="../imagens/manual_Icon.jpg" class="icones">Ver o manual</a>
								<a href="#" title="Baixar o catálogo" data-toggle="modal" data-target="#catalogo"><img src="../imagens/catalogo_Icon.jpg" class="icones">Baixar o catálogo</a>
							</span>
						</p>
					</div>
					<div class="col-sm-12 col-md-6  SliderContainer"><img src="../imagens/produtos/mascote/03.jpg"  class="imagensSlide"  alt="infláveis" title="infláveis"  width="98%" height="auto"></div>
				</div>
			</div>

			<img src="../imagens/arrow2.png" class="hidden-xs left">
			<img src="../imagens/arrow1.png" class="hidden-xs right">

			<div class='slider-nav hidden-xs'>
				<div><img src="../imagens/produtos/mascote/01.jpg" width="100px"></div>
				<div><img src="../imagens/produtos/mascote/02.jpg" width="100px"></div>
				<div><img src="../imagens/produtos/mascote/03.jpg" width="100px"></div>
			</div>        
		</div>
		<!------------------------------------->


		<section class="form-footer row hidden-xs hidden-sm clearfix form">
			<?php
				include_once '../includes/components/form_footer.php';
			?>
		</section>



		<!-- produtos -->
                
                <section class="col-lg-12 col-md-12 col-sm-12 col-xs-12 galeria-produto"><div class="rows"><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/mascote/01.jpg" title="Mascote Inflável Sushiloko - Modelo 2D em impressão digital frente e verso. Medida Final 2,1m de largura x 3,0m de altura"><span><img src="../imagens/produtos/mascote/01.jpg" alt="Mascote Inflável Sushiloko - Modelo 2D em impressão digital frente e verso. Medida Final 2,1m de largura x 3,0m de altura"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/mascote/02.jpg" title="Mascote Inflável Super D - Modelo 2D em impressão digital frente e verso. Medida Final 5,0m de altura"><span><img src="../imagens/produtos/mascote/02.jpg" alt="Mascote Inflável Super D - Modelo 2D em impressão digital frente e verso. Medida Final 5,0m de altura"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/mascote/03.jpg" title="Mascote Inflável Rosa Mistica - Modelo 2D em impressão digital frente e verso. Medida Final 5,8m de largura x 6,0m de altura"><span><img src="../imagens/produtos/mascote/03.jpg" alt="Mascote Inflável Rosa Mistica - Modelo 2D em impressão digital frente e verso. Medida Final 5,8m de largura x 6,0m de altura"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/mascote/05.jpg" title="Mascote Inflável Piu-Piu - Modelo 2D em impressão digital frente e verso. Medida Final 2,0m de largura x 3,0m de altura"><span><img src="../imagens/produtos/mascote/05.jpg" alt="Mascote Inflável Piu-Piu - Modelo 2D em impressão digital frente e verso. Medida Final 2,0m de largura x 3,0m de altura"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/mascote/06.jpg" title="Mascote Inflável Peperito (verso) - Modelo 2D em impressão digital total. Medida Final 3,0m de altura"><span><img src="../imagens/produtos/mascote/06.jpg" alt="Mascote Inflável Peperito (verso) - Modelo 2D em impressão digital total. Medida Final 3,0m de altura"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/mascote/07.jpg" title="Mascote Inflável Peperito (lateral) - Modelo 2D em impressão digital total. Medida Final 3,0m de altura"><span><img src="../imagens/produtos/mascote/07.jpg" alt="Mascote Inflável Peperito (lateral) - Modelo 2D em impressão digital total. Medida Final 3,0m de altura"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/mascote/08.jpg" title="Mascote Inflável Peperito (frente) - Modelo 2D em impressão digital total. Medida Final 3,0m de altura"><span><img src="../imagens/produtos/mascote/08.jpg" alt="Mascote Inflável Peperito (frente) - Modelo 2D em impressão digital total. Medida Final 3,0m de altura"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/mascote/10.jpg" title="Mascote Inflável Papai Noel em Pé (frente) - Modelo 3D. Medida Final 3,0m de altura"><span><img src="../imagens/produtos/mascote/10.jpg" alt="Mascote Inflável Papai Noel em Pé (frente) - Modelo 3D. Medida Final 3,0m de altura"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/mascote/11.jpg" title="Mascote Inflável Papai Noel em Pé - Modelo 3D. Medida Final 5,0m de altura"><span><img src="../imagens/produtos/mascote/11.jpg" alt="Mascote Inflável Papai Noel em Pé - Modelo 3D. Medida Final 5,0m de altura"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/mascote/12.jpg" title="Mascote Inflável Mario (lateral) - Modelo 2D em impressão digital frente e verso. Medida Final 3,2m de altura"><span><img src="../imagens/produtos/mascote/12.jpg" alt="Mascote Inflável Mario (lateral) - Modelo 2D em impressão digital frente e verso. Medida Final 3,2m de altura"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/mascote/14.jpg" title="Mascote Inflável Mapinha Drogaria Minas Brasil - Modelo 2D em impressão digital frente e verso. Medida Final 2,0m de altura"><span><img src="../imagens/produtos/mascote/14.jpg" alt="Mascote Inflável Mapinha Drogaria Minas Brasil - Modelo 2D em impressão digital frente e verso. Medida Final 2,0m de altura"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/mascote/15.jpg" title="Mascote Inflável Lindoar - Modelo 2D em impressão digital frente e verso. Medida Final 2,5m de altura"><span><img src="../imagens/produtos/mascote/15.jpg" alt="Mascote Inflável Lindoar - Modelo 2D em impressão digital frente e verso. Medida Final 2,5m de altura"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/mascote/16.jpg" title="Mascote Inflável KM Carregadores (lateral) - Modelo 3D em impressão digital total. Medida Final 1,8m de altura"><span><img src="../imagens/produtos/mascote/16.jpg" alt="Mascote Inflável KM Carregadores (lateral) - Modelo 3D em impressão digital total. Medida Final 1,8m de altura"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/mascote/17.jpg" title="Mascote Inflável KM Carregadores (frente) - Modelo 3D em impressão digital total. Medida Final 1,8m de altura"><span><img src="../imagens/produtos/mascote/17.jpg" alt="Mascote Inflável KM Carregadores (frente) - Modelo 3D em impressão digital total. Medida Final 1,8m de altura"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/mascote/18.jpg" title="Mascote Inflável Isorecort - Modelo 2D em impressão digital frente e verso. Medida Final 4,0m de altura"><span><img src="../imagens/produtos/mascote/18.jpg" alt="Mascote Inflável Isorecort - Modelo 2D em impressão digital frente e verso. Medida Final 4,0m de altura"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/mascote/19.jpg" title="Mascote Inflável Hemopa - Modelo 2D em impressão digital frente e verso. Medida Final 2,7m de largura x 3,0m de altura"><span><img src="../imagens/produtos/mascote/19.jpg" alt="Mascote Inflável Hemopa - Modelo 2D em impressão digital frente e verso. Medida Final 2,7m de largura x 3,0m de altura"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/mascote/20.jpg" title="Mascote Inflável GBM Distribuidora - Modelo 2D em impressão digital frente e verso. Medida Final 4,6m de largura x 4,0m de altura x 0,7m de comprimento"><span><img src="../imagens/produtos/mascote/20.jpg" alt="Mascote Inflável GBM Distribuidora - Modelo 2D em impressão digital frente e verso. Medida Final 4,6m de largura x 4,0m de altura x 0,7m de comprimento"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/mascote/21.jpg" title="Mascote Inflável Galo - Modelo 2D em impressão digital frente e verso. Medida Final 3,0m de largura x 3,0m de altura"><span><img src="../imagens/produtos/mascote/21.jpg" alt="Mascote Inflável Galo - Modelo 2D em impressão digital frente e verso. Medida Final 3,0m de largura x 3,0m de altura"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/mascote/22.jpg" title="Mascote Inflável Duke Energy (verso) - Modelo 3D em impressão digital total. Medida Final 2,2m de altura"><span><img src="../imagens/produtos/mascote/22.jpg" alt="Mascote Inflável Duke Energy (verso) - Modelo 3D em impressão digital total. Medida Final 2,2m de altura"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/mascote/23.jpg" title="Mascote Inflável Duke Energy (lateral) - Modelo 3D em impressão digital total. Medida Final 2,2m de altura"><span><img src="../imagens/produtos/mascote/23.jpg" alt="Mascote Inflável Duke Energy (lateral) - Modelo 3D em impressão digital total. Medida Final 2,2m de altura"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/mascote/24.jpg" title="Mascote Inflável Duke Energy (frente) - Modelo 3D em impressão digital total. Medida Final 2,2m de altura"><span><img src="../imagens/produtos/mascote/24.jpg" alt="Mascote Inflável Duke Energy (frente) - Modelo 3D em impressão digital total. Medida Final 2,2m de altura"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/mascote/25.jpg" title="Mascote Inflável Boneco Brahma - Modelo 3D em impressão digital total. Medida Final 3,8m de altura"><span><img src="../imagens/produtos/mascote/25.jpg" alt="Mascote Inflável Boneco Brahma - Modelo 3D em impressão digital total. Medida Final 3,8m de altura"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/mascote/26.jpg" title="Mascote Inflável Bem Cuidar - Modelo 2D em impressão digital frente e verso. Medida Final 2,0m de altura"><span><img src="../imagens/produtos/mascote/26.jpg" alt="Mascote Inflável Bem Cuidar - Modelo 2D em impressão digital frente e verso. Medida Final 2,0m de altura"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/mascote/27.jpg" title="Mascote Inflável BelTools - Modelo 2D em impressão digital frente e verso. Medida Final 3,2m de largura x 4,0m de altura"><span><img src="../imagens/produtos/mascote/27.jpg" alt="Mascote Inflável BelTools - Modelo 2D em impressão digital frente e verso. Medida Final 3,2m de largura x 4,0m de altura"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/mascote/28.jpg" title="Mascote com Base Inflável Moinho - Modelo 2D em impressão digital frente e verso. Medida Final 5,0m de altura"><span><img src="../imagens/produtos/mascote/28.jpg" alt="Mascote com Base Inflável Moinho - Modelo 2D em impressão digital frente e verso. Medida Final 5,0m de altura"></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/mascote/mascote-003.jpg" title=""><span><img src="../imagens/produtos/mascote/mascote-003.jpg" alt=""></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/mascote/mascote-005.jpg" title=""><span><img src="../imagens/produtos/mascote/mascote-005.jpg" alt=""></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/mascote/mascote-006.jpg" title=""><span><img src="../imagens/produtos/mascote/mascote-006.jpg" alt=""></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/mascote/mascote-009.jpg" title=""><span><img src="../imagens/produtos/mascote/mascote-009.jpg" alt=""></span></a><a class="fancybox-thumb col-lg-2 col-md-2 col-sm-2 col-xs-3" rel="fancybox-thumb" href="../imagens/produtos/mascote/mascote-013.jpg" title=""><span><img src="../imagens/produtos/mascote/mascote-013.jpg" alt=""></span></a></div></section>
                
                <!-- end produtos -->


		<?php require_once './../includes/produtos-internas.php'; ?>
		<?php require_once './../includes/duvidas-frequentes-roof-top.php'; ?>

	</div>





</div>

<div class="container-fluid" id="mapa-interna">
	<div id="bg-interna" class="hidden-xs hidden-sm"></div>
	<div id="map_canvas"></div>
	<div class="container z-index">
		<?php require_once './../includes/form-contato.php'; ?>
	</div>
</div>


<?php require_once '../includes/footer-map-interna.php'; ?>	
<?php require_once '../includes/manual.php'; ?>	
<?php require_once '../includes/catalogo.php'; ?>	
<?php require_once '../includes/footer-2.php'; ?>
