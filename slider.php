
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <title></title>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">

        <meta name="robots" content="noindex, nofollow">

        <meta name="googlebot" content="noindex, nofollow">

        <meta name="viewport" content="width=device-width, initial-scale=1">

        <script type="text/javascript"  src="/js/lib/dummy.js" ></script>

        <link rel="stylesheet" type="text/css" href="css/styles.min.css">

        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css">

        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js"></script>



        <style type="text/css">
            .slider > div {
                display:block; width:100%; padding: 50px 0;
                background: #FF0;
                text-align: center; font-size: 2em;
            }
            .slider-nav { 
                text-align: center;
                display: inline-block;
                position: fixed;
                right: 2%;
            }
            .slider-nav > div {
                display: inline-block;
                width: 80px;
                height: 71px;
                margin: 0 0px;
                padding: 3px 0;
                text-align: center;
                font-size: 2em;
                cursor: pointer;
                overflow: hidden;
            }
        </style>

        <script type="text/javascript">
            window.onload = function () {
                $('.slider-for').slick({
                    arrows: false, dots: false, infinite: true, speed: 500,
                    autoplay: true, autoplaySpeed: 3000, slidesToShow: 1, slidesToScroll: 1
                });
                $('.slider-nav > div').click(function () {
                    $('.slider-for').slick('slickGoTo', $(this).index());
                })
            }
        </script>



    </head>
    <body>
        <div class="slider-for">
            <div>
                <div class="col-md-6 textoSlide">
                    <p>
                        <strong>blimp</strong>
                        Promova a interação inusitada e divertida de seus clientes com sua marca! A blimp, ou bola-show, é uma maneira dinâmica e atraente de chamar a atenção para sua empresa em eventos de grande público como shows e festas. Disponíveis em diferentes tamanhos e materiais (inclusive transparentes), os blimps fazem sua marca acontecer.
                        <span>
                            <a href="#" title="Baixar o catálogo" data-toggle="modal" data-target="#catalogo">
                                <img src="imagens/manual_Icon.jpg" class="icones">
                                Baixar o catálogo
                            </a>
                        </span>
                    </p>
                </div>
                <div class="col-md-6"><img src="imagens/produtos/blimp/02.jpg" class="imagensSlide" alt="infláveis" title="infláveis"  width="98%" height="auto"></div>
            </div>
            <div>
                <div class="col-md-6 textoSlide">
                    <p>
                        <strong>roof tops</strong>
                        Os infláveis do tipo roof Top são ideais para divulgação de festas e eventos promocionais por serem muito chamativos e 
                        proporcionarem grande destaque da logo da empresa. Disponíveis em diversos tamanhos, 
                        podem ser instalados no topo de imóveis (daí o nome rooftop) ou instalados com segurança em calçadas e 
                        nos próprios locais dos eventos. Todos os modelos são motoventilados.
                        <span>
                            <a href="#" title="Ver o manual" data-toggle="modal" data-target="#Modal"><img src="imagens/manual_Icon.jpg" class="icones">Ver o manual</a>
                            <a href="#" title="Baixar o catálogo" data-toggle="modal" data-target="#catalogo"><img src="imagens/catalogo_Icon.jpg" class="icones">Baixar o catálogo</a>
                        </span>
                    </p>
                </div>
                <div class="col-md-6">
                    <img src="imagens/produtos/roof-top/04.jpg" alt="infláveis"  class="imagensSlide"  title="infláveis" width="98%" height="auto">
                </div>
            </div>
            <div>
                <div class="col-md-6 textoSlide">
                    <p>
                        <strong>Mascotes</strong>
                        Promova a interação inusitada e divertida de seus clientes com sua marca! A blimp, ou bola-show, é uma maneira dinâmica e atraente de chamar a atenção para sua empresa em eventos de grande público como shows e festas. Disponíveis em diferentes tamanhos e materiais (inclusive transparentes), os blimps fazem sua marca acontecer.
                        <span>
                            <a href="#" title="Ver o manual" data-toggle="modal" data-target="#Modal"><img src="imagens/manual_Icon.jpg" class="icones">Ver o manual</a>
                            <a href="#" title="Baixar o catálogo" data-toggle="modal" data-target="#catalogo"><img src="imagens/catalogo_Icon.jpg" class="icones">Baixar o catálogo</a>
                        </span>
                    </p>
                </div>
                <div class="col-md-6"><img src="imagens/produtos/mascote/02.jpg"  class="imagensSlide"  alt="infláveis" title="infláveis"  width="98%" height="auto"></div>
            </div>
        </div>

        <div class='slider-nav'>
            <div><img src="imagens/produtos/blimp/02.jpg" width="100px"></div>
            <div><img src="imagens/produtos/roof-top/04.jpg" width="100px"></div>
            <div><img src="imagens/produtos/mascote/02.jpg" width="100px"></div>
        </div>

        <script>
            // tell the embed parent frame the height of the content
            if (window.parent && window.parent.parent) {
                window.parent.parent.postMessage(["resultsFrame", {
                        height: document.body.getBoundingClientRect().height,
                        slug: "xghc52wu"
                    }], "*")
            }
        </script>
    </body>
</html>
